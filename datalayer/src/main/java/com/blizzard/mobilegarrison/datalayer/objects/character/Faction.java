package com.blizzard.mobilegarrison.datalayer.objects.character;

/**
 * Enum that determines the faction of a character.
 *
 * @author Joshua Hale
 */
public enum Faction {

    ALLIANCE,
    HORDE,
    NEUTRAL
}
