package com.blizzard.mobilegarrison.ui.garrison;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.blizzard.mobilegarrison.R;
import com.blizzard.mobilegarrison.adapters.MissionsAdapter;
import com.blizzard.mobilegarrison.constants.MissionsFragmentType;
import com.blizzard.mobilegarrison.datalayer.manager.bus.EventBus;
import com.blizzard.mobilegarrison.datalayer.objects.character.WoWCharacter;
import com.blizzard.mobilegarrison.datalayer.objects.garrison.mission.Mission;
import com.blizzard.mobilegarrison.datalayer.tasks.async.GetAvailableMissionsTask;
import com.blizzard.mobilegarrison.datalayer.tasks.results.GetAvailableMissionsEvent;
import com.blizzard.mobilegarrison.datalayer.tasks.results.GetErrorEvent;
import com.blizzard.mobilegarrison.datalayer.tasks.results.StartMissionEvent;
import com.blizzard.mobilegarrison.ui.base.BaseFragment;
import com.blizzard.mobilegarrison.ui.garrison.mission.MissionDetailActivity;
import com.blizzard.mobilegarrison.utils.error.ErrorUtils;
import com.squareup.otto.Subscribe;

import java.util.List;

/**
 * Shows a list of all of the current mission for a particular character
 */
public class AvailableMissionsFragment extends BaseFragment {

    private List<Mission> mMissions;
    private View mView;

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup parent,
                             final Bundle savedInstanceState) {

        mView = inflater.inflate(R.layout.fragment_current_missions, parent, false);

        init();

        return mView;
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getInstance().register(this);
    }

    private void init() {

        ListView missionsListView = (ListView) mView.findViewById(R.id.missionsListView);
        missionsListView.setEmptyView(mView.findViewById(R.id.emptyView));

        new GetAvailableMissionsTask(getActivity()).execute();
    }

    private void updateUI() {

        MissionsAdapter missionsAdapter = new MissionsAdapter(getActivity(), mMissions, MissionsFragmentType.AVAILABLE);
        ListView missionsListView = (ListView) mView.findViewById(R.id.missionsListView);
        missionsListView.setAdapter(missionsAdapter);

        missionsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Mission mission = mMissions.get(position);

                Bundle extras = new Bundle();
                extras.putSerializable(getString(R.string.mission), mission);
                Intent intent = new Intent(getActivity(), MissionDetailActivity.class);
                intent.putExtras(extras);
                startActivityForResult(intent, 0);
            }
        });
    }

    @Subscribe
    public void getMissions(GetAvailableMissionsEvent event) {
        mMissions = event.getResult();

        if (mMissions == null) {
            TextView emptyView = (TextView) mView.findViewById(R.id.emptyView);
            emptyView.setText(R.string.no_missions_found);
        }

        updateUI();
    }

    @Subscribe
    public void onError(GetErrorEvent event) {
        String message = event.getMessage();
        boolean showDialog = event.showDialog();

        ErrorUtils.showMessage(getActivity(), message, showDialog);
    }

    @Subscribe
    public void onMissionStarted(StartMissionEvent event) {

        Log.v("MissionFragment", "Mission Started, Updating");
        init();
    }


    @Override
    public void onStop() {
        EventBus.getInstance().unregister(this);
        super.onStop();
    }

    @Override
    public void onCharacterChanged(final WoWCharacter wowCharacter) {
        super.onCharacterChanged(wowCharacter);

        new GetAvailableMissionsTask(getActivity()).execute();
    }
}
