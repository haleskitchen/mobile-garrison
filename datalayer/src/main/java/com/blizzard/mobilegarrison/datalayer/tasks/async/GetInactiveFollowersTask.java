package com.blizzard.mobilegarrison.datalayer.tasks.async;

import android.content.Context;
import android.os.AsyncTask;

import com.blizzard.mobilegarrison.datalayer.constants.ErrorStrings;
import com.blizzard.mobilegarrison.datalayer.manager.bus.EventBus;
import com.blizzard.mobilegarrison.datalayer.manager.data.DataManager;
import com.blizzard.mobilegarrison.datalayer.manager.user.UserManager;
import com.blizzard.mobilegarrison.datalayer.objects.garrison.follower.Follower;
import com.blizzard.mobilegarrison.datalayer.tasks.results.GetErrorEvent;
import com.blizzard.mobilegarrison.datalayer.tasks.results.GetInactiveFollowersEvent;

import java.util.List;

/**
 * This AsyncTask allows for the retrieval of our Inactive Followers dependent on the Character
 * that is currently selected. The table that gets pulled is unique to the character.
 * Each table only consists of the IDs of followers that exist, which is then used
 * with an INNER JOIN to retrieve the information for the followers that don't change. <br />
 * <br />
 * Follower Data that is unique to the character: <br />
 * - Follower Rarity <br />
 * - Follower Quality <br />
 * - Follower Traits <br />
 * - Follower Abilities <br />
 * - Follower Experience <br />
 * - Follower Item Level <br />
 * - Follower Level <br />
 * <br />
 * Follower Data that is the same across all characters: <br />
 * - Follower Name <br />
 * - Follower Class <br />
 * - Follower Icon <br />
 * - Follower Race <br />
 * - Follower Gender <br />
 */
public class GetInactiveFollowersTask extends AsyncTask<Void, Void, List<Follower>> {

    private final Context mContext;

    public GetInactiveFollowersTask(Context context) {
        mContext = context;
    }

    @Override
    public List<Follower> doInBackground(Void... params) {

        String dbString = UserManager.getInstance(mContext).getDatabaseString();

        DataManager dataManager = DataManager.getInstance(mContext);
        return dataManager.getInactiveFollowers(dbString);
    }

    @Override
    public void onPostExecute(List<Follower> results) {
        if (results != null) {
            EventBus.getInstance().post(new GetInactiveFollowersEvent(results));
        } else {
            EventBus.getInstance().post(new GetErrorEvent(
                    ErrorStrings.ERROR_NO_FOLLOWERS, true));
        }
    }
}
