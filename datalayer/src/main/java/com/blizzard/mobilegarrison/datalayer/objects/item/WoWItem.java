package com.blizzard.mobilegarrison.datalayer.objects.item;

import com.blizzard.mobilegarrison.datalayer.objects.character.Quality;

/**
 * This object is created after the official REST API from Blizzard returns a JSON
 * response for a particular item id.
 *
 * @author Joshua Hale (josh@joshuahale.com)
 */
public class WoWItem {

    private int id;
    private Quality quality;
    private String description;
    private String name;
    private String icon;

    public int getId() {
        return id;
    }

    public void setId(final int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(final String icon) {
        this.icon = icon;
    }

    public Quality getQuality() {
        return quality;
    }

    public void setQuality(final Quality quality) {
        this.quality = quality;
    }
}
