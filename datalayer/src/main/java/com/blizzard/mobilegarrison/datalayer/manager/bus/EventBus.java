package com.blizzard.mobilegarrison.datalayer.manager.bus;

import com.squareup.otto.Bus;

/**
 * Singleton instance of the Bus class by Otto, which allows for communication
 * between the UI and backend without coupling it.
 *
 * @author Joshua Hale.
 */
public class EventBus {

    private static final Bus BUS = new Bus();

    public static Bus getInstance() {
        return BUS;
    }
}
