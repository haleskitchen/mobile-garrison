package com.blizzard.mobilegarrison.datalayer.objects.garrison.follower;

import com.blizzard.mobilegarrison.datalayer.R;

/**
 * Enum that determines the trais for a particular follower <br />
 * The ID represents the Trait ID, used to pull trait details. <br />
 * The resource ID determines the image to show for a particular trait
 *
 * @author Joshua Hale
 */
public enum FollowerTrait {

    ALCHEMY(54, R.drawable.trait_alchemy),
    ALLY_OF_ARGUS(67, R.drawable.trait_ally_of_argus),
    ANGLER(227, R.drawable.trait_angler),
    BEASTSLAYER(37, R.drawable.trait_beastslayer),
    BLACKSMITHING(55, R.drawable.trait_blacksmithing),
    BODYGUARD(231, R.drawable.trait_bodyguard),
    BURST_OF_POWER(77, R.drawable.trait_burst_of_power),
    BREW_AFICIONADO(69, R.drawable.trait_brew_aficionado),
    CANINE_COMPANION(68, R.drawable.trait_canine_companion),
    CAVE_DWELLER(45, R.drawable.trait_cave_dweller),
    CHILD_OF_DRAENOR(70, R.drawable.trait_child_of_draenor),
    CHILD_OF_THE_MOON(66, R.drawable.trait_child_of_the_moon),
    COLD_BLOODED(8, R.drawable.trait_cold_blooded),
    COMBAT_EXPERIENCE(201, R.drawable.trait_combat_experience),
    DANCER(232, R.drawable.trait_dancer),
    DEATH_FASCINATION(71, R.drawable.trait_death_fascination),
    DEMONSLAYER(36, R.drawable.trait_demonslayer),
    DWARVENBORN(65, R.drawable.trait_dwarvenborn),
    ECONOMIST(75, R.drawable.trait_economist),
    ELVENKIND(74, R.drawable.trait_elvenkind),
    ENCHANTING(56, R.drawable.trait_enchanting),
    ENGINEERING(57, R.drawable.trait_tradeskill),
    EPIC_MOUNT(221, R.drawable.trait_epic_mount),
    EVERGREEN(228, R.drawable.trait_evergreen),
    EXTRA_TRAINING(80, R.drawable.trait_extra_training),
    FAST_LEARNER(29, R.drawable.trait_fast_learner),
    FURYSLAYER(41, R.drawable.trait_furyslayer),
    GNOME_LOVER(63, R.drawable.trait_gnome_lover),
    GRONNSLAYER(40, R.drawable.trait_gronnslayer),
    GUERILLA_FIGHTER(46, R.drawable.trait_guerilla_fighter),
    HEARTHSTONE_PRO(236, R.drawable.trait_hearthstone_pro),
    HERBALISM(53, R.drawable.trait_herbalism),
    HIGH_STAMINA(76, R.drawable.trait_high_stamina),
    HUMANIST(64, R.drawable.trait_humanist),
    INSCRIPTION(58, R.drawable.trait_inscription),
    JEWELCRAFTING(59, R.drawable.trait_jewelcrafting),
    LEATHERWORKING(60, R.drawable.trait_leatherworking),
    LONE_WOLF(78, R.drawable.trait_lone_wolf),
    MARSHWALKER(48, R.drawable.trait_marshwalker),
    MINING(52, R.drawable.trait_mining),
    MOUNTAINEER(7, R.drawable.trait_mountaineer),
    NATURALIST(44, R.drawable.trait_naturalist),
    OGRESLAYER(38, R.drawable.trait_ogreslayer),
    ORCSLAYER(4, R.drawable.trait_orcslayer),
    PLAINSRUNNER(49, R.drawable.trait_plainsrunner),
    PRIMALSLAYER(39, R.drawable.trait_primalslayer),
    SCAVENGER(79, R.drawable.trait_scavenger),
    SKINNING(62, R.drawable.trait_skinning),
    TAILORING(61, R.drawable.trait_tailoring),
    TALONSLAYER(43, R.drawable.trait_talonslayer),
    TOTEMIST(72, R.drawable.trait_totemist),
    URBANITE(47, R.drawable.trait_urbanite),
    VOIDSLAYER(42, R.drawable.trait_voidslayer),
    VOODOO_ZEALOT(73, R.drawable.trait_voodoo_zealot),
    WASTELANDER(9, R.drawable.trait_wastelander);

    private static final int size = FollowerTrait.values().length;

    private final int followerTrait;
    private final int iconResId;

    private FollowerTrait(final int followerTrait, final int iconResId) {
        this.followerTrait = followerTrait;
        this.iconResId = iconResId;
    }

    public int getFollowerTrait() {
        return this.followerTrait;
    }

    public static FollowerTrait getEnum(final int value) {
        for (final FollowerTrait followerTrait : values()) {
            if(value == followerTrait.getFollowerTrait()) {
                return followerTrait;
            }
        }
        return null;
    }

    public int getIconResId() {
        return iconResId;
    }

    public static int size() {
        return size;
    }
}
