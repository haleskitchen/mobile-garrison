package com.blizzard.mobilegarrison.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.blizzard.mobilegarrison.R;
import com.blizzard.mobilegarrison.constants.Font;
import com.blizzard.mobilegarrison.constants.MissionsFragmentType;
import com.blizzard.mobilegarrison.datalayer.objects.character.Quality;
import com.blizzard.mobilegarrison.datalayer.objects.garrison.mission.Mission;
import com.blizzard.mobilegarrison.datalayer.objects.garrison.mission.MissionReward;
import com.blizzard.mobilegarrison.datalayer.utils.time.TimeUtils;
import com.blizzard.mobilegarrison.utils.font.FontUtils;
import com.blizzard.mobilegarrison.utils.garrison.GarrisonUtils;

import java.util.List;

/**
 * Adapter for both the {@link com.blizzard.mobilegarrison.ui.garrison.AvailableMissionsFragment}
 * and {@link com.blizzard.mobilegarrison.ui.garrison.ActiveMissionsFragment} to show the
 * missions for each subsection. This shows all of the different missions that are either
 * available or that the user has currently selected folowers for
 *
 * @author Joshua Hale
 * @version 0.1
 * @since 0.1
 */
public class MissionsAdapter extends BaseAdapter {

    private final Context mContext;
    private FontUtils mFontUtils;
    private final List<Mission> mMissions;
    private final MissionsFragmentType mType;

    public MissionsAdapter(Context context, List<Mission> missions, MissionsFragmentType type) {
        super();

        mContext = context;
        mMissions = missions;
        mType = type;
    }

    @Override
    public int getCount() {
        return mMissions.size();
    }

    @Override
    public Object getItem(int position) {
        return mMissions.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            mFontUtils = new FontUtils(mContext);
            convertView = inflater.inflate(R.layout.item_mission, parent, false);

            viewHolder = new ViewHolder();

            viewHolder.backgroundLocationImageView = (ImageView) convertView.
                    findViewById(R.id.backgroundLocationImageView);
            viewHolder.missionRewardImageView = (ImageView) convertView.
                    findViewById(R.id.missionRewardImageView);
            viewHolder.missionTypeImageView = (ImageView) convertView.
                    findViewById(R.id.missionTypeImageView);
            viewHolder.missionDurationTextView = (TextView) convertView.
                    findViewById(R.id.missionDurationTextView);
            viewHolder.missionIlvlTextView = (TextView) convertView.
                    findViewById(R.id.missionIlvlTextView);
            viewHolder.missionLevelTextView = (TextView) convertView.
                    findViewById(R.id.missionLevelTextView);
            viewHolder.missionNameTextView = (TextView) convertView.
                    findViewById(R.id.missionNameTextView);
            viewHolder.missionRewardAmountTextView = (TextView) convertView.
                    findViewById(R.id.missionRewardAmountTextView);
            viewHolder.parentFrameLayout = (FrameLayout) convertView
                    .findViewById(R.id.parentFrameLayout);

            convertView.setTag(viewHolder);
        } else {

            viewHolder = (ViewHolder) convertView.getTag();
        }

        Mission mission = mMissions.get(position);

        setupMissionRewardUI(viewHolder, mission.getMissionReward());

        viewHolder.missionLevelTextView.setText(String.valueOf((mission.getMissionLevel())));

        if (mission.getMissionIlvl() == 0) {
            viewHolder.missionIlvlTextView.setVisibility(View.GONE);
        } else {
            viewHolder.missionIlvlTextView.setText(String.valueOf(mission.getMissionIlvl()));
        }

        viewHolder.missionNameTextView.setText(mission.getMissionName());

        if (mType == MissionsFragmentType.AVAILABLE) {
            viewHolder.missionDurationTextView.setText(getMissionDurationText(mission.getMissionDuration()));
        } else if (mType == MissionsFragmentType.ACTIVE) {
            TimeUtils.showTimeRemaining(viewHolder.missionDurationTextView, mission.getEndTime());
        }

        viewHolder.missionTypeImageView.setImageResource(
                GarrisonUtils.getMissionTypeDrawable(mission.getMissionType()));

        viewHolder.backgroundLocationImageView.setImageResource(
                GarrisonUtils.getMissionZoneListViewBackgroundDrawable(mission.getMissionZone()));

        if (mission.getMissionRarity() == Quality.RARE) {
            viewHolder.parentFrameLayout.setBackgroundResource(R.drawable.bg_listview_mission_item_rare);
        }

        mFontUtils.setTypeface(Font.MORPHEUS, viewHolder.missionNameTextView);

        return convertView;
    }

    /**
     * Return the drawable that is needed for each of the different reward types
     * that are possible for missions
     * @param reward <b>(MissionReward)</b> - Mission at the current position in our adapter
     * @return <b>(int)</b> - Resource ID for the correct drawable
     */
    private int getMissionRewardDrawable(MissionReward reward) {
        if (reward.getExperienceReward() > 0) {
            return R.drawable.ic_mission_reward_experience;
        } else if (reward.getGoldReward() > 0) {
            return R.drawable.ic_mission_reward_gold;
        } else {
            return GarrisonUtils.getMissionItemRewardDrawable(reward.getItemReward());
        }
    }

    private void setupMissionRewardUI(ViewHolder viewHolder, MissionReward reward) {

        viewHolder.missionRewardImageView.setImageResource(getMissionRewardDrawable(reward));

        if (reward.getResourcesReward() > 0) {
            viewHolder.missionRewardAmountTextView.setText(String.valueOf(reward.getResourcesReward()));
        } else {
            viewHolder.missionRewardAmountTextView.setVisibility(View.GONE);
        }
    }



    /**
     * Formats the string for our mission duration
     * @param missionDuration <b>(int)</b> - Mission duration (in hours)
     * @return <b>(String)</b> - Mission duration in format ( missionDuration )
     */
    private String getMissionDurationText(int missionDuration) {

        return "(" + missionDuration + " hr)";
    }

    private static class ViewHolder {

        public FrameLayout parentFrameLayout;
        public ImageView backgroundLocationImageView;
        public ImageView missionRewardImageView;
        public ImageView missionTypeImageView;
        public TextView missionDurationTextView;
        public TextView missionIlvlTextView;
        public TextView missionLevelTextView;
        public TextView missionNameTextView;
        public TextView missionRewardAmountTextView;
    }
}
