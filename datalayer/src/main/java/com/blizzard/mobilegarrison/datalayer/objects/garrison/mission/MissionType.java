package com.blizzard.mobilegarrison.datalayer.objects.garrison.mission;

/**
 * Enum that determines which type the Mission is
 *
 * @author Joshua Hale
 */
public enum MissionType {

    COMBAT,
    PATROL,
    TREASURE
}
