package com.blizzard.mobilegarrison.receiver;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

import com.blizzard.mobilegarrison.R;
import com.blizzard.mobilegarrison.ui.dashboard.DashboardActivity;

/**
 * BroadcastReceiver that handles the showing of a Local Notification when the timestamp
 * for a notification has finally passed. The only issue with using this methodology rather
 * than having the server control it is that we are relying on the phone to be on. If the user
 * restarts their phone, the AlarmManager is wiped and we are no longer able to see
 * notifications for the missions. This is a temporary solution until I can put the logic
 * into the backend and run through GCM.
 *
 * @author Joshua Hale (josh@joshuahale.com)
 */
public class NotificationReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent paramIntent) {

        //-- Create a new Notification using the NotificationCompat Builder
        //-- so that our notification can be used across all different OS versions.
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context)
                .setSmallIcon(R.drawable.ic_notification)
                .setContentTitle(context.getString(R.string.notification_title))
                .setContentText(context.getString(R.string.notification_message))
                .setAutoCancel(true);

        //-- Open the DashboardActivity when the user selects the notification.
        Intent resultIntent = new Intent(context, DashboardActivity.class);

        //-- Stack builder object contains an artificial back stack for the Activity
        //-- that we start. This allows the user to exit the application when they
        //-- use the back button on their device.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addParentStack(DashboardActivity.class);
        stackBuilder.addNextIntent(resultIntent);

        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(resultPendingIntent);

        NotificationManager notificationManager = (NotificationManager)
                context.getSystemService(Context.NOTIFICATION_SERVICE);

        //-- Using the standard 1, but if we used a different ID, we could
        //-- modify the notification later.
        notificationManager.notify(1, mBuilder.build());
    }

}
