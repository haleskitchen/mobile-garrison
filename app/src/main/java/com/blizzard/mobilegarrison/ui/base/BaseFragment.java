package com.blizzard.mobilegarrison.ui.base;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.blizzard.mobilegarrison.datalayer.objects.listener.OnCharacterChangedListener;
import com.blizzard.mobilegarrison.datalayer.objects.character.WoWCharacter;

/**
 * BaseFragment that will handle character changes when the listener is fired
 *
 * @author Joshua Hale
 */
public class BaseFragment extends Fragment implements OnCharacterChangedListener {

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
                             final Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onCharacterChanged(final WoWCharacter wowCharacter) {

    }
}
