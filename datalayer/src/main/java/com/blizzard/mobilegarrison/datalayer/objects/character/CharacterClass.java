package com.blizzard.mobilegarrison.datalayer.objects.character;

/**
 * Enum that handles which class a character is
 *
 * @author Joshua Hale
 */
public enum CharacterClass {

    DEATH_KNIGHT(6),
    DRUID(11),
    HUNTER(3),
    MAGE(8),
    MONK(10),
    PALADIN(2),
    PRIEST(5),
    ROGUE(4),
    SHAMAN(7),
    WARLOCK(9),
    WARRIOR(1);

    private final int characterClass;

    private CharacterClass(final int characterClass) {
        this.characterClass = characterClass;
    }

    private int getCharacterClass() {
        return this.characterClass;
    }

    public static CharacterClass getEnum(final int value) {
        for (final CharacterClass charClass : values()) {
            if(value == charClass.getCharacterClass()) {
                return charClass;
            }
        }
        return null;
    }
}
