package com.blizzard.mobilegarrison.datalayer.objects.character;

/**
 * Enum that determines the character race
 */
public enum CharacterRace {

    BLOOD_ELF(10),
    DRAENEI(11),
    DWARF(3),
    GOBLIN(9),
    GNOME(7),
    HUMAN(1),
    NIGHT_ELF(4),
    ORC(2),
    PANDAREN_ALLIANCE(25),
    PANDAREN_HORDE(26),
    PANDAREN_NEUTRAL(24),
    TAUREN(6),
    TROLL(8),
    UNDEAD(5),
    WORGEN(22),
    OTHER(999);

    private final int characterRace;

    private CharacterRace(final int characterRace) {
        this.characterRace = characterRace;
    }

    private int getCharacterRace() {
        return this.characterRace;
    }

    public static CharacterRace getEnum(final int value) {
        for (final CharacterRace charRace : values()) {
            if(value == charRace.getCharacterRace()) {
                return charRace;
            }
        }
        return null;
    }
}
