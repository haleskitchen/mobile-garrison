package com.blizzard.mobilegarrison.datalayer.objects.character;

/**
 * Metadata for a specific class. <br />
 * Class color, class background, and class name.
 *
 * @author Joshua Hale
 */
class ClassMetadata {

    private int color;

    public int getColor() {
        return color;
    }

    public void setColor(final int color) {
        this.color = color;
    }
}
