package com.blizzard.mobilegarrison.widgets.mission;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blizzard.mobilegarrison.R;
import com.blizzard.mobilegarrison.datalayer.interfaces.OnFollowerRemovedListener;
import com.blizzard.mobilegarrison.datalayer.objects.garrison.follower.Follower;
import com.blizzard.mobilegarrison.utils.garrison.GarrisonUtils;
import com.blizzard.mobilegarrison.utils.lazyload.ImageLoader;

/**
 * Layout that is used in our {@link com.blizzard.mobilegarrison.ui.garrison.mission.MissionDetailFragment}
 * to show what Follower will be used for a specific Mission
 *
 * @author Joshua Hale
 */
public class MissionFollowerLayout extends LinearLayout {

    private Follower mFollower;
    private ImageLoader mImageLoader;
    private OnFollowerRemovedListener mListener;

    private ImageView mFollowerIconImageView;
    private TextView mFollowerLevelTextView;
    private TextView mFollowerNameTextView;

    public MissionFollowerLayout(Context context) {
        super(context);
    }

    public MissionFollowerLayout(Context context, OnFollowerRemovedListener listener) {
        super(context);
        mListener = listener;
        mImageLoader = new ImageLoader();

        init();
    }

    //-- TODO: Add the follower to the layout when clicked from ListAdapter
    public MissionFollowerLayout(Context context, Follower follower) {
        super(context);

        mFollower = follower;

        init();
    }

    private void init() {
        inflate(getContext(), R.layout.item_mission_follower, this);

        mFollowerIconImageView = (ImageView) findViewById(R.id.followerIconImageView);
        mFollowerLevelTextView = (TextView) findViewById(R.id.followerLevelTextView);
        mFollowerNameTextView = (TextView) findViewById(R.id.followerNameTextView);
        LinearLayout followerParentLinearLayout = (LinearLayout) findViewById(R.id.followerParentLinearLayout);

        followerParentLinearLayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null && mFollower != null) {
                    mListener.onFollowerRemoved(mFollower);
                    setFollower(null);
                    resetUI();
                }
            }
        });
    }

    private void resetUI() {
        mFollowerIconImageView.setImageResource(R.drawable.follower_placeholder);
        mFollowerIconImageView.setBackgroundResource(R.drawable.background_item_default);
        mFollowerLevelTextView.setBackgroundResource(R.drawable.background_item_default_no_border);
        mFollowerLevelTextView.setText("");
        mFollowerNameTextView.setText("");
    }

    private void updateUI() {

        if (mFollower != null) {
            mFollowerLevelTextView.setText(String.valueOf(mFollower.getFollowerLevel()));
            mFollowerNameTextView.setText(mFollower.getFollowerName());
            mImageLoader.displayImage(GarrisonUtils
                    .getFollowerImageUrl(mFollower.getFollowerImageUri()), mFollowerIconImageView);

            mFollowerIconImageView.setBackgroundResource(
                    GarrisonUtils.getFollowerIconBorder(mFollower.getQuality()));
            mFollowerLevelTextView.setBackgroundResource(
                    GarrisonUtils.getFollowerLevelBorder(mFollower.getQuality()));
        } else {
            resetUI();
        }
    }

    public void setFollower(Follower follower) {
        mFollower = follower;

        updateUI();
    }
}
