package com.blizzard.mobilegarrison.datalayer.tasks.async;

import android.content.Context;
import android.os.AsyncTask;

import com.blizzard.mobilegarrison.datalayer.manager.data.DataManager;
import com.blizzard.mobilegarrison.datalayer.manager.user.UserManager;
import com.blizzard.mobilegarrison.datalayer.objects.garrison.mission.Mission;

import java.util.ArrayList;
import java.util.List;

/**
 * Async Wrapper that deletes all of the completed missions for a specific character.
 *
 * @author Joshua Hale (josh@joshuahale.com)
 */
public class DeleteMissionsTask extends AsyncTask<Void, Void, Void> {

    private final Context mContext;
    private final List<Integer> mMissionIds;

    public DeleteMissionsTask(Context context, List<Mission> missions) {
        mContext = context;
        mMissionIds = new ArrayList<>();

        for (Mission mission : missions) {
            mMissionIds.add(mission.getId());
        }
    }

    @Override
    public Void doInBackground(Void... params) {

        String uniqueId = UserManager.getInstance(mContext).getDatabaseString();
        DataManager dataManager = DataManager.getInstance(mContext);
        dataManager.deleteMissions(mMissionIds, uniqueId);

        return null;
    }
}
