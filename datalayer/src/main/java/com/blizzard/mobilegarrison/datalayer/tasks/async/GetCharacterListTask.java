package com.blizzard.mobilegarrison.datalayer.tasks.async;

import android.content.Context;
import android.os.AsyncTask;

import com.blizzard.mobilegarrison.datalayer.constants.ErrorStrings;
import com.blizzard.mobilegarrison.datalayer.interfaces.OnCharactersLoadedListener;
import com.blizzard.mobilegarrison.datalayer.manager.bus.EventBus;
import com.blizzard.mobilegarrison.datalayer.manager.data.DataManager;
import com.blizzard.mobilegarrison.datalayer.objects.character.WoWCharacter;
import com.blizzard.mobilegarrison.datalayer.tasks.results.GetCharactersEvent;
import com.blizzard.mobilegarrison.datalayer.tasks.results.GetErrorEvent;

import java.util.List;

/**
 * Retrieves the list of characters for the user's account.
 */
public class GetCharacterListTask extends AsyncTask<Void, Void, List<WoWCharacter>> {

    private final Context mContext;
    private OnCharactersLoadedListener mListener;

    /**
     * Overloaded constructor for use with our BaseActivity. Otto has an issue
     * distinguishing the classes to register and unregister, so using a listener
     * makes it easier when this information is required in multiple places.
     * @param context <b>(Context)</b> - Activity Context
     * @param listener <b>(OnCharactersLoadedListener)</b> - Listener that fires once all
     *                 of the character data has been loaded
     */
    public GetCharacterListTask(Context context, OnCharactersLoadedListener listener) {
        mContext = context;
        mListener = listener;
    }

    /**
     * Constructor that just uses the context to retrieve our DataManager.
     * @param context <b>(Context)</b> - Activity context
     */
    public GetCharacterListTask(Context context) {
        mContext = context;
    }

    @Override
    public List<WoWCharacter> doInBackground(final Void... params) {

        //-- Retrieve the singleton instance of our DataManager
        DataManager dataManager = DataManager.getInstance(mContext);
        return dataManager.getCharacterList();
    }

    @Override
    public void onPostExecute(final List<WoWCharacter> characters) {

        if (characters != null && characters.size() > 0) {
            EventBus.getInstance().post(new GetCharactersEvent(characters));

            //-- If we have a listener, fire it.
            if (mListener != null) {
                mListener.onCharactersLoadedListener(characters);
            }
        } else {
            EventBus.getInstance().post(new GetErrorEvent(
                    ErrorStrings.ERROR_NO_CHARACTERS, true));
        }
    }
}
