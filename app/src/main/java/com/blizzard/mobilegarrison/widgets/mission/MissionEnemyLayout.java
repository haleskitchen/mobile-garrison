package com.blizzard.mobilegarrison.widgets.mission;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blizzard.mobilegarrison.R;
import com.blizzard.mobilegarrison.datalayer.constants.MobileGarrisonConstants;
import com.blizzard.mobilegarrison.datalayer.objects.garrison.follower.FollowerCounter;
import com.blizzard.mobilegarrison.datalayer.objects.garrison.mission.MissionEnemy;
import com.blizzard.mobilegarrison.utils.lazyload.ImageLoader;

import org.apmem.tools.layouts.FlowLayout;

/**
 * Custom Layout that shows a MissionEnemy in our
 * {@link com.blizzard.mobilegarrison.ui.garrison.mission.MissionDetailFragment}
 *
 * @author Joshua hale
 */
public class MissionEnemyLayout extends LinearLayout {

    private MissionEnemy mMissionEnemy;

    public MissionEnemyLayout(Context context) {
        super(context);
    }

    public MissionEnemyLayout(Context context, MissionEnemy missionEnemy) {
        super(context);

        mMissionEnemy = missionEnemy;

        init();
    }

    private void init() {
        inflate(getContext(), R.layout.item_mission_enemy, this);

        TextView enemyNameTextView = (TextView) findViewById(R.id.enemyNameTextView);
        ImageView enemyIconImageView = (ImageView) findViewById(R.id.enemyIconImageView);

        enemyNameTextView.setText(mMissionEnemy.getName());

        ImageLoader imageLoader = new ImageLoader();
        imageLoader.displayImage(MobileGarrisonConstants.ENEMY_IMAGE_URL + mMissionEnemy.getIconUri(),
                enemyIconImageView);

        FlowLayout flowLayout = (FlowLayout) findViewById(R.id.enemyCountersFlowLayout);

        LayoutInflater inflater = (LayoutInflater) getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        for (FollowerCounter counter : mMissionEnemy.getCounters()) {

            View view = inflater.inflate(R.layout.item_follower_counter, null);

            ImageView enemyCounterImageView = (ImageView) view.findViewById(R.id.enemyCounterImageView);
            enemyCounterImageView.setImageResource(counter.getIconResId());
            flowLayout.addView(view);
        }
    }
}
