package com.blizzard.mobilegarrison.datalayer.manager.data;

import android.content.Context;
import android.net.http.AndroidHttpClient;
import android.util.Log;

import com.blizzard.mobilegarrison.datalayer.constants.MobileGarrisonConstants;
import com.blizzard.mobilegarrison.datalayer.manager.user.UserManager;
import com.blizzard.mobilegarrison.datalayer.objects.auth.AccessToken;
import com.blizzard.mobilegarrison.datalayer.objects.character.WoWCharacter;
import com.blizzard.mobilegarrison.datalayer.objects.garrison.follower.AbilityDetails;
import com.blizzard.mobilegarrison.datalayer.objects.garrison.follower.Follower;
import com.blizzard.mobilegarrison.datalayer.objects.garrison.follower.FollowerAbility;
import com.blizzard.mobilegarrison.datalayer.objects.garrison.follower.FollowerTrait;
import com.blizzard.mobilegarrison.datalayer.objects.garrison.follower.TraitDetails;
import com.blizzard.mobilegarrison.datalayer.objects.garrison.mission.Mission;
import com.blizzard.mobilegarrison.datalayer.objects.garrison.mission.MissionEnemy;
import com.blizzard.mobilegarrison.datalayer.objects.item.WoWItem;
import com.blizzard.mobilegarrison.datalayer.tasks.async.GetAccessTokenTask;
import com.blizzard.mobilegarrison.datalayer.tasks.callable.DeleteMissionCallable;
import com.blizzard.mobilegarrison.datalayer.tasks.callable.GetAbilityDetailsCallable;
import com.blizzard.mobilegarrison.datalayer.tasks.callable.GetMissionEnemyCallable;
import com.blizzard.mobilegarrison.datalayer.tasks.callable.GetTraitDetailsCallable;
import com.blizzard.mobilegarrison.datalayer.utils.json.JSONUtils;
import com.blizzard.mobilegarrison.datalayer.utils.logging.CustomLog;
import com.blizzard.mobilegarrison.datalayer.utils.time.TimeUtils;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * Singleton manager interface that allows for all of the transactions that
 * are necessary between the front-end of the application and the backend. Handles
 * all the consumption of data from external sources and the creation of object models
 * that will be used throughout the lifecycle of the application.
 *
 * @author Joshua Hale (josh@joshuahale.com)
 * @version 0.1
 */
public class DataManager {

    private static final String TAG = "DataManager";
    private static DataManager instance;
    private final Context context;
    private List<WoWCharacter> mCharacters;

    /**
     * Constructor that requires an Activity Contextd
     *
     * @param context <b>(Context)</b> - Activity context required for grabbing resource
     *                identifiers within our application
     */
    private DataManager(final Context context) {
        this.context = context;
    }

    /**
     * Returns an instance of the DataManager
     *
     * @param context <b>(Context)</b> - Activity context
     * @return <b>(DataManager)</b> - Returns the singleton instance of the DataManager
     */
    public static DataManager getInstance(final Context context) {

        if (instance == null) {
            instance = new DataManager(context);
        }

        return instance;
    }

    /**
     * Retrieves an access token from the Battle.net API to be used throughout the application
     *
     * @param authCode <b>(String)</b> - Auth code returned from the WebView when the user logs in
     * @return <b>(String)</b> - The access token that is needed to access the application.
     */
    public String getAccessToken(final String authCode) {

        //-- Retrieve the instance of the UserManager
        final UserManager userManager = UserManager.getInstance(context);

        //-- If we don't have an auth code check to make sure we have one stored
        if (authCode == null) {
            userManager.getAuthCode();
        } else {
            userManager.setAuthCode(authCode);
        }

        //-- Retrieve the stored AccessToken object
        final AccessToken accessToken = userManager.getAccessToken();

        //-- If we have an access token, return the AccessToken String
        if (accessToken != null) {
            return accessToken.getAccessToken();
        } else {

            //-- If we don't, attempt to get a new AccessToken with the AuthCode we are given.
            try {
                return new GetAccessTokenTask(context).execute(authCode).get();
            } catch (final ExecutionException e) {
                CustomLog.w(TAG, "Execution of AsyncTask generated exception: " + e.getMessage());
            } catch (final InterruptedException e) {
                CustomLog.w(TAG, "Retrieving the information was interrupted: " + e.getMessage());
            }
        }

        return null;
    }

    /**
     * Returns the list of AbilityDetails for a specific Follower
     * @param followerAbilities <b>(FollowerAbility)</b> - Follower Abilities, parsed from
     *                          the Followers API
     * @return <b>(List)</b> - Ability Details, used for the UI for our FollowerDetailFragment
     */
    public List<AbilityDetails> getFollowerAbilityDetails(
            final List<FollowerAbility> followerAbilities) {

        //-- Create a new List of AbilityDetails
        final List<AbilityDetails> abilityDetailsList = new ArrayList<>();

        //-- Create a new Executor service that allows us to multi-thread these
        //-- different callables
        final ExecutorService executor = Executors.newFixedThreadPool(followerAbilities.size());

        //-- Create a List of Callables that return an AbilityDetails object
        final List<Callable<AbilityDetails>> callableList = new ArrayList<>();

        //-- Foreach loop that loops through each FollowerAbility
        //-- and creates a new {@link GetAbilityDetailsCallable.class} for each
        //-- FollowerAbility
        for (final FollowerAbility ability : followerAbilities) {

            final String getAbilityUrl = getAbilityUrl(ability);

            final GetAbilityDetailsCallable abilityCallable =
                    new GetAbilityDetailsCallable(ability, getAbilityUrl);

            callableList.add(abilityCallable);
        }

        //-- Attempts to start all of the callables in the above created list
        //-- and waits for the return AbilityDetails object.
        try {
            final List<Future<AbilityDetails>> futureResults = executor.invokeAll(callableList);

            for (final Future<AbilityDetails> future : futureResults) {
                final AbilityDetails abilityDetails = future.get(3000, TimeUnit.MILLISECONDS);

                if (abilityDetails != null) {
                    abilityDetailsList.add(abilityDetails);
                }
            }
        } catch (final InterruptedException e) {
            CustomLog.w(TAG, "InterruptedException occurred: " + e.getMessage());
            e.printStackTrace();
        } catch (final ExecutionException e) {
            CustomLog.w(TAG, "Execution Exception in ExecutorService occurred: " + e.getMessage());
        } catch (final TimeoutException e) {
            CustomLog.w(TAG, "TimeoutException - Timeout for retrieving Battle.net API occurred: "
                    + e.getMessage());
        }

        //-- Returns the list once all of the FutureTasks have been successful or have failed.
        return abilityDetailsList;
    }

    /**
     * Returns a list of Mission Enemies for a specific mission
     * @param enemyIds <b>(List)</b> - List of Integers, which resolve to IDs for each enemy.
     * @return <b>(List)</b> - List of MissionEnemies after retrieving all results from the server
     */
    public List<MissionEnemy> getMissionEnemies(List<Integer> enemyIds) {

        //-- Create a new ArrayList of Enemies
        List<MissionEnemy> missionEnemies = new ArrayList<>();

        //-- Create a new ThreadPool at the size of our input list
        ExecutorService executor = Executors.newFixedThreadPool(enemyIds.size());

        //-- Create a new Callable List so that we can execute all callables at the same time
        List<Callable<MissionEnemy>> callableList = new ArrayList<>();

        //-- For each Integer, we create a new callable with the ID given
        for (Integer enemyId : enemyIds) {

            GetMissionEnemyCallable callable = new GetMissionEnemyCallable(
                    getMissionEnemyUrl(enemyId));
            callableList.add(callable);
        }

        try {
            //-- Turns our threaded calls into synchronous calls by waiting for the results.
            List<Future<MissionEnemy>> futureResults = executor.invokeAll(callableList);

            for (Future<MissionEnemy> future : futureResults) {
                MissionEnemy missionEnemy = future.get(3000, TimeUnit.MILLISECONDS);

                if (missionEnemy != null) {
                    missionEnemies.add(missionEnemy);
                }
            }
        } catch (InterruptedException e) {
            Log.w(TAG, "InterruptedException occurred: " + e.getMessage());
        } catch (ExecutionException e) {
            Log.w(TAG, "ExecutionException occurred: " + e.getMessage());
        } catch (TimeoutException e) {
            Log.w(TAG, "Timeout Exception occurred: " + e.getMessage());
        }

        //-- Returns the list of enemies once all the future tasks have resolved.
        return missionEnemies;
    }

    /**
     * Deletes all the missions that have been completed
     * @param missionIds <b>(List)</b> - Mission Ids for the missions
     * @param uniqueId <b>(String)</b> - Unique ID for our character.
     */
    public void deleteMissions(List<Integer> missionIds, String uniqueId) {

        ExecutorService executor = Executors.newFixedThreadPool(missionIds.size());

        List<Callable<Void>> callableList = new ArrayList<>();


        for (Integer missionId : missionIds) {
            DeleteMissionCallable callable = new DeleteMissionCallable(uniqueId, missionId);
            callableList.add(callable);
        }

        try {
            List<Future<Void>> futureResults = executor.invokeAll(callableList);

            for (Future<Void> future : futureResults) {
                Void result = future.get(3000, TimeUnit.MILLISECONDS);
            }
        } catch (InterruptedException e) {
            CustomLog.w(TAG, "InterruptedException occurred: " + e.getMessage());
        } catch (ExecutionException e) {
            Log.w(TAG, "ExecutionException occurred: " + e.getMessage());
        } catch (TimeoutException e) {
            Log.w(TAG, "Timeout Exception occurred: " + e.getMessage());
        }
    }

    /**
     * Returns the list of AbilityDetails for a specific Follower
     * @param followerTraits <b>(FollowerTrait)</b> - Follower Abilities, parsed from
     *                          the Followers API
     * @return <b>(List)</b> - Ability Details, used for the UI for FollowerDetailFragment
     */
    public List<TraitDetails> getFollowerTraits(List<FollowerTrait> followerTraits) {

        //-- Create a new List of AbilityDetails
        final List<TraitDetails> traitDetailsList = new ArrayList<>();

        //-- Create a new Executor service that allows us to multi-thread these
        //-- different callables
        final ExecutorService executor = Executors.newFixedThreadPool(followerTraits.size());

        //-- Create a List of Callables that return an AbilityDetails object
        final List<Callable<TraitDetails>> callableList = new ArrayList<>();

        //-- Foreach loop that loops through each FollowerAbility
        //-- and creates a new {@link GetAbilityDetailsCallable.class} for each
        //-- FollowerAbility
        for (final FollowerTrait trait : followerTraits) {

            final GetTraitDetailsCallable traitCallable =
                    new GetTraitDetailsCallable(trait, getTraitUrl(trait));

            callableList.add(traitCallable);
        }

        //-- Attempts to start all of the callables in the above created list
        //-- and waits for the return AbilityDetails object.
        try {
            final List<Future<TraitDetails>> futureResults = executor.invokeAll(callableList);

            for (final Future<TraitDetails> future : futureResults) {
                TraitDetails traitDetails = future.get(3000, TimeUnit.MILLISECONDS);

                if (traitDetails != null) {
                    traitDetailsList.add(traitDetails);
                }
            }
        } catch (final InterruptedException e) {
            CustomLog.w(TAG, "InterruptedException occurred: " + e.getMessage());
            e.printStackTrace();
        } catch (final ExecutionException e) {
            CustomLog.w(TAG, "Execution Exception in ExecutorService occurred: " + e.getMessage());
        } catch (final TimeoutException e) {
            CustomLog.w(TAG, "TimeoutException - Timeout for retrieving Battle.net API occurred: "
                    + e.getMessage());
        }

        //-- Returns the list once all of the FutureTasks have been successful or have failed.
        return traitDetailsList;
    }

    /**
     * Returns an InputStream from a specific URL
     *
     * @param client  <b>(AndroidHttpClient)</b> - Client is passed in so that the method calling it
     *                can manage the lifecycle of it when it's completed.
     * @param httpGet <b>(HttpGet)</b> - HTTPGet call that will be executed.
     * @return <b>(InputStream)</b> - InputStream returned from the HttpGet Content
     */
    public static InputStream getInputStream(final AndroidHttpClient client, final HttpGet httpGet) {

        try {
            final HttpResponse res = client.execute(httpGet);
            final HttpEntity entity = res.getEntity();
            return entity.getContent();
        } catch (final IOException e) {
            CustomLog.w(TAG, "Error retrieving IO Stream: " + e.getMessage());
        }

        return null;
    }

    /**
     * Returns the value of the InputStream response in a single String object
     *
     * @param inputStream <b>(InputStream)</b> - InputStream returned from an HTTP call
     * @return <b>(String)</b> - String returned from the
     */
    public static String getStringFromInputStream(final InputStream inputStream) {
        String result = "";

        try {
            //-- Get a new BufferedReader that reads the string with a UTF-8 Character Set
            final BufferedReader reader = new BufferedReader(
                    new InputStreamReader(inputStream, "UTF-8"), 8);

            //-- Create a new StringBuilder object to build the string from the response
            final StringBuilder sb = new StringBuilder();

            String line;

            //-- While we have lines within our repsonse, read the line and append it to
            //-- the end of our StringBuilder object
            while ((line = reader.readLine()) != null) {
                line = line + "\n";
                sb.append(line);
            }

            //-- Make the resulting String the result of the StringBuilder
            result = sb.toString();
        } catch (final UnsupportedEncodingException e) {
            CustomLog.w(TAG, "UnsupportedEncodingException e: " + e.getMessage());
        } catch (final IOException e) {
            CustomLog.w(TAG, "IOException: " + e.getMessage());
        }

        return result;
    }



    /**
     * Returns our current list of Followers for a specific character
     *
     * @return <b>(List)</b> - List of followers for a specific character.
     */
    public List<Follower> getAllFollowers(final String dbString) {
        return getFollowers(MobileGarrisonConstants.FOLLOWERS_URL, dbString);
    }

    /**
     * Returns a list of followers for a specific character that are inactive (currently not on mission)
     * @param tableString <b>(String)</b> - Unique Identifier for our Character
     * @return <b>(List)</b> - Returns a list of followers that are not currently on missions
     */
    public List<Follower> getInactiveFollowers(String tableString) {
        return getFollowers(MobileGarrisonConstants.FOLLOWERS_INACTIVE_URL, tableString);
    }

    /**
     * Returns our current list of Followers for a specific character
     *
     * @return <b>(List)</b> - List of followers for a specific character.
     */
    private List<Follower> getFollowers(String url, String dbString) {
        final AndroidHttpClient httpClient = AndroidHttpClient.newInstance("Android");
        final List<NameValuePair> params = new ArrayList<>(1);
        params.add(new BasicNameValuePair("unique_id", dbString));

        String paramString = URLEncodedUtils.format(params, "UTF-8");
        url = url + paramString;

        final HttpGet httpGet = new HttpGet(url);

        try {

            final HttpResponse response = httpClient.execute(httpGet);
            final InputStream inputStream = response.getEntity().getContent();

            //-- Convert the InputStream to a String
            final String rawString = getStringFromInputStream(inputStream);

            //-- Get a new JSONUtils object
            final JSONUtils jsonUtils = new JSONUtils();

            //-- Return the List of followers from the String
            return jsonUtils.getFollowers(rawString);
        } catch (final IOException e) {
            CustomLog.w(TAG, "IOException error: " + e.getMessage());
        } finally {
            httpClient.close();
        }

        return null;
    }

    /**
     * Returns a list of all of the Active Missions from our Active Missions API
     * @param tableString <b>(String)</b> - Unique ID for our character
     * @return <b>(List)</b> - List of active missions
     */
    public List<Mission> getActiveMissions(String tableString) {
        return getMissions(MobileGarrisonConstants.ACTIVE_MISSIONS_URL, tableString);
    }

    /**
     * Returns a list of all of the available missions from our Available Missions API
     * @param tableString <b>(String)</b> - Unique ID for our Character
     * @return <b>(List)</b> - List of available missions
     */
    public List<Mission> getAvailableMissions(String tableString) {
        return getMissions(MobileGarrisonConstants.AVAILABLE_MISSIONS_URL, tableString);
    }

    /**
     * Returns a list of Mission objects from the Missions API
     * @param url <b>(String)</b> - Url to retrieve specific missions from
     * @param tableString <b>(String)</b> - Table to select missions from. Unique Identifier
     *                    so that each character can have their own set of missions.
     * @return <b>(List)</b> - List of Mission objects
     */
    private List<Mission> getMissions(String url, String tableString) {

        //-- Create a new HttpClient
        AndroidHttpClient httpClient = AndroidHttpClient.newInstance("Android");

        //-- Add the parameters to the end of the HttpGet call
        List<NameValuePair> params = new ArrayList<>(1);
        params.add(new BasicNameValuePair("unique_id", tableString));

        String paramString = URLEncodedUtils.format(params, "UTF-8");
        url = url + paramString;

        final HttpGet httpGet = new HttpGet(url);

        try {

            final HttpResponse response = httpClient.execute(httpGet);
            final InputStream inputStream = response.getEntity().getContent();

            //-- Convert the InputStream to a String
            final String rawString = getStringFromInputStream(inputStream);
            //-- Get a new JSONUtils object
            final JSONUtils jsonUtils = new JSONUtils();

            //-- Return the List of followers from the String
            return jsonUtils.getMissions(rawString);
        } catch (final IOException e) {
            Log.w(TAG, "IOException error: " + e.getMessage());
        } finally {
            httpClient.close();
        }

        return null;
    }

    public WoWItem getWowItem(int itemId) {

        AndroidHttpClient httpClient = AndroidHttpClient.newInstance("Android");

        String url = MobileGarrisonConstants.API_ITEM + itemId;

        final HttpGet httpGet = new HttpGet(url);

        try {
            HttpResponse response = httpClient.execute(httpGet);
            InputStream inputStream = response.getEntity().getContent();

            String rawString = getStringFromInputStream(inputStream);
            JSONUtils jsonUtils = new JSONUtils();
            return jsonUtils.getWowItem(rawString);
        } catch (IOException e) {
            CustomLog.w(TAG, "IOException: " + e.getMessage());
        } finally {
            httpClient.close();
        }

        return null;
    }

    /**
     * Returns the List of WoWCharacter objects for the currently logged in user.
     * @return <b>(List)</b> - List of current WoWCharacters
     */
    public List<WoWCharacter> getCharacterList() {

        if (mCharacters != null) {
            return mCharacters;
        }

        List<WoWCharacter> characters = new ArrayList<>();

        final UserManager userManager = UserManager.getInstance(context);

        //-- If our UserManager has an access token, attempt to authenticate
        //-- against the Battle.net servers in order to retrieve
        //-- the list of characters.
        if (userManager.getAccessToken() != null) {

            //-- Add the parameters to the HttpGet call
            final List<NameValuePair> params = new ArrayList<>();
            params.add(new BasicNameValuePair("locale", "en_US"));
            params.add(new BasicNameValuePair("access_token",
                    userManager.getAccessToken().getAccessToken()));

            final String paramString = URLEncodedUtils.format(params, "UTF-8");
            final String url = getCharacterListUrl() + paramString;
            
            final HttpGet httpGet = new HttpGet(url);
            final AndroidHttpClient httpClient = AndroidHttpClient.newInstance("Android");

            try {
                final HttpResponse httpResponse = httpClient.execute(httpGet);

                //-- If we get a response from the server, attempt to parse it.
                if (httpResponse != null) {
                    final InputStream inputStream = httpResponse.getEntity().getContent();
                    final String rawString = getStringFromInputStream(inputStream);

                    final JSONUtils jsonUtils = new JSONUtils();

                    //-- Return the list of characters parsed from our JSONUtils class
                    characters = jsonUtils.getWoWCharacters(rawString);

                    //-- Sort the list of characters in ascending order from highest
                    //-- level to lowest.
                    Collections.sort(characters, new LevelSort());
                    mCharacters = characters;
                    return characters;

                }
            } catch (final IOException e) {
                CustomLog.w(TAG, "Error retrieving content: " + e.getMessage());
            } finally {
                httpClient.close();
            }
        }

        return characters;
    }

    private List<Double> getFollowerIds(List<Follower> followers) {

        List<Double> followerIds = new ArrayList<>();

        for (int i = 0; i < 3; i++) {
            followerIds.add(0.0);
        }

        for (int j = 0; j < followers.size(); j++) {
            Follower follower = followers.get(j);

            followerIds.add(j, follower.getId());
        }

        return followerIds;
    }

    /**
     * Returns a boolean value that is the response from the server once we have either
     * successfully started a mission or have failed.
     * @param dbString <b>(String)</b> - Database Access Token
     * @param mission <b>(Mission)</b> - Mission that we are attempting to start
     * @param followers <b>(List)</b> - List of followers associated with the mission.
     * @return <b>(long)</b> - Long value of when the start time of the mission is, in seconds.
     */
    public long startMission(String dbString, Mission mission, List<Follower> followers) {

        List<Double> followerIds = getFollowerIds(followers);

        long now = TimeUtils.getTimeNow();

        int hours = mission.getMissionDuration();

        long missionCompletionTime = TimeUtils.getMissionCompletedTimestamp(hours, 0, now);

        AndroidHttpClient httpClient = AndroidHttpClient.newInstance("Android");

        try {

            //-- Create a new HTTP Post to our Start Mission API
            HttpPost httpPost = new HttpPost(MobileGarrisonConstants.START_MISSION_URL);

            final List<NameValuePair> params = new ArrayList<>();
            params.add(new BasicNameValuePair("missionId", String.valueOf(mission.getMissionId())));
            params.add(new BasicNameValuePair("follower1", String.valueOf(followerIds.get(0))));
            params.add(new BasicNameValuePair("follower2", String.valueOf(followerIds.get(1))));
            params.add(new BasicNameValuePair("follower3", String.valueOf(followerIds.get(2))));
            params.add(new BasicNameValuePair("startTime", String.valueOf(now)));
            params.add(new BasicNameValuePair("endTime", String.valueOf(missionCompletionTime)));
            params.add(new BasicNameValuePair("successChance", String.valueOf(mission.getSuccessChance())));
            params.add(new BasicNameValuePair("uniqueId", dbString));

            //-- Set Post Parameters
            httpPost.setEntity(new UrlEncodedFormEntity(params));

            //-- Return the response
            HttpResponse response = httpClient.execute(httpPost);

            if (response != null) {
                InputStream is = response.getEntity().getContent();
                String rawString = getStringFromInputStream(is);

                Log.e(TAG, "Start Mission Result String: " + rawString);

                JSONObject jsonObject = new JSONObject(rawString);



                if (jsonObject.getBoolean("success")) {
                    return missionCompletionTime;
                } else {
                    return 0;
                }
            } else {
                Log.e(TAG, "No response from server");
                return 0;
            }

        } catch (IOException ioe) {
            CustomLog.v(TAG, "Error posting data: " + ioe.getMessage());
        } catch (JSONException e) {
            CustomLog.v(TAG, "Error parsing JSON: " + e.getMessage());
            e.printStackTrace();
        } finally {
            httpClient.close();
        }

        return 0;
    }

    /**
     * Return the Account ID for the currently logged in user.
     * @return <b>(String)</b> - Account ID (Unique) for the logged in user.
     */
    public String getAccountId() {

        final UserManager userManager = UserManager.getInstance(context);

        //-- If we have an account id, return it.
        if (userManager.getAccountId() != null) {
            return userManager.getAccountId();
        } else {

            final List<NameValuePair> params = new ArrayList<>();
            params.add(new BasicNameValuePair("access_token",
                    userManager.getAccessToken().getAccessToken()));

            final String paramString = URLEncodedUtils.format(params, "UTF-8");
            final String url = getAccountIdUrl() + paramString;

            final HttpGet httpGet = new HttpGet(url);
            final AndroidHttpClient httpClient = AndroidHttpClient.newInstance("Android");

            try {
                final HttpResponse httpResponse = httpClient.execute(httpGet);

                if (httpResponse != null) {
                    final InputStream inputStream = httpResponse.getEntity().getContent();
                    final String rawString = getStringFromInputStream(inputStream);

                    final JSONUtils jsonUtils = new JSONUtils();
                    final String accountId = jsonUtils.getAccountId(rawString);

                    userManager.setAccountId(accountId);

                    return accountId;
                }
            } catch (final IOException e) {
                CustomLog.w(TAG, "Error retrieving content: " + e.getMessage());
            } finally {
                httpClient.close();
            }
        }

        return null;
    }

    /**
     * Retrieve the account id from the Battle.net API for User Accounts
     * @return <b>(String)</b> - Account ID URL
     */
    private String getAccountIdUrl() {
        return MobileGarrisonConstants.BATTLE_NET_API_URL_US
                + MobileGarrisonConstants.BATTLE_NET_API_USER_ACCOUNT_ID;
    }

    /**
     * Retrieve the character list of the currently logged in user from
     * the Battle.net API for characters.
     * @return <b>(String)</b> - WoW Characters URL
     */
    private String getCharacterListUrl() {

        return MobileGarrisonConstants.BATTLE_NET_API_URL_US
                + MobileGarrisonConstants.BATTLE_NET_API_WOW_CHARACTERS;
    }

    /**
     * Returns the Spell Details for a specific ability
     *
     * @param followerAbility <b>(FollowerAbility)</b> - FollowerAbility for which details we are
     *                        attempting to retrieve
     * @return <b>(String)</b> - Battle.net API URL that will give us ability details
     */
    private String getAbilityUrl(final FollowerAbility followerAbility) {

        String url = MobileGarrisonConstants.FOLLOWER_ABILITY_URL;

        final List<NameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair("spellId", String.valueOf(followerAbility.getFollowerAbilityId())));

        final String paramString = URLEncodedUtils.format(params, "UTF-8");
        url = url + paramString;

        return url;
    }

    private String getMissionEnemyUrl(int enemyId) {
        String url = MobileGarrisonConstants.MISSION_ENEMY_URL;

        List<NameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair("enemyId", String.valueOf(enemyId)));

        String paramString = URLEncodedUtils.format(params, "UTF-8");
        url = url + paramString;

        return url;
    }

    /**
     * Returns the Spell Details for a specific ability
     *
     * @param followerTrait <b>(FollowerTrait)</b> - FollowerTrait for which details we are
     *                        attempting to retrieve
     * @return <b>(String)</b> - Battle.net API URL that will give us trait details
     */
    private String getTraitUrl(final FollowerTrait followerTrait) {

        String url = MobileGarrisonConstants.FOLLOWER_TRAIT_URL;

        final List<NameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair("traitId", String.valueOf(followerTrait.getFollowerTrait())));

        final String paramString = URLEncodedUtils.format(params, "UTF-8");
        url = url + paramString;

        return url;
    }


    /**
     * Sorts the levels of the different WoWCharacter objects based on the level of the
     * characters, in ascending order.
     */
    private class LevelSort implements Comparator<WoWCharacter> {

        public LevelSort() {
            super();
        }

        public int compare(final WoWCharacter character1, final WoWCharacter character2) {

            int level1 = character1.getLevel();
            int level2 = character2.getLevel();

            if (level1 < level2) {
                return 1;
            } if (level1 == level2) {
                return 0;
            } if (level1 > level2) {
                return -1;
            }

            return 0;
        }
    }
}


