package com.blizzard.mobilegarrison.datalayer.tasks.callable;

import android.net.http.AndroidHttpClient;

import com.blizzard.mobilegarrison.datalayer.manager.data.DataManager;
import com.blizzard.mobilegarrison.datalayer.objects.garrison.follower.AbilityDetails;
import com.blizzard.mobilegarrison.datalayer.objects.garrison.follower.FollowerAbility;
import com.blizzard.mobilegarrison.datalayer.utils.json.JSONUtils;

import org.apache.http.client.methods.HttpGet;

import java.io.InputStream;
import java.util.concurrent.Callable;

/**
 * Callable that is used to get the AbilityDetails for a specific FollowerAbility
 *
 * @author Joshua Hale
 */
public class GetAbilityDetailsCallable implements Callable<AbilityDetails> {

    private static final String TAG = "GetAbilityDetailsCallable";

    private final FollowerAbility followerAbility;
    private final String url;

    public GetAbilityDetailsCallable(final FollowerAbility followerAbility, final String url) {
        this.followerAbility = followerAbility;
        this.url = url;
    }

    @Override
    public AbilityDetails call() throws Exception {

        final HttpGet httpGet = new HttpGet(url);

        final AndroidHttpClient client = AndroidHttpClient.newInstance("Android");

        try {
            final InputStream inputStream = DataManager.getInputStream(client, httpGet);

            final String rawString = DataManager.getStringFromInputStream(inputStream);

            final JSONUtils jsonUtils = new JSONUtils();
            final AbilityDetails abilityDetails = jsonUtils.getAbilityDetails(followerAbility, rawString);

            if (abilityDetails != null) {
                return abilityDetails;
            }
        } finally {
            client.close();
        }

        return null;
    }
}