package com.blizzard.mobilegarrison.ui.garrison.follower;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.blizzard.mobilegarrison.R;
import com.blizzard.mobilegarrison.datalayer.constants.MobileGarrisonConstants;
import com.blizzard.mobilegarrison.datalayer.manager.bus.EventBus;
import com.blizzard.mobilegarrison.datalayer.objects.character.Quality;
import com.blizzard.mobilegarrison.datalayer.objects.garrison.follower.AbilityDetails;
import com.blizzard.mobilegarrison.datalayer.objects.garrison.follower.Follower;
import com.blizzard.mobilegarrison.datalayer.objects.garrison.follower.FollowerAbility;
import com.blizzard.mobilegarrison.datalayer.objects.garrison.follower.TraitDetails;
import com.blizzard.mobilegarrison.datalayer.tasks.async.GetAbilityDetailsTask;
import com.blizzard.mobilegarrison.datalayer.tasks.async.GetTraitsTask;
import com.blizzard.mobilegarrison.datalayer.tasks.results.GetAbilityDetailsEvent;
import com.blizzard.mobilegarrison.datalayer.tasks.results.GetErrorEvent;
import com.blizzard.mobilegarrison.datalayer.tasks.results.GetTraitDetailsEvent;
import com.blizzard.mobilegarrison.utils.character.CharacterUtils;
import com.blizzard.mobilegarrison.utils.color.ColorUtils;
import com.blizzard.mobilegarrison.utils.error.ErrorUtils;
import com.blizzard.mobilegarrison.utils.lazyload.ImageLoader;
import com.blizzard.mobilegarrison.widgets.FollowerDetailItemLevelLayout;
import com.squareup.otto.Subscribe;

import java.util.List;

/**
 * DetailFragment for a follower that shows the individual aspects of a Follower
 * from XP and rarity to traits and abilities.
 *
 * @author Joshua Hale
 */
public class FollowerDetailFragment extends Fragment {

    private Follower mFollower;
    private Views mViews;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent,
                             Bundle savedInstanceState) {

        getActivity().setTitle(getString(R.string.garrison));

        View view = inflater.inflate(R.layout.fragment_follower_detail, parent, false);

        mViews = new Views(view);

        init();

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getInstance().register(this);
    }

    private void init() {

        Bundle args = getArguments();

        if (args != null) {
            Follower follower = (Follower) args.getSerializable(getString(R.string.argument_follower));

            if (follower != null) {
                mFollower = follower;
                List<FollowerAbility> mFollowerAbilities = follower.getFollowerAbilities();
                setupFollower();

                new GetAbilityDetailsTask(getActivity(), mFollowerAbilities).execute();
                new GetTraitsTask(getActivity(), follower.getFollowerTraits()).execute();
            } else {
                Toast.makeText(getActivity(),
                        "No follower data, please try again later", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void setupFollower() {

        CharacterUtils charUtils = new CharacterUtils(getActivity());

        addFollowerImage(mFollower.getFollowerImageUri(), mFollower.getQuality());
        addFollowerName(mFollower.getFollowerName(), mFollower.getQuality());
        setupXpBar();

        mViews.followerClassImageView.setImageResource(charUtils.getClassIcon(mFollower.getFollowerClass()));
        mViews.followerClassTextView.setText(charUtils.getCharacterClassName(mFollower.getFollowerClass()));
        mViews.followerLevelTextView.setText(String.valueOf(mFollower.getFollowerLevel()));

        if (mFollower.getFollowerLevel() == 100) {
            FollowerDetailItemLevelLayout itemDetailLayout = new
                    FollowerDetailItemLevelLayout(getActivity(), mFollower);

            mViews.itemLevelLinearLayout.addView(itemDetailLayout);
        }
    }

    private void setupXpBar() {

        if (mFollower.getFollowerLevel() == 100 && mFollower.getQuality() == Quality.EPIC) {
            mViews.followerXpBar.setVisibility(View.GONE);
            mViews.followerXpLinearLayout.setVisibility(View.GONE);
        } else {

            if (mFollower.getFollowerLevel() == 100) {
                mViews.followerUpgradeTextView.setText(getString(R.string.to_next_upgrade));
            } else {
                mViews.followerUpgradeTextView.setText(getString(R.string.to_next_level));
            }

            int currentXp = mFollower.getFollowerCurrentXp();
            int totalXp = mFollower.getFollowerTotalXp();

            int xpDiff = totalXp - currentXp;
            mViews.followerXpTextView.setText(String.valueOf(xpDiff));

            if (currentXp == 0) {
                mViews.followerXpBar.setProgress(0);
            } else {
                mViews.followerXpBar.setProgress(getFollowerXp(currentXp, totalXp));
            }
        }
    }

    /**
     * Returns the integer to put in the ProgressBar in the
     * @param currentXP <b>(int)</b> - XP value returned from the server that determines the
     *                  amount of XP left before the ProgressBar to be filled
     * @return <b>(int)</b> - Integer to put into the ProgressBar
     */
    private int getFollowerXp(int currentXP, int totalXp) {
        return (int) Math.round((totalXp - currentXP) * 100.0 / 100000);
    }


    private void addFollowerImage(String params, Quality quality) {

        ImageLoader imageLoader = new ImageLoader();

        imageLoader.displayImage(getFollowerUrl(params), mViews.followerIconImageView);

        //-- Changes the background of the icon for the follower and the
        //-- background of the follower level based on the quality of the follower

        switch (quality) {
            case UNCOMMON:
                mViews.followerIconImageView.setBackgroundResource(R.drawable.background_item_uncommon);
                mViews.followerLevelTextView.setBackgroundResource(R.drawable.background_item_uncommon_no_border);
                break;
            case RARE:
                mViews.followerIconImageView.setBackgroundResource(R.drawable.background_item_rare);
                mViews.followerLevelTextView.setBackgroundResource(R.drawable.background_item_rare_no_border);
                break;
            case EPIC:
                mViews.followerIconImageView.setBackgroundResource(R.drawable.background_item_epic);
                mViews.followerLevelTextView.setBackgroundResource(R.drawable.background_item_epic_no_border);
                break;
            case LEGENDARY:
                mViews.followerIconImageView.setBackgroundResource(R.drawable.background_item_legendary);
                mViews.followerLevelTextView.setBackgroundResource(R.drawable.background_item_legendary_no_border);
                break;
        }
    }

    private void addFollowerName(String name, Quality quality) {

        mViews.followerNameTextView.setText(name);
        mViews.followerNameTextView.setTextColor(ColorUtils.getQualityColor(quality));
    }

    /**
     * Updates the UI after retrieving the List of Ability Details
     * @param abilityDetails <b>(List)</b> - Ability Details
     */
    private void updateAbilitiesUI(List<AbilityDetails> abilityDetails) {

        for (AbilityDetails abilityDetail : abilityDetails) {
            addFollowerAbilityView(abilityDetail);
        }
        
    }

    private void updateTraitsUI(List<TraitDetails> traitDetails) {

        for (TraitDetails traitDetail : traitDetails) {
            addFollowerTraitView(traitDetail);
        }
    }

    private void addFollowerAbilityView(AbilityDetails abilityDetails) {

        View abilityView = getActivity().getLayoutInflater().
                inflate(R.layout.follower_detail_ability, mViews.abilitiesLinearLayout, false);

        ImageView spellIconImageView = (ImageView) abilityView.findViewById(R.id.spellIconImageView);
        ImageView counterIconImageView = (ImageView) abilityView.findViewById(R.id.counterIconImageView);
        LinearLayout counterLinearLayout = (LinearLayout) abilityView.findViewById(R.id.counterLinearLayout);
        TextView spellNameTextView = (TextView) abilityView.findViewById(R.id.spellNameTextView);
        TextView spellDescriptionTextView = (TextView) abilityView.findViewById(R.id.spellDescriptionTextView);

        counterIconImageView.setImageResource(abilityDetails.getCounterIconId());
        spellIconImageView.setImageResource(abilityDetails.getSpellIconId());
        spellDescriptionTextView.setText(abilityDetails.getDescription());
        spellNameTextView.setText(abilityDetails.getName());

        if (abilityDetails.getCounterIconId() == 0) {
            counterLinearLayout.setVisibility(View.GONE);
        }

        mViews.abilitiesLinearLayout.addView(abilityView);
    }

    private void addFollowerTraitView(TraitDetails traitDetails) {
        View abilityView = getActivity().getLayoutInflater().
                inflate(R.layout.follower_detail_trait, mViews.traitsLinearLayout, false);

        ImageView spellIconImageView = (ImageView) abilityView.findViewById(R.id.spellIconImageView);
        TextView spellNameTextView = (TextView) abilityView.findViewById(R.id.spellNameTextView);
        TextView spellDescriptionTextView = (TextView) abilityView.findViewById(R.id.spellDescriptionTextView);

        spellIconImageView.setImageResource(traitDetails.getIconResId());
        spellDescriptionTextView.setText(traitDetails.getDescription());
        spellNameTextView.setText(traitDetails.getName());

        mViews.traitsLinearLayout.addView(abilityView);
    }

    private String getFollowerUrl(String params) {
        return MobileGarrisonConstants.FOLLOWERS_IMAGE_URL + params;
    }

    @Subscribe
    public void getAbilityDetails(GetAbilityDetailsEvent event) {
        updateAbilitiesUI(event.getResult());
    }

    @Subscribe
    public void getTraitDetails(GetTraitDetailsEvent event) {
        updateTraitsUI(event.getResult());
    }

    @Subscribe
    public void onError(GetErrorEvent event) {
        String message = event.getMessage();
        boolean showDialog = event.showDialog();

        ErrorUtils.showMessage(getActivity(), message, showDialog);
    }

    @Override
    public void onStop() {
        EventBus.getInstance().unregister(this);
        super.onStop();
    }

    static class Views {

        final ImageView followerClassImageView;
        final ImageView followerIconImageView;
        final LinearLayout abilitiesLinearLayout;
        final LinearLayout followerXpLinearLayout;
        final LinearLayout itemLevelLinearLayout;
        final LinearLayout traitsLinearLayout;
        final ProgressBar followerXpBar;
        final TextView followerClassTextView;
        final TextView followerLevelTextView;
        final TextView followerNameTextView;
        final TextView followerUpgradeTextView;
        final TextView followerXpTextView;

        public Views(View view) {

            followerClassImageView = (ImageView) view.findViewById(R.id.followerClassImageView);
            followerIconImageView = (ImageView) view.findViewById(R.id.followerIconImageView);
            abilitiesLinearLayout = (LinearLayout) view.findViewById(R.id.abilitiesLinearLayout);
            followerXpLinearLayout = (LinearLayout) view.findViewById(R.id.followerXpLinearLayout);
            itemLevelLinearLayout = (LinearLayout) view.findViewById(R.id.itemLevelLinearLayout);
            traitsLinearLayout = (LinearLayout) view.findViewById(R.id.traitsLinearLayout);
            followerXpBar = (ProgressBar) view.findViewById(R.id.follower_xp_bar);
            followerClassTextView = (TextView) view.findViewById(R.id.followerClassTextView);
            followerLevelTextView = (TextView) view.findViewById(R.id.followerLevelTextView);
            followerNameTextView = (TextView) view.findViewById(R.id.followerNameTextView);
            followerUpgradeTextView = (TextView) view.findViewById(R.id.followerUpgradeTextView);
            followerXpTextView = (TextView) view.findViewById(R.id.followerXpTextView);
        }
    }
}
