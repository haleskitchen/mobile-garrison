package com.blizzard.mobilegarrison.constants;

/**
 * Since we are using the same adapter for the two missions types, we pass in the
 * enum type to the MissionsAdapter to determine some of the UI differences that we will have.
 * For instance, the available missions has an onClickListener, while the active missions do not.
 * Also the active missions display the time remaining until the mission is completed where
 * the available missions show the time it would take to complete it.
 *
 * @author Joshua Hale (josh@joshuahale.com)
 */
public enum MissionsFragmentType {
    ACTIVE,
    AVAILABLE
}
