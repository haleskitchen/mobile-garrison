package com.blizzard.mobilegarrison.datalayer.objects.character;

/**
 * Enum that determines the quality of an item or Follower
 *
 * @author Joshua Hale
 */
public enum Quality {

    UNCOMMON(1),
    RARE(2),
    EPIC(3),
    LEGENDARY(4);

    private final int followerQuality;

    private Quality(final int followerQuality) {
        this.followerQuality = followerQuality;
    }

    private int getFollowerQuality() {
        return this.followerQuality;
    }

    public static Quality getEnum(final int value) {
        for (final Quality quality : values()) {
            if(value == quality.getFollowerQuality()) {
                return quality;
            }
        }
        return null;
    }
}
