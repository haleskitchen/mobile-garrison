package com.blizzard.mobilegarrison.ui.base;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Spinner;

import com.blizzard.mobilegarrison.R;
import com.blizzard.mobilegarrison.adapters.DrawerAdapter;
import com.blizzard.mobilegarrison.adapters.NavigationAdapter;
import com.blizzard.mobilegarrison.datalayer.interfaces.OnCharactersLoadedListener;
import com.blizzard.mobilegarrison.datalayer.manager.user.UserManager;
import com.blizzard.mobilegarrison.datalayer.objects.character.WoWCharacter;
import com.blizzard.mobilegarrison.datalayer.objects.listener.OnCharacterChangedListener;
import com.blizzard.mobilegarrison.datalayer.tasks.async.GetCharacterListTask;
import com.blizzard.mobilegarrison.ui.garrison.GarrisonActivity;
import com.blizzard.mobilegarrison.ui.settings.SettingsActivity;
import com.blizzard.mobilegarrison.widgets.general.WoWDialog;

import java.util.List;

/**
 * BaseActivity for MobileGarrison extends an ActionBarActivity so we can use the ActionBar
 * throughout the application. Most Activities will extend the BaseActivity so that we can keep
 * track of the OnCharacterChangedListener. This listener listens for changes when the user has
 * switched characters from the Toolbar Drawer. This allows for the UI to update instantaneously
 * without having to make the user go back to some other screen and then return.
 */
public class BaseActivity extends ActionBarActivity implements OnCharacterChangedListener {

    //-- Protected so that other activities can access the toolbar directly
    //-- and manipulate things at will
    protected Toolbar mToolbar;

    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerAdapter mDrawerAdapter;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private List<WoWCharacter> mCharacters;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_base);

        init();
    }

    @Override
    public void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    /**
     * Initializes most of the shared widgets across all of our activities.
     */
    protected void init() {

        //-- Different Views that are used for Navigation
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);

        mToolbar.setBackgroundColor(getResources().getColor(R.color.horde_red));
        mToolbar.setTitleTextColor(Color.WHITE);

        mDrawerAdapter = new DrawerAdapter(this);
        mDrawerList.setAdapter(mDrawerAdapter);

        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Class nextClass;

                switch (position) {

                    case 1:
                        nextClass = GarrisonActivity.class;
                        break;
                    case 9:
                        nextClass = SettingsActivity.class;
                        break;
                    default:
                        nextClass = null;
                        break;
                }

                startNextActivity(nextClass);
            }
        });

        if (mToolbar != null) {
            setSupportActionBar(mToolbar);
        }

        initDrawer();

        final UserManager userManager = UserManager.getInstance(this);

        List<WoWCharacter> characters = userManager.getCurrentWoWCharacters();

        if (characters != null) {
            mCharacters = characters;
            initDrawerHeader();
        } else {
            OnCharactersLoadedListener listener = new OnCharactersLoadedListener() {
                @Override
                public void onCharactersLoadedListener(List<WoWCharacter> characters) {
                    mCharacters = characters;
                    UserManager.getInstance(BaseActivity.this).setCharacters(mCharacters);

                    initDrawerHeader();
                }
            };
            new GetCharacterListTask(this, listener).execute();
        }
    }

    /**
     * Initializes the drawer on the left hand side of the menu.
     */
    private void initDrawer() {

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                mToolbar, R.string.drawer_open, R.string.drawer_closed) {
        };

        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

    /**
     * Initializes the Drawer header, by adding the drop down of the list of characters
     * for the currently logged in account. The user can change characters at will
     * and the section of the application they are in will reflect the change immediately.
     */
    private void initDrawerHeader() {

        //-- We check this so that if our BaseActivity has already been called
        //-- we do not add another header view to it.
        if (mDrawerList.getHeaderViewsCount() == 0) {

            final LayoutInflater inflater = getLayoutInflater();
            final ViewGroup header = (ViewGroup) inflater.inflate(R.layout.header_navigation,
                    mDrawerList, false);

            final Spinner navigationSpinner = (Spinner) header.
                    findViewById(R.id.spinner_character_select);

            final NavigationAdapter navigationAdapter = new NavigationAdapter(
                    this, mCharacters, false);

            navigationSpinner.setAdapter(navigationAdapter);

            setCurrentSelection(navigationSpinner);

            //-- The spinner has a post so that the onItemSelectedListener
            //-- only gets called when the user interacts with it from the UI
            //-- thread. A bug can occur where the item selected is consistently
            //-- the zero item in the AdapterView, and always sets the
            //-- current character to the zero character.
            navigationSpinner.post(new Runnable() {
                public void run() {
                    navigationSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(final AdapterView<?> parent, final View view,
                                                   final int position, final long id) {
                            final WoWCharacter wowCharacter = mCharacters.get(position);
                            onCharacterChanged(wowCharacter);
                            mDrawerLayout.closeDrawers();
                        }

                        @Override
                        public void onNothingSelected(final AdapterView<?> parent) {

                        }
                    });
                }
            });

            //-- When first introducted this method could only be called before setting the
            //-- adapter with setAdapter(ListAdapter). Starting with KITKAT, this method
            //-- may be called at any time. If the ListView's adapter does not extend
            //-- HeaderViewListAdapter, it will be wrapped with a supporting instance
            //-- of WrapperListAdapter.
            int currentApiVersion = android.os.Build.VERSION.SDK_INT;

            //-- This means that we have to check the version before KITKAT and apply
            //-- the other fix, of setting the adapter to null, then adding the HeaderView
            //-- and reapplying the adapter afterwards.
            if (currentApiVersion <= Build.VERSION_CODES.JELLY_BEAN_MR2) {
                mDrawerList.setAdapter(null);
                mDrawerList.addHeaderView(header);
                mDrawerList.setAdapter(mDrawerAdapter);
            } else {
                mDrawerList.addHeaderView(header);
            }
        }
    }

    /**
     * Sets the current selection of the NavigationSpinner to the currently selected
     * WoWCharacter by the user.
     * @param navigationSpinner <b>(Spinner)</b> - Navigation Spinner from the Drawer Layout
     */
    private void setCurrentSelection(Spinner navigationSpinner) {

        UserManager userManager = UserManager.getInstance(this);
        WoWCharacter wowCharacter = userManager.getCurrentWoWCharacter();

        int index = mCharacters.indexOf(wowCharacter);

        navigationSpinner.setSelection(index);
    }

    /**
     * Starts the next Activity. <br />
     * - If the next class is null, we haven't implemented the content, and shows a dialog. <br />
     * - If the class it calls is the same as the calling class, close the drawer. <br />
     * - Else, start the next activity
     * @param nextClass <b>(Class)</b> - Class selected by the user from the Navigation Spinner
     */
    private void startNextActivity(Class nextClass) {

        mDrawerLayout.closeDrawers();

        if (nextClass == null) {
            WoWDialog wowDialog = new WoWDialog(this, "This content is not yet available");
            wowDialog.show();
        } else if (this.getClass() != nextClass) {
            startActivity(new Intent(this, nextClass));
        }
    }

    /**
     * Set the status bar color to the color picked. This is for Android 5.0 and up
     * where Material allows us to change the color of the status bar to fit our design
     * @param resId <b>(int)</b> - Resource ID of the color chosen
     */
    protected void setStatusBarColor(int resId) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(resId);
        }
    }

    /**
     * Returns the DrawerLayout. Used by the other parts of the application to close
     * the drawer when needed
     * @return <b>(DrawerLayout)</b> - DrawerLayout for the BaseActivity
     */
    public DrawerLayout getDrawerLayout() {
        return mDrawerLayout;
    }

    /**
     * When the user changes the character, the Activity updates the UserManager
     * with the currently selected character.
     * @param wowCharacter <b>(WoWCharacter)</b> - User currently selected WoW Character
     */
    @Override
    public void onCharacterChanged(WoWCharacter wowCharacter) {
        final UserManager userManager = UserManager.getInstance(this);
        userManager.setCurrentWoWCharacter(wowCharacter);
    }

    @Override
    public void onBackPressed() {

        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawers();
        } else {
            super.onBackPressed();
        }
    }
}
