package com.blizzard.mobilegarrison.ui.garrison;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;

import com.blizzard.mobilegarrison.R;
import com.blizzard.mobilegarrison.datalayer.objects.character.WoWCharacter;
import com.blizzard.mobilegarrison.ui.base.BaseActivity;
import com.blizzard.mobilegarrison.ui.base.BaseFragment;

/**
 * Activity that holds the GarrisonFragment and manages the lifecycle.
 *
 * @author Joshua Hale
 */
public class GarrisonActivity extends BaseActivity {

    private Fragment currentFragment;

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_base);
        init();
    }

    @Override
    protected void init() {
        super.init();

        mToolbar.setBackgroundResource(R.drawable.actionbar_bg_garrison);

        setStatusBarColor(getResources().getColor(R.color.wow_brown_secondary));

        Fragment fragment = new GarrisonFragment();
        currentFragment = fragment;

        final FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.container, fragment, "previous");
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public void onCharacterChanged(final WoWCharacter wowCharacter) {
        super.onCharacterChanged(wowCharacter);

        if (currentFragment instanceof BaseFragment) {
            BaseFragment fragment = (BaseFragment) currentFragment;
            fragment.onCharacterChanged(wowCharacter);
        }
    }

    @Override
    public void setTitle(CharSequence title) {
        super.setTitle(title);

        mToolbar.setTitle(title);
    }
}
