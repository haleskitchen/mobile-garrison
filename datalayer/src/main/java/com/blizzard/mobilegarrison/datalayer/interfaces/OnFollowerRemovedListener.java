package com.blizzard.mobilegarrison.datalayer.interfaces;

import com.blizzard.mobilegarrison.datalayer.objects.garrison.follower.Follower;

/**
 * This interface is used to update the UI of the MissionDetailFragment when a follower
 * has been removed from the mission. Since the followers are in their own widget
 * in that Fragment, this listener tells the MissionDetailFragment that one of the
 * already selected followers for the mission has been removed and the UI should be
 * updated appropriately.
 *
 * @author Joshua Hale (josh@joshuahale.com)
 */
public interface OnFollowerRemovedListener {

    public void onFollowerRemoved(Follower follower);
}
