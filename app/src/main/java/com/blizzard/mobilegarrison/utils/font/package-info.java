/**
 * Handles the different custom fonts within the application
 *
 * @version 1.0
 * @since 1.0
 */
package com.blizzard.mobilegarrison.utils.font;