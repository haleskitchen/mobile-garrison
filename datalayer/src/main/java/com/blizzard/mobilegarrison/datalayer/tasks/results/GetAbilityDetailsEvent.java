package com.blizzard.mobilegarrison.datalayer.tasks.results;

import com.blizzard.mobilegarrison.datalayer.objects.garrison.follower.AbilityDetails;

import java.util.List;

/**
 * Otto Event that is fired when {@link com.blizzard.mobilegarrison.datalayer.tasks.async.GetAbilityDetailsTask}
 * returns a list of Ability Details
 *
 * @author Joshua Hale
 */
public class GetAbilityDetailsEvent {

    private final List<AbilityDetails> abilityDetails;

    public GetAbilityDetailsEvent(List<AbilityDetails> abilityDetails) {
        this.abilityDetails = abilityDetails;
    }

    public List<AbilityDetails> getResult() {
        return abilityDetails;
    }
}
