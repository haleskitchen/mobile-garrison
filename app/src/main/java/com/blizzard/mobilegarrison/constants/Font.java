package com.blizzard.mobilegarrison.constants;

/**
 * Enum used by our FontUtils to determine which font to showcase for a particular
 * TextView or ViewGroup
 *
 * @author Joshua Hale
 */
public enum Font {
    ARIAL_NARROW,
    FRIZQT,
    MORPHEUS
}
