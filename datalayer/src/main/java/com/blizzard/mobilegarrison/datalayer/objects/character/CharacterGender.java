package com.blizzard.mobilegarrison.datalayer.objects.character;

/**
 * Enum that determines Character Gender
 */
public enum CharacterGender {
    MALE,
    FEMALE
}
