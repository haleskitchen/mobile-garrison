/**
 * Manages the parsing of different JSON files to return
 * objects needed for the application
 *
 * @version 1.0
 * @since 1.0
 */
package com.blizzard.mobilegarrison.datalayer.utils.json;