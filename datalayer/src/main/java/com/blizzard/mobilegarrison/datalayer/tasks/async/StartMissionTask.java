package com.blizzard.mobilegarrison.datalayer.tasks.async;

import android.content.Context;
import android.os.AsyncTask;

import com.blizzard.mobilegarrison.datalayer.constants.ErrorStrings;
import com.blizzard.mobilegarrison.datalayer.manager.bus.EventBus;
import com.blizzard.mobilegarrison.datalayer.manager.data.DataManager;
import com.blizzard.mobilegarrison.datalayer.manager.user.UserManager;
import com.blizzard.mobilegarrison.datalayer.objects.garrison.follower.Follower;
import com.blizzard.mobilegarrison.datalayer.objects.garrison.mission.Mission;
import com.blizzard.mobilegarrison.datalayer.tasks.results.GetErrorEvent;
import com.blizzard.mobilegarrison.datalayer.tasks.results.StartMissionEvent;

import java.util.List;

/**
 * Once the validation has completed for a mission (the success chance has been calculated,
 * the number of followers is correct, etc.), this task is fired. This task takes
 * the appropriated followers and mission and posts the values needed to the backend. <br /><br />
 *
 * If the mission was successfully added, a 200 response will be given back from the server
 * and the calling activity will be finished. On top of that, the CurrentMissionsFragment (if
 * it is still registered with the EventBus) will update to show the change in the list.
 *
 * @author Joshua Hale (josh@joshuahale.com)
 */
public class StartMissionTask extends AsyncTask<Void, Void, Long> {

    private final Context mContext;
    private final Mission mMission;
    private final List<Follower> mFollowers;

    /**
     * Constructor that gets called by an Activity or Fragment
     * @param context <b>(Context)</b>  - Context, needed for our DataManager
     * @param mission <b>(Mission)</b> - Mission to be started
     * @param followers <b>(List)</b> - List of followers to be added to the mission.
     */
    public StartMissionTask(Context context, Mission mission, List<Follower> followers) {
        mContext = context;
        mFollowers = followers;
        mMission = mission;
    }

    @Override
    public Long doInBackground(Void... params) {
        String dbString = UserManager.getInstance(mContext).getDatabaseString();
        DataManager dataManager = DataManager.getInstance(mContext);

        return dataManager.startMission(dbString, mMission, mFollowers);
    }

    @Override
    public void onPostExecute(Long missionStartTime) {

        if (missionStartTime == null || missionStartTime == 0) {
            EventBus.getInstance().post(new GetErrorEvent(
                    ErrorStrings.ERROR_MISSION_NOT_STARTED, false));
        } else {
            EventBus.getInstance().post(new StartMissionEvent(missionStartTime));
        }
    }
}
