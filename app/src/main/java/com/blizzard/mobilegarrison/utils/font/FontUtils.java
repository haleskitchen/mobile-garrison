package com.blizzard.mobilegarrison.utils.font;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ScaleXSpan;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.blizzard.mobilegarrison.R;
import com.blizzard.mobilegarrison.constants.Font;
import com.blizzard.mobilegarrison.datalayer.utils.logging.CustomLog;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;

/**
 * Font Utility class used to get different fonts from the assets directory of SMALibrary <br />
 * Different fonts will be used by different brands, and this is the accessor for all of those fonts. <br />
 * The developer can also add a specific typeface to a TextView, which will also help the kerning of the text
 * once the typeface has been added to the TextView
 * @author qxg6106 (Joshua.Hale@partner.bmwgroup.com)
 *
 */
public class FontUtils {

    private static final String TAG = "FontUtils";

    private final Context context;

    /**
     * Font Utils needs an Activity Context in order to retrieve assets from the directory
     * @param context <b>(Context)</b> - Activity context
     */
    public FontUtils(final Context context) {
        this.context = context;
    }


    public void setTypeface(Font font, ViewGroup parent) {
        Typeface typeface = getTypeface(font);

        if (typeface != null) {
            setTypeface(typeface, parent);
        }
    }

    public void setTypeface(Font font, TextView textView) {

        Typeface typeface = getTypeface(font);

        if (typeface != null) {
            setTypeface(typeface, textView);
        }
    }

    private Typeface getTypeface(Font font) {

        switch (font) {
            case ARIAL_NARROW:
                return getArialNarrowFont();
            case FRIZQT:
                return getFrizQTFont();
            case MORPHEUS:
                return getMorpheusFont();
            default:
                return null;
        }
    }

    /**
     * Sets a typeface for a specific TextView
     * @param typeface <b>(Typeface)</b> - Typeface to be set
     * @param view <b>(TextView)</b> - TextView where typeface will be set
     */
    private void setTypeface(final Typeface typeface, final TextView view) {
        view.setTypeface(typeface);
        view.setPaintFlags(view.getPaintFlags() | Paint.SUBPIXEL_TEXT_FLAG);
    }

    /**
     * Sets a typeface for all the TextViews in a ViewGruop
     * @param typeface <b>(Typeface)</b> - Typeface to be set
     * @param parent <b>(ViewGroup)</b> - Group of views that will be searched and set accordingly
     */
    private void setTypeface(final Typeface typeface, final ViewGroup parent) {
        for(int i = 0; i < parent.getChildCount(); i++) {
            final View v = parent.getChildAt(i);
            if(v instanceof ViewGroup) {
                setTypeface(typeface, (ViewGroup) v);
            } else if(v instanceof TextView) {
                TextView tv = (TextView)v;
                setTypeface(typeface, tv);
            }
        }
    }

    /**
     * Change the letter spacing on a specific text view by a given value
     * @param textView <b>(TextView)</b> - TextView with string already populated to add spacing between letters
     * @param letterSpacing <b>(float)</b> - Value for spacing out the individual letters
     */
    public void setLetterSpacing(final TextView textView, final float letterSpacing) {
        applyLetterSpacing(textView, letterSpacing);
    }

    /**
     * Sets text to upper case font
     * @param view <b>(TextView)</b> - TextView with lowercase text
     * @param lowerCase <b>(String)</b> - String to be capitalized
     */
    public void setFontUpperCase(final TextView view, final String lowerCase) {

        final String upperCase = lowerCase.toUpperCase(Locale.getDefault());

        view.setText(upperCase);
    }

    /**
     * Application of spacing algorithm to TextView
     * @param textView <b>(TextView)</b> - TextView (with text) passed in by developers
     * @param letterSpacing <b>(float)</b> - Float value of spacing between characters
     */
    private void applyLetterSpacing(final TextView textView, final float letterSpacing) {

        //-- Grab the original text from the TextView
        final String originalText = textView.getText().toString();

        //-- Create a new string builder, it's the interface for building the new string data
        final StringBuilder sb = new StringBuilder();

        //-- This loop involves looping through each character
        //-- Adding it to the StringBuilder object and
        //-- Then adding a non-breaking space (this is what determines the letter spacing)
        for(int i = 0; i < originalText.length(); i++) {
            final String c = "" + originalText.charAt(i);
            sb.append(c);
            if(i + 1 < originalText.length()) {
                sb.append("\u00A0");
            }
        }

        //-- Create a new Spannable String after the final string has
        //-- had spaces added to it
        final SpannableString finalText = new SpannableString(sb.toString());

        //-- If the length is greater than one, add the ScaleXSpan to the
        //-- letters based on a coefficient of the value determined by the developer
        if(sb.toString().length() > 1) {
            for(int i = 1; i < sb.toString().length(); i += 2) {
                finalText.setSpan(new ScaleXSpan((letterSpacing + 1) / 10), i, i + 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            }
        }

        //-- Sets the text to the textview with a Spannable Buffer Type
        //-- Which allows the TextView to understand there is space between letters
        textView.setText(finalText, TextView.BufferType.SPANNABLE);
    }

    /**
     * Different default values to determine space between letters <br />
     * Doesn't have to be used, but these are the defaults.
     * @author qxg6106 (Joshua.Hale@partner.bmwgroup.com)
     */
    private class LetterSpacing {
        public final static float NORMAL = 0;
        public final static float SMALL = 4;
        public final static float MEDIUM = (float)5;
        public final static float LARGE = (float)6;
        public final static float XLARGE = (float)7;
    }

    /**
     * Returns a specified Typeface from our res/raw directory that houses all of the
     * different Typefaces are stored for the application.
     * @param resource <b>(int)</b> - Resource identifier for font located within res/raw directory
     * @return <b>(Typeface)</b> - Typeface returned from the resource identifier
     */
    private Typeface getFontFromResource(final int resource) {
        Typeface tf = null;

        final InputStream is = context.getResources().openRawResource(resource);
        final String outPath = context.getCacheDir() + "/tmp" + System.currentTimeMillis() + ".raw";

        try {
            final byte[] buffer = new byte[is.available()];

            final BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(outPath));

            int l;
            while ((l = is.read(buffer)) > 0) {
                bos.write(buffer, 0, l);
            }

            bos.close();

            tf = Typeface.createFromFile(outPath);

            new File(outPath).delete();
        } catch(IOException e) {
            CustomLog.v(TAG, "Error reading in font");
        }

        return tf;
    }

    Typeface getFrizQTFont() {
        return getFontFromResource(R.raw.friz_qt);
    }

    Typeface getMorpheusFont() {
        return getFontFromResource(R.raw.morpheus);
    }

    Typeface getArialNarrowFont() {
        return getFontFromResource(R.raw.arial_narrow);
    }
}

