# README #

This is for the POC version of the MobileGarrison application for World of Warcraft: Warlords of Draenor

### What is this repository for? ###

* UI enhancement from original WoW Armory application
* Should showcase the ability of the Garrison on a mobile platform
* v0.1

### How do I get set up? ###

* Download Android Studio
* Download Project
* Import project into Android Studio
* Run

### Contribution guidelines ###

* Thanks to Battle.net API for allowing the configuration of the application for OAuth 2.0
* Thanks to Blizzard for letting me retrieve texture assets from the game