/**
 * This package is used just for our LoginActivity. The
 * LoginActivity is a simple wrapper object for our
 * {@link com.blizzard.mobilegarrison.ui.login.fragments.LoginFragment
 *
 * @since 1.0
 * @version 1.0
 */
package com.blizzard.mobilegarrison.ui.login.activities;