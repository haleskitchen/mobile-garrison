package com.blizzard.mobilegarrison.datalayer.objects.garrison.mission;

/**
 * Enum that is used to determine which type of reward we are receiving for a given mission
 * whether it be gold, experience, garrison resources or an item
 *
 * @author Joshua Hale (josh@joshuahale.com)
 */
public enum MissionRewardType {
    EXPERIENCE,
    GARRISON_RESOURCES,
    GOLD,
    ITEM
}
