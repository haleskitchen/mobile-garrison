package com.blizzard.mobilegarrison.datalayer.parser.garrison;

import android.util.Log;

import com.blizzard.mobilegarrison.datalayer.objects.character.Faction;
import com.blizzard.mobilegarrison.datalayer.objects.character.Quality;
import com.blizzard.mobilegarrison.datalayer.objects.garrison.mission.Mission;
import com.blizzard.mobilegarrison.datalayer.objects.garrison.mission.MissionReward;
import com.blizzard.mobilegarrison.datalayer.objects.garrison.mission.MissionRewardType;
import com.blizzard.mobilegarrison.datalayer.objects.garrison.mission.MissionType;
import com.blizzard.mobilegarrison.datalayer.objects.garrison.mission.MissionZone;
import com.blizzard.mobilegarrison.datalayer.utils.logging.CustomLog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


/**
 * Uses standard JSON parsing in order to retrieve a list of missions. GSON
 * could also be used in lieu of manually parsing this. The goal of this was
 * to show that even if there is a library to parse this type of information,
 * it can also be done manually.
 *
 * @author Joshua Hale
 */
public class MissionsParser {
    
    private static final String FIELD_AVAILABLE_DAYS = "availableDays";
    private static final String FIELD_AVAILABLE_HOURS = "availableHours";
    private static final String FIELD_END_TIME = "endTime";
    private static final String FIELD_FACTION = "faction";
    private static final String FIELD_FOLLOWER_LIMIT = "followerLimit";
    private static final String FIELD_GAINS_GOLD = "gainsGold";
    private static final String FIELD_GAINS_XP = "gainsXp";
    private static final String FIELD_LOCATION = "location";
    private static final String FIELD_MISSION_CATEGORY = "missionCategory";
    private static final String FIELD_MISSION_DESCRIPTION = "missionDescription";
    private static final String FIELD_MISSION_DURATION = "missionDuration";
    private static final String FIELD_MISSION_ENCOUNTER1 = "missionEncounter1";
    private static final String FIELD_MISSION_ENCOUNTER2 = "missionEncounter2";
    private static final String FIELD_MISSION_ENCOUNTER3 = "missionEncounter3";
    private static final String FIELD_MISSION_ENVIRONMENT = "missionEnvironment";
    private static final String FIELD_MISSION_ID = "missionId";
    private static final String FIELD_MISSION_ILVL = "missionIlvl";
    private static final String FIELD_MISSION_LEVEL = "missionLevel";
    private static final String FIELD_MISSION_NAME = "missionName";
    private static final String FIELD_MISSION_RARITY = "missionRarity";
    private static final String FIELD_RESOURCE_COST = "garrisonResourceCost";
    private static final String FIELD_REWARD_ITEM = "rewardItem";
    private static final String FIELD_REWARD_ITEM_AMOUNT = "rewardItemAmount";
    private static final String FIELD_REWARD_RESOURCES = "rewardGarrisonResources";
    private static final String FIELD_REWARD_XP = "rewardXp";
    private static final String FIELD_START_TIME = "startTime";
    private static final String FIELD_SUCCESS_CHANCE = "successChance";

    private static final String LOCATION_AUCHINDOUN = "Auchindoun";
    private static final String LOCATION_BLACKROCK_FOUNDRY = "Blackrock Foundry";
    private static final String LOCATION_BLOODMAUL_SLAG_MINES = "Bloodmaul Slag Mines";
    private static final String LOCATION_GRIMRAIL_DEPOT = "Grimrail Depot";
    private static final String LOCATION_HIGHMAUL = "Highmaul";
    private static final String LOCATION_SHADOWMOON_BURIAL_GROUNDS = "Shadowmoon Burial Grounds";
    private static final String LOCATION_SHATARI_SKYMESA = "Sha'tari Skymesa";
    private static final String LOCATION_SKYREACH = "Skyreach";
    private static final String LOCATION_THE_EVERBLOOM = "The Everbloom";
    private static final String LOCATION_UPPER_BLACKROCK_SPIRE = "Upper Blackrock Spire";

    private static final String MISSION_TYPE_COMBAT = "Combat";
    private static final String MISSION_TYPE_PATROL = "Patrol";
    private static final String MISSION_TYPE_TREASURE = "Treasure";

    private static final String ZONE_BLACKROCK_MOUNTAIN = "Blackrock Mountain";
    private static final String ZONE_FROSTFIRE_RIDGE = "Frostfire Ridge";
    private static final String ZONE_GORGROND = "Gorgrond";
    private static final String ZONE_NAGRAND = "Nagrand";
    private static final String ZONE_SHADOWMOON_VALLEY = "Shadowmoon Valley";
    private static final String ZONE_SPIRES_OF_ARAK = "Spires of Arak";
    private static final String ZONE_TALADOR = "Talador";

    private static final String TAG = "MissionsParser";

    public List<Mission> getMissions(String inputString) {

        List<Mission> missions = new ArrayList<>();

        try {
            JSONArray missionArray = new JSONArray(inputString);

            for (int i = 0; i < missionArray.length(); i++) {
                JSONObject missionObject = missionArray.getJSONObject(i);
                Mission mission = new Mission();

                mission.setAvailableDays(getInt(missionObject, FIELD_AVAILABLE_DAYS));
                mission.setAvailableHours(getInt(missionObject, FIELD_AVAILABLE_HOURS));
                mission.setEndTime(getInt(missionObject, FIELD_END_TIME));
                mission.setFaction(getFaction(missionObject.getString(FIELD_FACTION)));
                mission.setFollowerLimit(getInt(missionObject, FIELD_FOLLOWER_LIMIT));
                mission.setXpGain(getInt(missionObject, FIELD_GAINS_XP));
                mission.setMissionDescription(missionObject.getString(FIELD_MISSION_DESCRIPTION));
                mission.setMissionDuration(getInt(missionObject, FIELD_MISSION_DURATION));
                mission.setMissionEncounter1(getInt(missionObject, FIELD_MISSION_ENCOUNTER1));
                mission.setMissionEncounter2(getInt(missionObject, FIELD_MISSION_ENCOUNTER2));
                mission.setMissionEncounter3(getInt(missionObject, FIELD_MISSION_ENCOUNTER3));
                mission.setMissionEnvironment(missionObject.getString(FIELD_MISSION_ENVIRONMENT));
                mission.setMissionId(getInt(missionObject, FIELD_MISSION_ID));
                mission.setMissionIlvl(getInt(missionObject, FIELD_MISSION_ILVL));
                mission.setMissionLevel(getInt(missionObject, FIELD_MISSION_LEVEL));
                mission.setMissionLocation(missionObject.getString(FIELD_LOCATION));
                mission.setMissionName(missionObject.getString(FIELD_MISSION_NAME));
                mission.setMissionRarity(getQuality(missionObject.getString(FIELD_MISSION_RARITY)));
                mission.setMissionType(getMissionType(missionObject.getString(FIELD_MISSION_CATEGORY)));
                mission.setGarrisonResourceCost(getInt(missionObject, FIELD_RESOURCE_COST));
                mission.setStartTime(getInt(missionObject, FIELD_START_TIME));
                mission.setSuccessChance(getInt(missionObject, FIELD_SUCCESS_CHANCE));

                mission.setMissionZone(getMissionZone(missionObject.getString(FIELD_LOCATION)));

                mission.setMissionReward(getMissionReward(missionObject));

                missions.add(mission);
            }
        } catch (JSONException e) {
            CustomLog.v(TAG, "Error parsing JSON: " + e.getMessage());
        }

        return missions;
    }

    private MissionReward getMissionReward(JSONObject missionObject) throws JSONException {

        MissionReward missionReward = new MissionReward();

        int goldReward = getInt(missionObject, FIELD_GAINS_GOLD);
        int itemReward = getInt(missionObject, FIELD_REWARD_ITEM);
        int itemRewardAmount = getInt(missionObject, FIELD_REWARD_ITEM_AMOUNT);
        int resourcesReward = getInt(missionObject, FIELD_REWARD_RESOURCES);
        int xpReward = getInt(missionObject, FIELD_REWARD_XP);



        if (itemReward != 0) {
            missionReward.setMissionRewardType(MissionRewardType.ITEM);
        } else if (goldReward != 0) {
            missionReward.setMissionRewardType(MissionRewardType.GOLD);
        } else if (resourcesReward != 0) {
            missionReward.setMissionRewardType(MissionRewardType.GARRISON_RESOURCES);
        } else if (xpReward != 0) {
            missionReward.setMissionRewardType(MissionRewardType.EXPERIENCE);
        }

        missionReward.setGoldReward(goldReward);
        missionReward.setItemRewardAmount(itemRewardAmount);
        missionReward.setItemReward(itemReward);
        missionReward.setResourcesReward(resourcesReward);
        missionReward.setExperienceReward(xpReward);

        return missionReward;
    }

    private int getInt(JSONObject object, String field) throws JSONException {

        String stringValue = object.getString(field);

        return Integer.valueOf(stringValue);
    }

    private Faction getFaction(String factionString) {

        if (factionString.equalsIgnoreCase("Alliance")) {
            return Faction.ALLIANCE;
        } else if (factionString.equalsIgnoreCase("Horde")) {
            return Faction.HORDE;
        }

        return Faction.NEUTRAL;
    }

    private MissionZone getMissionZone(String location) {

        switch (location) {
            case LOCATION_UPPER_BLACKROCK_SPIRE:
            case ZONE_BLACKROCK_MOUNTAIN:
                return MissionZone.BLACKROCK_MOUNTAIN;
            case LOCATION_BLOODMAUL_SLAG_MINES:
            case ZONE_FROSTFIRE_RIDGE:
                return MissionZone.FROSTFIRE_RIDGE;
            case LOCATION_GRIMRAIL_DEPOT:
            case LOCATION_THE_EVERBLOOM:
            case LOCATION_BLACKROCK_FOUNDRY:
            case ZONE_GORGROND:
                return MissionZone.GORGROND;
            case LOCATION_HIGHMAUL:
            case ZONE_NAGRAND:
                return MissionZone.NAGRAND;
            case LOCATION_SHADOWMOON_BURIAL_GROUNDS:
            case ZONE_SHADOWMOON_VALLEY:
                return MissionZone.SHADOWMOON_VALLEY;
            case LOCATION_SKYREACH:
            case LOCATION_SHATARI_SKYMESA:
            case ZONE_SPIRES_OF_ARAK:
                return MissionZone.SPIRES_OF_ARAK;
            case LOCATION_AUCHINDOUN:
            case ZONE_TALADOR:
                return MissionZone.TALADOR;
            default:
                Log.w(TAG, "No location found for " + location);
                return MissionZone.NAGRAND;
        }
    }

    private MissionType getMissionType(String type) {

        switch (type) {
            case MISSION_TYPE_COMBAT:
                return MissionType.COMBAT;
            case MISSION_TYPE_PATROL:
                return MissionType.PATROL;
            case MISSION_TYPE_TREASURE:
                return MissionType.TREASURE;
            default:
                return MissionType.COMBAT;
        }
    }

    private Quality getQuality(String qualityString) {

        if (qualityString.equalsIgnoreCase("Uncommon")) {
            return Quality.UNCOMMON;
        } else if (qualityString.equalsIgnoreCase("Rare")) {
            return Quality.RARE;
        } else if (qualityString.equalsIgnoreCase("Epic")) {
            return Quality.EPIC;
        } else if (qualityString.equalsIgnoreCase("Legendary")) {
            return Quality.LEGENDARY;
        }

        return Quality.UNCOMMON;
    }
}
