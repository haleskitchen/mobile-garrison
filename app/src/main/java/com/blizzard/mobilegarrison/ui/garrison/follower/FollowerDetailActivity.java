package com.blizzard.mobilegarrison.ui.garrison.follower;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;

import com.blizzard.mobilegarrison.R;
import com.blizzard.mobilegarrison.datalayer.objects.garrison.follower.Follower;
import com.blizzard.mobilegarrison.ui.base.BaseActivity;

/**
 * Activity which holds a FollowerDetailFragment and manages the Fragment lifecycle
 *
 * @author Joshua Hale
 */
public class FollowerDetailActivity extends BaseActivity {

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);

        mToolbar.setBackgroundResource(R.drawable.actionbar_bg_garrison);

        setStatusBarColor(getResources().getColor(R.color.wow_brown_secondary));

        //-- Adds our Login Fragment
        final FragmentManager fm = getFragmentManager();
        final FragmentTransaction ft = fm.beginTransaction();
        Fragment f = new FollowerDetailFragment();

        Bundle args = new Bundle();

        if (getIntent().getExtras() != null) {
            Follower follower = (Follower) getIntent().getExtras().
                    getSerializable(getString(R.string.argument_follower));

            if (follower != null) {
                args.putSerializable(getString(R.string.argument_follower), follower);
                f.setArguments(args);
                ft.add(R.id.container, f);
                ft.commit();
            }
        }
    }
}
