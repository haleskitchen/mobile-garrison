package com.blizzard.mobilegarrison.utils.garrison;

import android.util.Log;

import com.blizzard.mobilegarrison.datalayer.objects.garrison.follower.Follower;
import com.blizzard.mobilegarrison.datalayer.objects.garrison.follower.FollowerAbility;
import com.blizzard.mobilegarrison.datalayer.objects.garrison.follower.FollowerCounter;
import com.blizzard.mobilegarrison.datalayer.objects.garrison.mission.Mission;
import com.blizzard.mobilegarrison.datalayer.objects.garrison.mission.MissionEnemy;

import java.util.ArrayList;
import java.util.List;

/**
 * This class allows us to generate the success chance for a specific mission. Since we
 * have to update the information each time the UI changes or the followers change for
 * a specific mission, it was easier to abstract this type of data to it's own class. We
 * generate a new SuccessChance object with the parameters needed and then from there
 * the success chance object can generate the data that we require for the mission.
 *
 * @author Joshua Hale (josh@joshuahale.com)
 */
public final class SuccessChance {

    private static final String TAG = "SuccessChance";

    private final Mission mMission;                               //-- Mission to get success chance for
    private List<FollowerCounter> mEnemyCounters;           //-- Counters the enemies have
    private final List<Follower> mFollowers;                      //-- Followers assigned to mission
    private final List<MissionEnemy> mEnemies;                    //-- Enemies for the mission

    private float mBaseSuccess;                             //-- Base success for the mission
    private float mPresence;                                //-- Standard presence value

    private int mMissionItemLevel;                           //-- Item level of the mission
    private int mMissionLevel;                               //-- Level of the mission

//    private String mMissionEnvironment;                      //-- Mission Environment, can be countered
//
//    boolean isEnvironmentCountered = false;                  //-- Boolean flag to track counter

    /**
     * Constructor for getting the Success Chance of a mission.
     * @param mission <b>(Mission)</b> - Mission to generate success chance for
     * @param followers <b>(List)</b> - List of Followers that are attached to the Mission
     * @param enemies <b>(List)</b> - List of MissionEnemy objects that are part of this mission.
     */
    public SuccessChance(Mission mission, List<Follower> followers, List<MissionEnemy> enemies) {
        mMission = mission;
        mFollowers = followers;
        mEnemies = enemies;

        init();
    }

    /**
     * Initializes all of the data we will need to successfully generate the success chance
     */
    private void init() {
        mBaseSuccess = (float) mMission.getSuccessChance();
//        mMissionEnvironment = mMission.getMissionEnvironment();
        mMissionItemLevel = mMission.getMissionIlvl();
        mMissionLevel = mMission.getMissionLevel();

        int followerLimit = mMission.getFollowerLimit();
        int numOfMechanics = getNumberOfMechanics();
        mPresence = getPresence(mBaseSuccess, numOfMechanics, followerLimit);

        mEnemyCounters = getEnemyCounters(mEnemies);
    }

    /**
     * Returns the success chance of a mission based on the followers
     * @return <b>(int)</b> - Integer value (from 0 - 100) of what the success chance will be.
     */
    public int getSuccessChance() {
        float contribution = 0;

        for (Follower follower : mFollowers) {
            contribution += getFollowerContribution(follower);
            Log.v(TAG, "Current Contribution: " + contribution);
        }

        int totalSuccess = (int) ((mBaseSuccess + contribution) * 100);
        Log.v(TAG, "Total Success: " + totalSuccess);

        if (totalSuccess > 100) {
            return 100;
        } else {
            return totalSuccess;
        }
    }

    private float getFollowerContribution(Follower follower) {

        int followerItemLevel = follower.getAverageItemLevel();
        int followerLevel = follower.getFollowerLevel();

        int counteredMechanics = getCounteredMechanics(follower);

        Log.v(TAG, "Number of Countered Mechanics: " + counteredMechanics);
        float counteredMechanicsPresence = mPresence * counteredMechanics;
        Log.v(TAG, "Presence for Countered Mechanics: " + counteredMechanicsPresence);

        int numOfNormalTraits;
        int numOfCounteredTraits;

//        if (!isEnvironmentCountered) {
//            numOfNormalTraits = getNormalTraits(follower.getFollowerTraits());
//            numOfCounteredTraits = getCounteredTraits(follower.getFollowerTraits());
//        } else {
            numOfNormalTraits = 1;
            numOfCounteredTraits = 1;
//        }

                //-- Determines whether or not the follower is over the level of the mission
        float overLeveledMultiplier = getOverLeveledMultiplier(followerLevel);
        Log.v(TAG, "OverLevel Multiplier: " + overLeveledMultiplier);

        //-- Determines whether or not the follower is over the item level of the mission
        float overItemLevelMultiplier = getOverItemLevelMultiplier( followerItemLevel);
        Log.v(TAG, "OverItemLevel Multiplier: " + overItemLevelMultiplier);

        //-- Determines whether or not the follower is under the level of the mission.
        float underLeveledMultiplier = getUnderLeveledMultiplier(followerLevel);
        Log.v(TAG, "UnderLeveled Multiplier: " + underLeveledMultiplier);

        //-- Determines whether or not the follower is under the item level for the mission.
        float underItemLevelMultiplier = getUnderItemLevelMultiplier( followerItemLevel);
        Log.v(TAG, "UnderItemLevel Multiplier: " + underItemLevelMultiplier);

        return underLeveledMultiplier * underItemLevelMultiplier * ((counteredMechanicsPresence)
                + (mPresence * (numOfNormalTraits + (numOfCounteredTraits)))
                + (mPresence * overLeveledMultiplier * overItemLevelMultiplier));
    }

    /**
     * Returns the total number of mechanics for the mission.
     * @return <b>(int)</b> - Total number of mechanics in the mission.
     */
    private int getNumberOfMechanics() {

        int numOfMechanics = 0;

        for (MissionEnemy enemy : mEnemies) {
            numOfMechanics = numOfMechanics + enemy.getCounters().size();
        }

        return numOfMechanics;
    }

    /**
     * Returns the float value of the presence of the Follower in the Mission
     * @param baseSuccess <b>(float)</b> - The base success of the follower
     * @param numOfMechanics <b>(float)</b> - Total number of mechanics to counter
     * @param numOfFollowers <b>(float)</b> - Total number of followers for the mission.
     * @return <b>(float)</b> - Standard presence (what a followers default value is)
     */
    private float getPresence(float baseSuccess, float numOfMechanics, float numOfFollowers) {
        return (1 - baseSuccess) / (numOfMechanics * 3 + numOfFollowers);
    }

    /**
     * Part of the calculation for the missions success chance. The over level multiplier
     * calculates whether or not the follower is over the level of the mission. <br />
     * Returns 1 if the follower is lower than or equal to the level of the mission. <br />
     * Returns greater than 1 if the follower is higher than the level of the mission.
     * @param followerLevel <b>(int)</b> - Level of the follower
     * @return <b>(int)</b> - Final over level multiplier
     */
    private float getOverLeveledMultiplier(int followerLevel) {

        float multiplier = (6 + Math.min((followerLevel - mMissionLevel) , 3)) / 6;

        if (multiplier < 1) {
            return 1;
        } else {
            return multiplier;
        }
    }

    /**
     * Part of the calculation for the missions success chance. The over item level multiplier
     * calculates whether or not the follower is over the item level of the mission. <br />
     * Returns 1 if the follower is lower than or equal to the level of the mission. <br />
     * Returns greater than 1 if the follower is higher than the level of the mission.
     * @param followerItemLevel <b>(int)</b> - Item level of the follower
     * @return <b>(float)</b> - The multiplier to add to a mission for a follower being over
     * the item level of a mission.
     */
    private float getOverItemLevelMultiplier(int followerItemLevel) {

        float multiplier = (30 + Math.min((followerItemLevel - mMissionItemLevel) , 15)) / 30;

        if (multiplier < 1) {
            return 1;
        } else {
            return multiplier;
        }
    }

    /**
     * Part of the calculation for the missions success chance. The under item level multiplier
     * calculates whether or not the follower is under the item level of the mission. <br />
     * Returns 1 if the follower is equal to or above the item level of the mission. <br />
     * Returns between 0 and 1 if the follower is less than the item level of the mission.
     * @param followerItemLevel <b>(int)</b> - Item level of the follower
     * @return <b>(float)</b> - Integer ranging between 0 and 1 depending on the item level.
     */
    private float getUnderItemLevelMultiplier(int followerItemLevel) {

        float multiplier = (15 - (mMissionItemLevel - followerItemLevel)) / 15;

        if (multiplier > 1) {
            return 1;
        } else {
            return multiplier;
        }
    }

    /**
     * Part of the calculation for the missions success chance. The underleveled multiplier
     * calculates if the follower is under the level of the mission. <br />
     * Returns 1 if the multiplier is greater than 1 (highest value). This symbolizes that
     * the follower is not underleveled. <br />
     * Returns between 0 and 1 if the follower is not the same level as the mission.
     *
     * @param followerLevel <b>(int)</b> - Level of the follower
     * @return <b>(float)</b> - Float value between 0 and 1 that becomes a multiplier against the
     * success chance of the mission.
     */
    private float getUnderLeveledMultiplier(int followerLevel) {

        float multiplier = (3 - (mMissionLevel - followerLevel)) / 3;

        if (multiplier > 1) {
            return 1;
        } else {
            return multiplier;
        }
    }

    /**
     * Returns the number of mechanics that this particular follower counters.
     * @param follower <b>(Follower)</b> - Follower that we need to determine how many counters
     *                 it does correctly.
     * @return <b>(int)</b> - Total number of counters the follower correctly counters.
     */
    private int getCounteredMechanics(Follower follower) {

        int numOfCounters = 0;

        //-- Retrieves the list of follower counters for the follower
        List<FollowerCounter> followerCounters = getFollowerCounters(follower);

        //-- Foreach loop that loops through all of the counters.
        //-- We determine if that counter is within our list of enemy counters.
        //-- If it is, increment the number of counters by 1, and remove the counter from
        //-- the enemy list to ensure duplication doesn't happen.
        for (FollowerCounter followerCounter : followerCounters) {
            if (mEnemyCounters.contains(followerCounter)) {
                numOfCounters++;
                mEnemyCounters.remove(followerCounter);
            }
        }

        return numOfCounters;
    }

    /**
     * Returns the entire list of counters for the mission based on the list of enemies
     * @param enemies <b>(List)</b> - List of MissionEnemy objects that are related to the mission
     * @return <b>(List)</b> - List of FollowerCounter objects that are all the counters for the mission.
     */
    private List<FollowerCounter> getEnemyCounters(List<MissionEnemy> enemies) {
        List<FollowerCounter> enemyCounters = new ArrayList<>();

        //-- Gets a list of all of the enemy counters for the mission
        for (MissionEnemy enemy: enemies) {
            for (FollowerCounter counter : enemy.getCounters()) {
                enemyCounters.add(counter);
            }
        }

        return enemyCounters;
    }

    /**
     * Returns the list of counters that the particular follower counters
     * @param follower <b>(Follower)</b> - Follower who's counters we need to determine.
     * @return <b>(List)</b> - List of FollowerCounter objects for the follower
     */
    private List<FollowerCounter> getFollowerCounters(Follower follower) {
        List<FollowerCounter> followerCounters = new ArrayList<>();

        //-- Iterates through the list of the followers abilities onces
        //-- and adds the counter of the ability to our list.
        for (FollowerAbility ability : follower.getFollowerAbilities()) {
            followerCounters.add(ability.getFollowerCounter());
        }

        return followerCounters;
    }
}
