package com.blizzard.mobilegarrison.datalayer.parser.character;

import com.blizzard.mobilegarrison.datalayer.objects.character.CharacterClass;
import com.blizzard.mobilegarrison.datalayer.objects.character.CharacterRace;
import com.blizzard.mobilegarrison.datalayer.objects.character.WoWCharacter;
import com.blizzard.mobilegarrison.datalayer.utils.logging.CustomLog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


/**
 * Uses standard JSON parsing in order to retrieve a list of characters. GSON
 * could also be used in lieu of manually parsing this. The goal of this was
 * to show that even if there is a library to parse this type of information,
 * it can also be done manually.
 *
 * @author Joshua Hale
 */
public class WoWCharacterParser {

    private static final String ARRAY_CHARACTERS = "characters";

    //-- Different fields found within our JSON objects
    private static final String FIELD_ACHIEVEMENT_POINTS = "achievementPoints";
    private static final String FIELD_BATTLEGROUP = "battlegroup";
    private static final String FIELD_CLASS = "class";
    private static final String FIELD_LEVEL = "level";
    private static final String FIELD_NAME = "name";
    private static final String FIELD_RACE = "race";
    private static final String FIELD_REALM = "realm";
    private static final String FIELD_THUMBNAIL = "thumbnail";

    private static final String TAG = "WoWCharacterParser";

    public WoWCharacterParser() {
        super();
    }

    /**
     * Parses a raw input string returned from the Battle.net API and gives a list
     * of the current WoW Characters the user has.
     * @param inputString <b>(String)</b> - JSON String that contains all of the character data
     * @return <b>(List)</b> - List of WoWCharacters for the current logged in user.
     */
    public List<WoWCharacter> getWoWCharacters(final String inputString) {

        final List<WoWCharacter> wowCharacters = new ArrayList<>();

        try {
            final JSONObject jsonObject = new JSONObject(inputString);
            final JSONArray characterArray = jsonObject.getJSONArray(ARRAY_CHARACTERS);

            if (characterArray != null) {
                for (int i = 0; i < characterArray.length(); i++) {
                    final JSONObject characterObject = characterArray.getJSONObject(i);

                    if (characterObject != null) {
                        final WoWCharacter wowCharacter = new WoWCharacter();
                        wowCharacter.setName(characterObject.getString(FIELD_NAME));
                        wowCharacter.setServer(characterObject.getString(FIELD_REALM));
                        wowCharacter.setBattlegroup(characterObject.getString(FIELD_BATTLEGROUP));
                        wowCharacter.setCharacterClass(CharacterClass.getEnum(characterObject.getInt(FIELD_CLASS)));
                        wowCharacter.setCharacterRace(CharacterRace.getEnum(characterObject.getInt(FIELD_RACE)));
                        wowCharacter.setLevel(characterObject.getInt(FIELD_LEVEL));
                        wowCharacter.setAchievementPoints(characterObject.getInt(FIELD_ACHIEVEMENT_POINTS));
                        wowCharacter.setThumbnailImageUrl(characterObject.getString(FIELD_THUMBNAIL));

                        wowCharacters.add(wowCharacter);
                    }
                }
            }
        } catch (final JSONException e) {
            CustomLog.w(TAG, "Error parsing JSON: " + e.getMessage());
        }

        return wowCharacters;
    }
}
