package com.blizzard.mobilegarrison.datalayer.tasks.results;

import com.blizzard.mobilegarrison.datalayer.objects.garrison.mission.Mission;

import java.util.List;

/**
 * Otto Event that is fired when {@link com.blizzard.mobilegarrison.datalayer.tasks.async.GetAvailableMissionsTask}
 * returns a list of Missions
 *
 * @author Joshua Hale
 */
public class GetActiveMissionsEvent {

    private final List<Mission> missions;

    public GetActiveMissionsEvent(List<Mission> missions) {
        this.missions = missions;
    }

    public List<Mission> getResult() {
        return this.missions;
    }
}
