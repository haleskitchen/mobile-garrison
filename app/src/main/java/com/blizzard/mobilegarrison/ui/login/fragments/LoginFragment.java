package com.blizzard.mobilegarrison.ui.login.fragments;

import android.app.Fragment;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.VideoView;

import com.blizzard.mobilegarrison.R;
import com.blizzard.mobilegarrison.constants.Font;
import com.blizzard.mobilegarrison.datalayer.manager.data.DataManager;
import com.blizzard.mobilegarrison.ui.login.activities.LoginWebActivity;
import com.blizzard.mobilegarrison.utils.font.FontUtils;

/**
 * This Fragment handles the user's authentication process by allowing the
 * user to enter their Battle.net user information and some other necessary
 * properties to direct them to the proper portal. If the user chooses to have
 * their login remembered, we store that username and the returned 0Auth 2.0
 * token securely.
 */
public class LoginFragment extends Fragment implements AdapterView.OnItemSelectedListener {


    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup parent,
                             final Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_login, parent, false);

        initializeViews(view);

        //-- Grab an instance of our FontUtils and Typefaces
        final FontUtils fontUtils = new FontUtils(getActivity());

        //-- Set the typeface across our layout to the FrizQT text
        final RelativeLayout parentLayout = (RelativeLayout) view.findViewById(R.id.parent);
        fontUtils.setTypeface(Font.FRIZQT, parentLayout);

        //-- Set the typefacet to our title to the Morpheus text
        final TextView title = (TextView) view.findViewById(R.id.app_title);
        fontUtils.setTypeface(Font.MORPHEUS, title);

        return view;
    }

    /**
     * Initializes the main view for the user within this Fragment
     * @param view <b>(View)</b> - Main content view that has been inflated
     */
    private void initializeViews(final View view) {

        //-- Grab the different views within our inflated view
        final ImageView backgroundImageView = (ImageView) view.findViewById(R.id.backgroundImageView);
        final Spinner regionsSpinner = (Spinner) view.findViewById(R.id.spinner_regions);
        final Spinner languagesSpinner = (Spinner) view.findViewById(R.id.spinner_languages);
        final VideoView backgroundVideo = (VideoView) view.findViewById(R.id.background_video);
        final Button loginButton = (Button) view.findViewById(R.id.button_login);

        //-- Sets the click listener to handle the user attempting to login
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                login();
            }
        });

        //-- Sets up the different spinners with their pre-designated array of String objects
        setupSpinner(regionsSpinner, R.array.regions);
        setupSpinner(languagesSpinner, R.array.languages);

        //-- Sets up the video view as our background
        setupVideoView(backgroundVideo, backgroundImageView);
    }

    /**
     * Sets up the designated spinner with the array (from the resource identifier)
     * @param spinner <b>(Spinner)</b> - Spinner with dropdown choices to be modified
     * @param arrayId <b>(int)</b> - Resource identifier for our array
     */
    private void setupSpinner(final Spinner spinner, final int arrayId) {
        //-- Creates an ArrayAdapter of the different regions from our strings.xml array
        final ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                arrayId, R.layout.login_spinner_item);

        //-- Adds the standard drop down view for the regions spinner
        adapter.setDropDownViewResource(R.layout.login_spinner_dropdown_item);

        //-- Sets the adapter to the spinner
        spinner.setAdapter(adapter);

        //-- Set the OnItemSelectedListener to the Fragment's listener
        spinner.setOnItemSelectedListener(this);
    }

    /**
     * Adds the background video to our VideoView object.
     * @param backgroundVideo <b>(VideoView)</b> - VideoView that has a video that will be loaded
     * @param backgroundImageView <b>(ImageView)</b> - ImageView that is shown if the video can't be loaded
     */
    private void setupVideoView(final VideoView backgroundVideo, final ImageView backgroundImageView) {

        //-- Returns the package of the application
        final String packageName = getActivity().getPackageName();

        //-- Grabs the video resource for our login video in the res/raw folder
        backgroundVideo.setVideoURI(Uri.parse("android.resource://" + packageName
                + "/" + R.raw.login_video_portrait));

        //-- When the background video has been prepared, we make sure that
        //-- the video loops and automatically starts
        backgroundVideo.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(final MediaPlayer mp) {
                mp.setLooping(true);
                backgroundVideo.start();
            }
        });

        backgroundVideo.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {

                backgroundVideo.setVisibility(View.GONE);
                backgroundImageView.setVisibility(View.VISIBLE);
                return true;
            }
        });
    }

    /**
     * Attempts to log the user into the rest of the application
     */
    private void login() {

        final Intent intent = new Intent(getActivity(), LoginWebActivity.class);
        startActivityForResult(intent, 0);
    }

    @Override
    public void onItemSelected(final AdapterView<?> parent, final View view, final int position,
                               final long id) {
        switch(view.getId()) {
            case R.id.spinner_regions:
                break;
            case R.id.spinner_languages:
                break;
        }
    }

    @Override
    public void onActivityResult(final int requestCode, final int resultCode,
                                    final Intent data) {

        final Bundle bundle = data.getExtras();
        final String authCode = bundle.getString("AuthCode");

        final DataManager dataManager = DataManager.getInstance(getActivity());
        dataManager.getAccessToken(authCode);
    }

    @Override
    public void onNothingSelected(final AdapterView<?> parent) {

    }

    @Override
    public void onPause() {
        super.onPause();

        //-- When our application goes into the background we pause the video view
        if (getView() != null) {
            VideoView videoView = (VideoView) getView().findViewById(R.id.background_video);

            videoView.pause();
        }
    }


}
