package com.blizzard.mobilegarrison.utils.error;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.widget.Toast;

import com.blizzard.mobilegarrison.R;

/**
 * Utility file that helps with the handling of errors.
 *
 * @author Joshua Hale
 */
public class ErrorUtils {

    private ErrorUtils() {

    }

    /**
     * Shows an error message either in a toast or an AlertDialog depending on the
     * @param context <b>(Context)</b> - Used to show the dialog or Toast
     * @param message <b>(String)</b> - Message to show in toast or dialog
     * @param showDialog <b>(boolean)</b> - Flag to show Toast or dialog
     */
    public static void showMessage(Context context, String message, boolean showDialog) {

        if (showDialog) {
            showErrorDialog(context, message);
        } else {
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Shows a dialog with the given message
     * @param context <b>(Context)</b> - Activity context
     * @param message <b>(String)</b> - Message for the dialog.
     */
    private static void showErrorDialog(Context context, String message) {

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(context.getString(R.string.error_title));
        builder.setMessage(message);
        builder.setPositiveButton(context.getString(R.string.ok),
                new DialogInterface.OnClickListener() {
            @Override
            public void onClick(final DialogInterface dialog, final int which) {
                dialog.dismiss();
            }
        });

        builder.create().show();

    }
}
