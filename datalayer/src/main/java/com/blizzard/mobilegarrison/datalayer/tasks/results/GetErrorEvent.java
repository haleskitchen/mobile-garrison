package com.blizzard.mobilegarrison.datalayer.tasks.results;


/**
 * Class that hands back an Error object to the UI
 * and determines whether or not the error should be thrown in a dialog
 * or should be shown in a ToastMessage, depending on the severity
 */
public class GetErrorEvent {

    private final boolean mShowDialog;
    private final String mMessage;

    public GetErrorEvent(String message, boolean showDialog) {
        mMessage = message;
        mShowDialog = showDialog;
    }

    public String getMessage() {
        return mMessage;
    }

    public boolean showDialog() {
        return mShowDialog;
    }
}
