package com.blizzard.mobilegarrison.utils.notification;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.blizzard.mobilegarrison.receiver.NotificationReceiver;

import java.util.Calendar;

/**
 * Notification Utility class that helps with the creation and addition of
 * notifications with any metadata that might be needed for a notification.
 *
 * @author Joshua Hale (josh@joshuahale.com)
 */
public class NotificationUtils {

    private NotificationUtils() {
        super();
    }

    /**
     * Schedules a notification at a particular time.
     * @param context <b>(Context)</b> - Activity Context, required in order to
     *                bind the AlarmManager service to our Broadcast.
     * @param timestamp <b>(long)</b> - The Unix Timestamp, in seconds, when the notification
     *                  should trigger.
     */
    public static void scheduleNotification(Context context, long timestamp) {

        //-- Retrieve the AlarmManager service for a System wide broadcast.
        AlarmManager alarmManager = (AlarmManager) context.getApplicationContext()
                .getSystemService(Context.ALARM_SERVICE);

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis((timestamp * 1000));

        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);

        Log.e("NotificationUtils", "Hour: " + hour + " Minute: " + minute);

        //-- Create an ID for the Alarm at the current millisecond time
        int id = (int) System.currentTimeMillis();

        //-- Intent that fires when the alarm finally triggers.
        Intent intent = new Intent(context, NotificationReceiver.class);

        //-- Creates a PendingIntent to fire off in the future.
        PendingIntent pendingIntent = PendingIntent.getBroadcast(
                context.getApplicationContext(), id, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
    }
}
