package com.blizzard.mobilegarrison.datalayer.objects.garrison.mission;

import com.blizzard.mobilegarrison.datalayer.objects.garrison.follower.FollowerCounter;

import java.io.Serializable;
import java.util.List;

/**
 * Object that contains all the data required for a MissionEnemy
 *
 * @author Joshua Hale
 */
public class MissionEnemy implements Serializable {

    private static final long serialVersionUID = 4211504212949634239L;
    private int id;
    private String name;
    private String iconUri;
    private List<FollowerCounter> counters;

    public int getId() {
        return id;
    }

    public void setId(final int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIconUri() {
        return iconUri;
    }

    public void setIconUri(String iconUri) {
        this.iconUri = iconUri;
    }

    public List<FollowerCounter> getCounters() {
        return counters;
    }

    public void setCounters(List<FollowerCounter> counters) {
        this.counters = counters;
    }
}
