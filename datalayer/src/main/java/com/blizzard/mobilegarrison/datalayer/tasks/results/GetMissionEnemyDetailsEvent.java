package com.blizzard.mobilegarrison.datalayer.tasks.results;

import com.blizzard.mobilegarrison.datalayer.objects.garrison.mission.MissionEnemy;

import java.util.List;

/**
 * Otto Event that is fired when the {@link com.blizzard.mobilegarrison.datalayer.tasks.async.GetMissionEnemyDetailsTask}
 * returns a list of MissionEnemies
 *
 * @author Joshua Hale
 */
public class GetMissionEnemyDetailsEvent {

    private final List<MissionEnemy> enemies;

    public GetMissionEnemyDetailsEvent(List<MissionEnemy> enemies) {
        this.enemies = enemies;
    }

    public List<MissionEnemy> getResult() {
        return enemies;
    }
}
