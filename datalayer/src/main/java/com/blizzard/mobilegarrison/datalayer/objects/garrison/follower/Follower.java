package com.blizzard.mobilegarrison.datalayer.objects.garrison.follower;

import com.blizzard.mobilegarrison.datalayer.objects.character.CharacterClass;
import com.blizzard.mobilegarrison.datalayer.objects.character.CharacterGender;
import com.blizzard.mobilegarrison.datalayer.objects.character.CharacterRace;
import com.blizzard.mobilegarrison.datalayer.objects.character.Quality;

import java.io.Serializable;
import java.util.List;

/**
 * Follower object that contains all the metadata for a follower. Returned
 * via the Follower API
 *
 * @author Joshua Hale
 */
public class Follower implements Serializable {

    private static final long serialVersionUID = -3518335716853356427L;
    private double id;
    private String followerName;
    private String imageUri;
    private CharacterClass followerClass;
    private CharacterRace followerRace;
    private CharacterGender followerGender;
    private int followerLevel;
    private int followerCurrentXp;
    private int followerTotalXp;
    private int followerArmorItemLevel;
    private int followerWeaponItemLevel;
    private boolean active;
    private Quality quality;
    private List<FollowerTrait> followerTraits;
    private List<FollowerAbility> followerAbilities;

    public double getId() {
        return id;
    }

    public void setId(final double id) {
        this.id = id;
    }

    public String getFollowerName() {
        return followerName;
    }

    public void setFollowerName(final String followerName) {
        this.followerName = followerName;
    }

    public String getFollowerImageUri() {
        return this.imageUri;
    }

    public void setFollowerImageUri(final String imageUri) {
        this.imageUri = imageUri;
    }

    public CharacterClass getFollowerClass() {
        return followerClass;
    }

    public void setFollowerClass(final CharacterClass followerClass) {
        this.followerClass = followerClass;
    }

    public CharacterRace getFollowerRace() {
        return followerRace;
    }

    public void setFollowerRace(final CharacterRace followerRace) {
        this.followerRace = followerRace;
    }

    public CharacterGender getFollowerGender() {
        return followerGender;
    }

    public void setFollowerGender(final CharacterGender followerGender) {
        this.followerGender = followerGender;
    }

    public int getFollowerLevel() {
        return followerLevel;
    }

    public void setFollowerLevel(final int followerLevel) {
        this.followerLevel = followerLevel;
    }

    public Quality getQuality() {
        return quality;
    }

    public void setQuality(final Quality quality) {
        this.quality = quality;
    }

    public List<FollowerTrait> getFollowerTraits() {
        return followerTraits;
    }

    public void setFollowerTraits(final List<FollowerTrait> followerTraits) {
        this.followerTraits = followerTraits;
    }

    public List<FollowerAbility> getFollowerAbilities() {
        return followerAbilities;
    }

    public void setFollowerAbilities(final List<FollowerAbility> followerAbilities) {
        this.followerAbilities = followerAbilities;
    }

    public int getFollowerArmorItemLevel() {
        return followerArmorItemLevel;
    }

    public void setFollowerArmorItemLevel(final int followerArmorItemLevel) {
        this.followerArmorItemLevel = followerArmorItemLevel;
    }

    public int getFollowerWeaponItemLevel() {
        return followerWeaponItemLevel;
    }

    public void setFollowerWeaponItemLevel(final int followerWeaponItemLevel) {
        this.followerWeaponItemLevel = followerWeaponItemLevel;
    }

    public int getAverageItemLevel() {
        return (this.followerArmorItemLevel + this.followerWeaponItemLevel) / 2;
    }

    public int getFollowerCurrentXp() {
        return followerCurrentXp;
    }

    public void setFollowerCurrentXp(int followerCurrentXp) {
        this.followerCurrentXp = followerCurrentXp;
    }

    public int getFollowerTotalXp() {
        return followerTotalXp;
    }

    public void setFollowerTotalXp(int followerTotalXp) {
        this.followerTotalXp = followerTotalXp;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
