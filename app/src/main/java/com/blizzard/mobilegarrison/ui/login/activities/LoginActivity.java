package com.blizzard.mobilegarrison.ui.login.activities;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.view.Window;

import com.blizzard.mobilegarrison.R;
import com.blizzard.mobilegarrison.ui.login.fragments.LoginFragmentTemp;

/**
 * Activity that is a wrapper for our LoginFragment.
 * @author Joshua.Hale (josh@joshuahale.com)
 */
public class LoginActivity extends Activity {

    private Fragment loginFragment;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //-- Turns off the title and sets the content view
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_login);

        //-- Adds our Login Fragment
        final FragmentManager fm = getFragmentManager();
        final FragmentTransaction ft = fm.beginTransaction();
        loginFragment = new LoginFragmentTemp();
        ft.add(R.id.container, loginFragment);
        ft.commit();
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode,
                                    final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        loginFragment.onActivityResult(requestCode, resultCode, data);
    }
}
