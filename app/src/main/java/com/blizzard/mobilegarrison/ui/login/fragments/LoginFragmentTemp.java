package com.blizzard.mobilegarrison.ui.login.fragments;

import android.app.Fragment;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.blizzard.mobilegarrison.R;
import com.blizzard.mobilegarrison.constants.Font;
import com.blizzard.mobilegarrison.datalayer.manager.data.DataManager;
import com.blizzard.mobilegarrison.datalayer.manager.user.UserManager;
import com.blizzard.mobilegarrison.ui.character.CharacterSelectActivity;
import com.blizzard.mobilegarrison.ui.login.activities.LoginWebActivity;
import com.blizzard.mobilegarrison.utils.font.FontUtils;
import com.blizzard.mobilegarrison.widgets.general.WoWDialog;

/**
 * Temporary LoginFRagment that shows an Authorize button rather than
 * the full Login Screen (due to OAuth 2.0 restrictions and time constraints)
 *
 * @author Joshua Hale
 */
public class LoginFragmentTemp extends Fragment {

    private View mView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent,
                              Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_login_temp, parent, false);
        mView = view;

        init();

        return view;
    }

    private void init() {
        //-- Grab an instance of our FontUtils and Typefaces
        FontUtils fontUtils = new FontUtils(getActivity());

        //-- Set the typeface across our layout to the FrizQT text
        RelativeLayout parentLayout = (RelativeLayout) mView.findViewById(R.id.parent);
        fontUtils.setTypeface(Font.FRIZQT, parentLayout);

        //-- Set the typefacet to our title to the Morpheus text
        TextView title = (TextView) mView.findViewById(R.id.app_title);
        fontUtils.setTypeface(Font.MORPHEUS, title);

        Button authorizeButton = (Button) mView.findViewById(R.id.button_authorize);
        authorizeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                authorize();
            }
        });

        setupVideoView();

        UserManager userManager = UserManager.getInstance(getActivity());

        if (userManager.getAccessToken() != null) {
            selectCharacters();
            getActivity().finish();
        }
    }

    /**
     * Adds the background video to our VideoView object.
     */
    private void setupVideoView() {

        final VideoView backgroundVideo = (VideoView) mView.findViewById(R.id.background_video);

        //-- Returns the package of the application
        String packageName = getActivity().getPackageName();

        //-- Grabs the video resource for our login video in the res/raw folder
        backgroundVideo.setVideoURI(Uri.parse("android.resource://" + packageName
                + "/" + R.raw.login_video_portrait));


        //-- When the background video has been prepared, we make sure that
        //-- the video loops and automatically starts
        backgroundVideo.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.setLooping(true);
                backgroundVideo.start();
            }
        });

        backgroundVideo.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {

                backgroundVideo.setVisibility(View.GONE);
                ImageView backgroundImageView = (ImageView) mView.findViewById(R.id.backgroundImageView);
                backgroundImageView.setVisibility(View.VISIBLE);
                return true;
            }
        });
    }

    /**
     * Attempts to authorize the user via the WebView.
     */
    private void authorize() {
        Intent intent = new Intent(getActivity(), LoginWebActivity.class);
        startActivityForResult(intent, 0);
    }

    /**
     * If the user has successfully logged in we can move to the CharacterSelection process
     */
    private void selectCharacters() {
        Intent intent = new Intent(getActivity(), CharacterSelectActivity.class);
        startActivity(intent);
        getActivity().finish();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode,
                                 Intent data) {

        if (data != null) {
            Bundle bundle = data.getExtras();
            String authCode = bundle.getString("AuthCode");

            DataManager dataManager = DataManager.getInstance(getActivity());

            WoWDialog wowDialog = new WoWDialog(getActivity(), "Authenticating");
            wowDialog.show();
            String accessToken = dataManager.getAccessToken(authCode);

            if (accessToken != null) {
                wowDialog.dismiss();
                selectCharacters();
            } else {
                Toast.makeText(getActivity(), "Unable to authenticate, please try again later", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
