package com.blizzard.mobilegarrison.datalayer.tasks.results;

import com.blizzard.mobilegarrison.datalayer.objects.item.WoWItem;

/**
 * This event class is registered once we return the data for a World of Warcraft
 * item from the official Blizzard API. This object is passed in through the constructor
 * and then handed to the UI where it can be used for whatever purpose.
 *
 * @author Joshua Hale (josh@joshuahale.com)
 */
public class GetItemEvent {

    private final WoWItem item;

    public GetItemEvent(WoWItem item) {
        this.item = item;
    }

    public WoWItem getResult() {
        return item;
    }
}
