package com.blizzard.mobilegarrison.ui.garrison;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.blizzard.mobilegarrison.R;
import com.blizzard.mobilegarrison.adapters.MissionsAdapter;
import com.blizzard.mobilegarrison.constants.MissionsFragmentType;
import com.blizzard.mobilegarrison.datalayer.manager.bus.EventBus;
import com.blizzard.mobilegarrison.datalayer.objects.character.WoWCharacter;
import com.blizzard.mobilegarrison.datalayer.objects.garrison.mission.Mission;
import com.blizzard.mobilegarrison.datalayer.tasks.async.DeleteMissionsTask;
import com.blizzard.mobilegarrison.datalayer.tasks.async.GetActiveMissionsTask;
import com.blizzard.mobilegarrison.datalayer.tasks.results.GetActiveMissionsEvent;
import com.blizzard.mobilegarrison.datalayer.tasks.results.GetErrorEvent;
import com.blizzard.mobilegarrison.datalayer.utils.time.TimeUtils;
import com.blizzard.mobilegarrison.ui.base.BaseFragment;
import com.blizzard.mobilegarrison.utils.error.ErrorUtils;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.List;

/**
 * Fragment that shows a ListView with all of the Missions that Followers are currently on
 *
 * @author Joshua Hale (josh@joshuahale.com)
 */
public class ActiveMissionsFragment extends BaseFragment {

    private List<Mission> mActiveMissions;
    private View mView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup parent,
                             final Bundle savedInstanceState) {

        mView = inflater.inflate(R.layout.fragment_current_missions, parent, false);

        init();

        return mView;
    }

    @Override
    public void onResume() {
        super.onResume();

        init();
    }

    @Override
    public void onStart() {
        EventBus.getInstance().register(this);
        super.onStart();
    }

    private void init() {
        new GetActiveMissionsTask(getActivity()).execute();
    }

    private void updateUI() {
        MissionsAdapter missionsAdapter = new MissionsAdapter(getActivity(), mActiveMissions, MissionsFragmentType.ACTIVE);
        ListView missionsListView = (ListView) mView.findViewById(R.id.missionsListView);
        missionsListView.setAdapter(missionsAdapter);
    }

    /**
     * Checks the entire list of active missions to see if we have any missions that have been completed.
     */
    private void checkCompletedMissions() {

        List<Mission> completedMissions = new ArrayList<>();

        for (Mission mission : mActiveMissions) {
            if (TimeUtils.isMissionCompleted(mission.getEndTime())) {
                completedMissions.add(mission);
            }
        }

        if (completedMissions.size() > 0) {
            showCompletedMissionsDialog(completedMissions);
        }
    }

    /**
     * If we at least one completed mission, we show the DialogFragment that shows the rewards
     * for completing the missions.
     * @param completedMissions <b>(List)</b> - List of completed missions.
     */
    private void showCompletedMissionsDialog(List<Mission> completedMissions) {
        Toast.makeText(getActivity(),
                completedMissions.size() + " missions completed", Toast.LENGTH_SHORT).show();
        //-- TODO: Show Dialog Fragment for Completed Missions.

        new DeleteMissionsTask(getActivity(), completedMissions).execute();
    }

    @Subscribe
    public void getMissions(GetActiveMissionsEvent event) {
        mActiveMissions = event.getResult();
        updateUI();
    }

    @Subscribe
    public void onError(GetErrorEvent event) {
        String message = event.getMessage();
        boolean showDialog = event.showDialog();

        ErrorUtils.showMessage(getActivity(), message, showDialog);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.active_missions, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {

        switch (menuItem.getItemId()) {
            case R.id.actionCompleteAllMissions:
                checkCompletedMissions();
                break;
            default:
                break;
        }

        return super.onOptionsItemSelected(menuItem);
    }

    @Override
    public void onStop() {
        EventBus.getInstance().unregister(this);
        super.onStop();
    }

    @Override
    public void onCharacterChanged(WoWCharacter character) {
        super.onCharacterChanged(character);

        new GetActiveMissionsTask(getActivity()).execute();
    }
}
