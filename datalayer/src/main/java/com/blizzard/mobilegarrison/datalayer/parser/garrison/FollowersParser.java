package com.blizzard.mobilegarrison.datalayer.parser.garrison;

import com.blizzard.mobilegarrison.datalayer.objects.character.CharacterClass;
import com.blizzard.mobilegarrison.datalayer.objects.character.CharacterGender;
import com.blizzard.mobilegarrison.datalayer.objects.character.CharacterRace;
import com.blizzard.mobilegarrison.datalayer.objects.character.Quality;
import com.blizzard.mobilegarrison.datalayer.objects.garrison.follower.Follower;
import com.blizzard.mobilegarrison.datalayer.objects.garrison.follower.FollowerAbility;
import com.blizzard.mobilegarrison.datalayer.objects.garrison.follower.FollowerTrait;
import com.blizzard.mobilegarrison.datalayer.utils.logging.CustomLog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Uses standard JSON parsing in order to retrieve a list of followers. GSON
 * could also be used in lieu of manually parsing this. The goal of this was
 * to show that even if there is a library to parse this type of information,
 * it can also be done manually.
 *
 * @author Joshua Hale
 */
public class FollowersParser {

    private static final String ARRAY_ABILITIES = "abilities";
    private static final String ARRAY_TRAITS = "traits";

    //-- Different fields found within our JSON objects
    private static final String FIELD_ACTIVE = "active";
    private static final String FIELD_ARMOR_ITEM_LEVEL = "armorItemLevel";
    private static final String FIELD_CLASS = "class";
    private static final String FIELD_CURRENT_XP = "currentXp";
    private static final String FIELD_GENDER = "gender";
    private static final String FIELD_ID = "id";
    private static final String FIELD_LEVEL = "level";
    private static final String FIELD_NAME = "name";
    private static final String FIELD_QUALITY = "quality";
    private static final String FIELD_RACE = "race";
    private static final String FIELD_URL = "url";
    private static final String FIELD_TOTAL_XP = "totalXp";
    private static final String FIELD_WEAPON_ITEM_LEVEL = "weaponItemLevel";

    private static final String TAG = "FollowersParser";

    public FollowersParser() {
        super();
    }

    public List<Follower> getFollowers(final String inputString) {

        try {
            final List<Follower> followers = new ArrayList<>();

            final JSONArray jsonArray = new JSONArray(inputString);

            for(int i = 0; i < jsonArray.length(); i++ ) {

                final JSONObject jsonObject = jsonArray.getJSONObject(i);
                final Follower follower = new Follower();

                follower.setId(Float.valueOf(jsonObject.getString(FIELD_ID)));
                follower.setFollowerName(jsonObject.getString(FIELD_NAME));
                follower.setFollowerRace(CharacterRace.getEnum(
                        Integer.valueOf(jsonObject.getString(FIELD_RACE))));
                follower.setFollowerClass(CharacterClass.getEnum(
                        Integer.valueOf(jsonObject.getString(FIELD_CLASS))));
                follower.setFollowerImageUri(jsonObject.getString(FIELD_URL));
                follower.setFollowerLevel(Integer.valueOf(jsonObject.getString(FIELD_LEVEL)));
                follower.setFollowerCurrentXp(Integer.valueOf(jsonObject.getString(FIELD_CURRENT_XP)));
                follower.setFollowerTotalXp(Integer.valueOf(jsonObject.getString(FIELD_TOTAL_XP)));
                follower.setQuality(Quality.getEnum(
                        Integer.valueOf(jsonObject.getString(FIELD_QUALITY))));
                follower.setFollowerWeaponItemLevel(
                        Integer.valueOf(jsonObject.getString(FIELD_WEAPON_ITEM_LEVEL)));
                follower.setFollowerArmorItemLevel(
                        Integer.valueOf(jsonObject.getString(FIELD_ARMOR_ITEM_LEVEL)));

                //-- Get the follower traits String
                final String followerTraits = jsonObject.getString(ARRAY_TRAITS);
                final String followerAbilities = jsonObject.getString(ARRAY_ABILITIES);

                setFollowerTraits(followerTraits, follower);
                setFollowerAbilities(followerAbilities, follower);
                int active = Integer.valueOf(jsonObject.getString(FIELD_ACTIVE));

                follower.setActive(active == 1);



                int race = Integer.valueOf(jsonObject.getString(FIELD_GENDER));

                if (race == 0) {
                    follower.setFollowerGender(CharacterGender.MALE);
                } else {
                    follower.setFollowerGender(CharacterGender.FEMALE);
                }

                followers.add(follower);
            }

            return followers;
        } catch (final JSONException e) {
            CustomLog.w(TAG, "Error parsing JSON: " + e.getMessage());
        }

        return null;
    }

    private void setFollowerTraits(final String followerTraits, final Follower follower) {
        final List<FollowerTrait> traits = new ArrayList<>();

        if (!followerTraits.equalsIgnoreCase("")) {
            final String[] followerArray = followerTraits.split(",");

            //-- For each String within the Array iterate through
            //-- and get the corresponding trait.
            for (String s : followerArray) {
                final int traitValue = Integer.valueOf(s);
                final FollowerTrait trait = FollowerTrait.getEnum(traitValue);
                traits.add(trait);
            }
        }

        follower.setFollowerTraits(traits);
    }

    private void setFollowerAbilities(final String followerAbilities, final Follower follower) {

        final List<FollowerAbility> abilities = new ArrayList<>();

        if (!followerAbilities.equalsIgnoreCase("")) {
            final String[] followerArray = followerAbilities.split(",");

            for (final String s : followerArray) {
                final int abilityValue = Integer.valueOf(s);
                final FollowerAbility ability = FollowerAbility.getEnum(abilityValue);
                abilities.add(ability);
            }
        }

        follower.setFollowerAbilities(abilities);
    }
}
