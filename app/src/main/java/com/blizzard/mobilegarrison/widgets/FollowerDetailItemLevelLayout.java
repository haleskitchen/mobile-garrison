package com.blizzard.mobilegarrison.widgets;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blizzard.mobilegarrison.R;
import com.blizzard.mobilegarrison.datalayer.objects.garrison.follower.Follower;

/**
 * DetailItemLayout encapsulates the layout for a specific follower. This layout
 * is used to showcase the different drawables that can be used for each rarity of a follower
 * as well as the rest of the information regarding a follower's item level.
 *
 * @author Joshua Hale
 */
public class FollowerDetailItemLevelLayout extends LinearLayout {

    private int armorItemLevel;
    private int averageItemLevel;
    private int weaponItemLevel;

    public FollowerDetailItemLevelLayout(Context context) {
        super(context);
    }

    public FollowerDetailItemLevelLayout(Context context, Follower follower) {
        super(context);

        armorItemLevel = follower.getFollowerArmorItemLevel();
        averageItemLevel = follower.getAverageItemLevel();
        weaponItemLevel = follower.getFollowerWeaponItemLevel();

        init();
    }

    private void init() {
        inflate(getContext(), R.layout.follower_detail_item_level, this);

        setupArmorItemLevelUI();
        setupAverageItemLevelUI();
        setupWeaponItemLevelUI();
    }

    private void setupAverageItemLevelUI() {

        View rootView = getRootView();

        TextView averageItemLevelTextView = (TextView) rootView
                .findViewById(R.id.averageItemLevelTextView);

        averageItemLevelTextView.setText("iLvl " + averageItemLevel);
    }

    private void setupArmorItemLevelUI() {

        View rootView = getRootView();

        ImageView armorItemLevelImageView = (ImageView) rootView.
                findViewById(R.id.armorItemLevelImageView);
        TextView armorTextView = (TextView) rootView.findViewById(R.id.armorTextView);
        TextView armorIlvlTextView = (TextView) rootView.findViewById(R.id.armorIlvlTextView);

        if (armorItemLevel >= 600 && armorItemLevel < 630) {
            armorItemLevelImageView.setImageResource(R.drawable.ic_war_ravaged_armor_set);
            armorTextView.setTextColor(getContext().getResources().getColor(R.color.item_uncommon));
        } else if (armorItemLevel >= 630 && armorItemLevel < 645) {
            armorItemLevelImageView.setImageResource(R.drawable.ic_blackrock_armor_set);
            armorTextView.setTextColor(getContext().getResources().getColor(R.color.item_rare));
        } else {
            armorItemLevelImageView.setImageResource(R.drawable.ic_goredrenched_armor_set);
            armorTextView.setTextColor(getContext().getResources().getColor(R.color.item_epic));
        }

        armorIlvlTextView.setText("iLvl " + armorItemLevel);

    }

    private void setupWeaponItemLevelUI() {

        View rootView = getRootView();

        ImageView weaponItemLevelImageView = (ImageView) rootView.
                findViewById(R.id.weaponItemLevelImageView);
        TextView weaponTextView = (TextView) rootView.findViewById(R.id.weaponTextView);
        TextView weaponIlvlTextView = (TextView) rootView.findViewById(R.id.weaponIlvlTextView);

        if (weaponItemLevel >= 600 && weaponItemLevel < 630) {
            weaponItemLevelImageView.setImageResource(R.drawable.ic_war_ravaged_weaponry);
            weaponTextView.setTextColor(getContext().getResources().getColor(R.color.item_uncommon));
        } else if (weaponItemLevel >= 630 && weaponItemLevel < 645) {
            weaponItemLevelImageView.setImageResource(R.drawable.ic_blackrock_weaponry);
            weaponTextView.setTextColor(getContext().getResources().getColor(R.color.item_rare));
        } else {
            weaponItemLevelImageView.setImageResource(R.drawable.ic_goredrenched_weaponry);
            weaponTextView.setTextColor(getContext().getResources().getColor(R.color.item_epic));
        }

        weaponIlvlTextView.setText("iLvl " + armorItemLevel);

    }
}
