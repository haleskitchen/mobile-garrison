package com.blizzard.mobilegarrison.datalayer.tasks.async;

import android.content.Context;
import android.os.AsyncTask;

import com.blizzard.mobilegarrison.datalayer.constants.ErrorStrings;
import com.blizzard.mobilegarrison.datalayer.manager.bus.EventBus;
import com.blizzard.mobilegarrison.datalayer.manager.data.DataManager;
import com.blizzard.mobilegarrison.datalayer.objects.garrison.follower.FollowerTrait;
import com.blizzard.mobilegarrison.datalayer.objects.garrison.follower.TraitDetails;
import com.blizzard.mobilegarrison.datalayer.tasks.results.GetErrorEvent;
import com.blizzard.mobilegarrison.datalayer.tasks.results.GetTraitDetailsEvent;

import java.util.List;

/**
 * Get a list of TraitDetails objects from a list of FollowerTraits. This will return
 * icons, texts, etc for specific FollowerTraits.
 *
 * @author Joshua Hale (josh@joshuahale.com)
 */
public class GetTraitsTask extends
        AsyncTask<Void, Void, List<TraitDetails>> {

    private final Context mContext;
    private final List<FollowerTrait> mFollowerTraits;

    /**
     * Constructor that requires an Activity Context and a list of follower traits
     * @param context <b>(Context)</b> - Activity context
     * @param followerTraits <b>(FollowerTraits)</b> - List of Follower traits that are
     *                       determined by the Follower object.
     */
    public GetTraitsTask(Context context, List<FollowerTrait> followerTraits) {
        mContext = context;
        mFollowerTraits = followerTraits;
    }

    @Override
    public List<TraitDetails> doInBackground(Void... params) {

        DataManager dataManager = DataManager.getInstance(mContext);
        return dataManager.getFollowerTraits(mFollowerTraits);
    }

    @Override
    public void onPostExecute(List<TraitDetails> traitDetails) {
        //-- Check to make sure we are not handing back a null object
        //-- or an empty object
        if (traitDetails != null && traitDetails.size() > 0) {
            EventBus.getInstance().post(new GetTraitDetailsEvent(traitDetails));
        } else {
            EventBus.getInstance().post(new GetErrorEvent(
                    ErrorStrings.ERROR_NO_TRAIT_DETAILS, false));
        }
    }
}