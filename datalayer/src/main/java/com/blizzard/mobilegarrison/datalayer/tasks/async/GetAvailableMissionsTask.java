package com.blizzard.mobilegarrison.datalayer.tasks.async;

import android.content.Context;
import android.os.AsyncTask;

import com.blizzard.mobilegarrison.datalayer.constants.ErrorStrings;
import com.blizzard.mobilegarrison.datalayer.manager.bus.EventBus;
import com.blizzard.mobilegarrison.datalayer.manager.data.DataManager;
import com.blizzard.mobilegarrison.datalayer.manager.user.UserManager;
import com.blizzard.mobilegarrison.datalayer.objects.garrison.mission.Mission;
import com.blizzard.mobilegarrison.datalayer.tasks.results.GetAvailableMissionsEvent;
import com.blizzard.mobilegarrison.datalayer.tasks.results.GetErrorEvent;

import java.util.List;

/**
 * This AsyncTask returns the list of currently available missions to the UI after
 * determining which character is logged in. We determine which character is currently
 * selected, based on the "databaseString" we retrieve as a private method in this task.
 * It correlates to a table on the backend which is unique for the account and character.
 * We return the list of mission objects that are handed to us by the server, and send
 * that up to the UI layer.
 *
 * @author Joshua Hale (josh@joshuahale.com)
 */
public class GetAvailableMissionsTask extends AsyncTask<Void, Void, List<Mission>> {

    private final Context mContext;

    /**
     * Constructor that needs a context in order to retrieve information from our DataManager
     * @param context <b>(Context)</b> - Activity context
     */
    public GetAvailableMissionsTask(Context context) {
        mContext = context;
    }

    @Override
    public List<Mission> doInBackground(Void... params) {

        String dbString = UserManager.getInstance(mContext).getDatabaseString();

        DataManager dataManager = DataManager.getInstance(mContext);
        return dataManager.getAvailableMissions(dbString);
    }

    @Override
    public void onPostExecute(List<Mission> missions) {
        if (missions != null) {
            EventBus.getInstance().post(new GetAvailableMissionsEvent(missions));
        } else {
            EventBus.getInstance().post(new GetErrorEvent(
                    ErrorStrings.ERROR_NO_MISSIONS, true));
        }
    }
}
