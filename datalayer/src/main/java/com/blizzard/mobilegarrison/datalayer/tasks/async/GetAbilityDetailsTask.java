package com.blizzard.mobilegarrison.datalayer.tasks.async;

import android.content.Context;
import android.os.AsyncTask;

import com.blizzard.mobilegarrison.datalayer.constants.ErrorStrings;
import com.blizzard.mobilegarrison.datalayer.manager.bus.EventBus;
import com.blizzard.mobilegarrison.datalayer.manager.data.DataManager;
import com.blizzard.mobilegarrison.datalayer.objects.garrison.follower.AbilityDetails;
import com.blizzard.mobilegarrison.datalayer.objects.garrison.follower.FollowerAbility;
import com.blizzard.mobilegarrison.datalayer.tasks.results.GetAbilityDetailsEvent;
import com.blizzard.mobilegarrison.datalayer.tasks.results.GetErrorEvent;

import java.util.List;

/**
 * This AsyncTask takes a list of Follower Abilities and returns a list of Ability Details.
 * The reason for this is that instead of having two objects (one being an enum with static
 * values), we transform that into an AbilityDetail object that houses all of the relevant
 * data pertaining to that ability. This can then be used on our UI layer in places we might need
 * it. Currently this is only being used by the FollowerDetailFragment
 * in order to show the details of a Followers abilities.
 * @author Joshua Hale (josh@joshuahale.com)
 * @version 1.0
 */
public class GetAbilityDetailsTask extends
        AsyncTask<Void, Void, List<AbilityDetails>> {

    private final Context mContext;
    private final List<FollowerAbility> mAbilities;

    /**
     * Public constructor
     * @param context <b>(Context)</b> - Activity context
     * @param abilities <b>(List)</b> - List of Follower Abilities to get details for
     */
    public GetAbilityDetailsTask(Context context, List<FollowerAbility> abilities) {
        mAbilities = abilities;
        mContext = context;
    }

    @Override
    public List<AbilityDetails> doInBackground(Void... params) {

        DataManager dataManager = DataManager.getInstance(mContext);
        return dataManager.getFollowerAbilityDetails(mAbilities);
    }

    @Override
    public void onPostExecute(List<AbilityDetails> abilityDetails) {
        if (abilityDetails != null && abilityDetails.size() > 0) {
            EventBus.getInstance().post(new GetAbilityDetailsEvent(abilityDetails));
        } else {
            EventBus.getInstance().post(new GetErrorEvent(
                    ErrorStrings.ERROR_NO_ABILITY_DETAILS, false));
        }
    }
}