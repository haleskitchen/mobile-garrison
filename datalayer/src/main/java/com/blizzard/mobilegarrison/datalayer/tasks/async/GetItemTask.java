package com.blizzard.mobilegarrison.datalayer.tasks.async;

import android.content.Context;
import android.os.AsyncTask;

import com.blizzard.mobilegarrison.datalayer.manager.bus.EventBus;
import com.blizzard.mobilegarrison.datalayer.manager.data.DataManager;
import com.blizzard.mobilegarrison.datalayer.objects.item.WoWItem;
import com.blizzard.mobilegarrison.datalayer.tasks.results.GetItemEvent;

/**
 * AsyncTask that fires off to the Blizzard API to get data for a particular item
 * in World of Warcraft. The official Blizzard REST API only requires an item id
 * so the id for the item in question is passed into the constructor. The DataManager
 * then handles the creation of the HTTP call and the parsing of the response in JSON
 * format. The object is then handled based on the EventBus implementation to return
 * to the UI once the request/response cycle is completed.
 *
 * @author Joshua Hale (josh@joshuahale.com)
 */
public class GetItemTask extends AsyncTask<Void, Void, WoWItem> {

    private final Context mContext;
    private final int mItemId;

    public GetItemTask(Context context, int itemId) {
        mContext = context;
        mItemId = itemId;
    }

    @Override
    public WoWItem doInBackground(Void... params) {

        DataManager dataManager = DataManager.getInstance(mContext);
        return dataManager.getWowItem(mItemId);
    }

    @Override
    public void onPostExecute(WoWItem item) {

        if (item != null) {
            EventBus.getInstance().post(new GetItemEvent(item));
        }
    }
}
