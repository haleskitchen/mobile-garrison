package com.blizzard.mobilegarrison.datalayer.objects.garrison.mission;

import java.io.Serializable;

/**
 * This object contains all of the data required for the reward of a mission. The different
 * reward types are gold, experience, garrison resource, or items. All of that is encapsulated
 * into one object so that the UI can dictate which one to pull from. The MissionRewardType
 * enum is used to determine which type of mission reward it is, and allows the UI
 * to dictate which methods to call.
 *
 * @author Joshua Hale (josh@joshuahale.com)
 */
public class MissionReward implements Serializable {


    private static final long serialVersionUID = -806153270388123000L;
    private MissionRewardType missionRewardType;
    private int goldReward;
    private int itemReward;
    private int itemRewardAmount;
    private int resourcesReward;
    private int experienceReward;

    public MissionRewardType getMissionRewardType() {
        return missionRewardType;
    }

    public void setMissionRewardType(final MissionRewardType missionRewardType) {
        this.missionRewardType = missionRewardType;
    }

    public int getGoldReward() {
        return goldReward;
    }

    public void setGoldReward(final int goldReward) {
        this.goldReward = goldReward;
    }

    public int getItemReward() {
        return itemReward;
    }

    public void setItemReward(final int itemReward) {
        this.itemReward = itemReward;
    }

    public int getItemRewardAmount() {
        return itemRewardAmount;
    }

    public void setItemRewardAmount(final int itemRewardAmount) {
        this.itemRewardAmount = itemRewardAmount;
    }

    public int getResourcesReward() {
        return resourcesReward;
    }

    public void setResourcesReward(final int resourcesReward) {
        this.resourcesReward = resourcesReward;
    }

    public int getExperienceReward() {
        return experienceReward;
    }

    public void setExperienceReward(final int experienceReward) {
        this.experienceReward = experienceReward;
    }
}
