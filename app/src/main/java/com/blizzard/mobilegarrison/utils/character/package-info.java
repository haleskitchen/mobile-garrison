/**
 * Contains all of the methods required to manipulate character data
 * or retrieve detailed character information
 *
 * @version 1.0
 * @since 1.0
 */
package com.blizzard.mobilegarrison.utils.character;