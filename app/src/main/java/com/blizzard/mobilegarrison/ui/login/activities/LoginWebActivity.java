package com.blizzard.mobilegarrison.ui.login.activities;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.webkit.CookieManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.blizzard.mobilegarrison.R;
import com.blizzard.mobilegarrison.datalayer.manager.user.UserManager;

import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

/**
 * WebView that gets shown when the user attempts to log into the Mobile Garrison App.
 *
 * @author Joshua Hale
 */
public class LoginWebActivity extends Activity {
    
    private static final String URL_CONTAINS = "https://dev.battle.net/";

    @SuppressLint("SetJavaScriptEnabled")
    @SuppressWarnings("deprecated")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_web);

        WebView webView = (WebView) findViewById(R.id.web_view);

        WebSettings webSettings = webView.getSettings();

        UserManager userManager = UserManager.getInstance(this);

        if (!userManager.isUserLoggedIn()) {
            webView.clearCache(true);

            CookieManager cookieManager = CookieManager.getInstance();

            int currentApiVersion = Build.VERSION.SDK_INT;
            if (currentApiVersion >= Build.VERSION_CODES.LOLLIPOP) {
               removeCookies();
            } else {
                cookieManager.removeAllCookie();
            }
        }
        webSettings.setJavaScriptEnabled(true);

        webView.setWebViewClient(new WebViewClient() {

            @Override
            public boolean shouldOverrideUrlLoading(WebView webView, String url) {
                if (url.contains(URL_CONTAINS)) {

                    try {
                        List<NameValuePair> params = URLEncodedUtils.parse(new URI(url), "UTF-8");

                        String authCode = "";
                        for (NameValuePair param : params) {
                            if (param.getName().equalsIgnoreCase("code")) {
                                authCode = param.getValue();
                                break;
                            }
                        }

                        Intent intent = new Intent();
                        Bundle bundle = new Bundle();
                        bundle.putString("AuthCode", authCode);
                        intent.putExtras(bundle);
                        setResult(RESULT_OK, intent);
                        finish();
                    } catch (URISyntaxException e) {
                        e.printStackTrace();
                    }
                }

                return super.shouldOverrideUrlLoading(webView, url);
            }
        });

        String testUrl = "https://us.battle.net/login/en/index?ref=https://us.battle.net/oauth/" +
                "authorize?scope%3Dwow.profile%2Bsc2.profile%26response_type%3Dcode%26redirect_uri" +
                "%3Dhttps%253A%252F%252Fdev.battle.net%252Fio-docs%252Foauth2callback" +
                "%26client_id%3Ds4ext55be7hqepxzgcradmcvwe2nm77z&app=oauth";

        webView.loadUrl(testUrl);
    }

    @TargetApi(21)
    private void removeCookies() {
        CookieManager cookieManager = CookieManager.getInstance();
        cookieManager.removeAllCookies(null);
    }
}
