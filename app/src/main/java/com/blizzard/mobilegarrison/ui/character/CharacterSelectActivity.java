package com.blizzard.mobilegarrison.ui.character;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.blizzard.mobilegarrison.R;
import com.blizzard.mobilegarrison.adapters.NavigationAdapter;
import com.blizzard.mobilegarrison.datalayer.manager.bus.EventBus;
import com.blizzard.mobilegarrison.datalayer.manager.data.DataManager;
import com.blizzard.mobilegarrison.datalayer.manager.user.UserManager;
import com.blizzard.mobilegarrison.datalayer.objects.character.WoWCharacter;
import com.blizzard.mobilegarrison.datalayer.tasks.async.GetCharacterListTask;
import com.blizzard.mobilegarrison.datalayer.tasks.results.GetCharactersEvent;
import com.blizzard.mobilegarrison.datalayer.tasks.results.GetErrorEvent;
import com.blizzard.mobilegarrison.ui.dashboard.DashboardActivity;
import com.blizzard.mobilegarrison.ui.login.activities.LoginActivity;
import com.blizzard.mobilegarrison.utils.error.ErrorUtils;
import com.blizzard.mobilegarrison.widgets.general.WoWDialog;
import com.squareup.otto.Subscribe;

import java.util.List;

/**
 * Once the user has authenticated to the activity we show this CharacterSelectActivity.
 * When the user has selected a character we can safely use this as the default character
 * and allow the user to modify which character is currently selected via the Dashboard.
 *
 * @author Joshua Hale (josh@joshuahale.com)
 * @version 1.0
 * @since 1.0
 */
public class CharacterSelectActivity extends Activity {

    private WoWDialog mWowDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_character_select);

        UserManager userManager = UserManager.getInstance(this);

        if (userManager.getCurrentWoWCharacter() == null) {
            //-- Shows our dialog that tells the user we are
            //-- getting information from the server
            showDialog();

            EventBus.getInstance().register(this);
            //-- Starts our background task to retrieve our character list
            new GetCharacterListTask(this).execute();
            new GetAccountIdTask().execute();
        } else {
            startActivity(new Intent(this, DashboardActivity.class));
            finish();
        }
    }

    /**
     * Updates the UI with the list of objects passed into the parameters
     *
     * @param characters <b>(List)</b> - List of WoWCharacter objects returned from the backend.
     */
    private void updateUI(final List<WoWCharacter> characters) {

        NavigationAdapter adapter = new NavigationAdapter(this, characters, true);

        ListView characterListView = (ListView) findViewById(R.id.characterListView);

        characterListView.setAdapter(adapter);

        characterListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                UserManager userManager = UserManager.getInstance(CharacterSelectActivity.this);
                WoWCharacter wowCharacter = characters.get(position);
                userManager.setCurrentWoWCharacter(wowCharacter);

                Intent intent = new Intent(CharacterSelectActivity.this,
                        DashboardActivity.class);
                startActivity(intent);
                finish();
            }
        });

        hideDialog();
    }

    /**
     * Shows the retrieving character list dialog
     */
    private void showDialog() {

        mWowDialog = new WoWDialog(this, "Retrieving character list");
        mWowDialog.show();
    }

    /**
     * Hides the retrieving character list dialog
     */
    private void hideDialog() {

        if (mWowDialog != null && mWowDialog.isShowing()) {
            mWowDialog.dismiss();
        }
    }

    /**
     * Log the user out of the application
     */
    private void logout() {

        UserManager userManager = UserManager.getInstance(this);
        userManager.clearUserData();

        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    /**
     * Shows the Logout Dialog if the user chooses to log out of the application.
     */
    private void logoutDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.dialog_title_logout);
        builder.setMessage(R.string.dialog_message_logout);
        builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                logout();
                dialog.dismiss();
            }
        });

        builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builder.create().show();
    }

    @Subscribe
    public void getCharacters(GetCharactersEvent event) {
        updateUI(event.getResult());
    }

    @Subscribe
    public void onError(GetErrorEvent event) {
        String message = event.getMessage();
        boolean showDialog = event.showDialog();

        ErrorUtils.showMessage(this, message, showDialog);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.logout, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.logout:
                logoutDialog();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onStop() {
        EventBus.getInstance().unregister(this);
        super.onStop();
    }

    /**
     * Retrieves the currently logged in user's account Id
     */
    private class GetAccountIdTask extends AsyncTask<Void, Void, Void> {

        @Override
        public Void doInBackground(Void... params) {

            DataManager dataManager = DataManager.getInstance(CharacterSelectActivity.this);
            dataManager.getAccountId();

            return null;
        }
    }
}
