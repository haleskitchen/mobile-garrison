package com.blizzard.mobilegarrison.utils.garrison;

import com.blizzard.mobilegarrison.R;
import com.blizzard.mobilegarrison.datalayer.constants.MobileGarrisonConstants;
import com.blizzard.mobilegarrison.datalayer.objects.character.Quality;
import com.blizzard.mobilegarrison.datalayer.objects.garrison.mission.MissionType;
import com.blizzard.mobilegarrison.datalayer.objects.garrison.mission.MissionZone;

/**
 * Utility class that handles any transactions needed between the data and the UI
 * portions of the Garrison. Also, this application handles the image data that would be used
 * for different portions of the missions UI as well as any data needed for the
 * Followers.
 *
 * @author Joshua Hale (josh@joshuahale.com)
 * @version 1.0
 */
public class GarrisonUtils {

    private static final String TAG = "GarrisonUtils";

    private GarrisonUtils() {
        super();
    }

    private static final int REWARD_ABROGATOR_STONE = 115280;
    private static final int REWARD_APEXIS_CRYSTAL = 823;
    private static final int REWARD_ARMOR_ENHANCEMENT_TOKEN = 120301;
    private static final int REWARD_BLACKROCK_ARMOR_SET = 114806;
    private static final int REWARD_BLACKROCK_WEAPONRY = 114081;
    private static final int REWARD_CACHE_OF_HIGHMAUL_TREASURES_MYTHIC = 118531;
    private static final int REWARD_CACHE_OF_HIGHMAUL_TREASURES_HEROIC = 118530;
    private static final int REWARD_CACHE_OF_HIGHMAUL_TREASURES_NORMAL = 118529;
    private static final int REWARD_CONQUEST_POINTS = 390;
    private static final int REWARD_ELEMENTAL_RUNE = 115510;
    private static final int REWARD_FOLLOWER_RETRAINING_CERTIFICATE = 118354;
    private static final int REWARD_GRANDIOSE_SPAULDERS = 114085;
    private static final int REWARD_HONOR_POINTS = 392;
    private static final int REWARD_SEAL_OF_TEMPERED_FATE = 994;
    private static final int REWARD_WEAPON_ENHANCEMENT_TOKEN = 120302;

    //-- Environmental presences such as Mountaineer, Angler, Evergreen which act as
    //-- counters to other environmental presences.
    private static final float TRAIT_PRESENCE_VALUE = 1;

    //-- Traits that counter a specific race have an increased modifier. Examples would
    //-- be ally of argus, Death Fascination, Gnome-Lover and Voodoo Zealot.
    private static final double TRAIT_RACE_BASED_VALUE = 1.5;

    /**
     * Returns the entire URL for a Follower Image
     * @param uri <b>(String)</b> - URI that resolves to the endpoint for the Follower Image
     * @return <b>(String)</b> - Full Image URL
     */
    public static String getFollowerImageUrl(String uri) {
        return MobileGarrisonConstants.FOLLOWERS_IMAGE_URL + uri;
    }

    /**
     * Returns the integer to put in the ProgressBar in the
     * @param currentXP <b>(int)</b> - XP value returned from the server that determines the
     *                  amount of XP left before the ProgressBar to be filled
     * @return <b>(int)</b> - Integer to put into the ProgressBar
     */
    public static int getFollowerXP(int currentXP, int totalXp) {
        return (int) Math.round((totalXp - currentXP) * 100.0 / 100000);
    }

    /**
     * Returns a resource ID for a specific Mission Zone ListView Background. <br />
     * This is used in {@link com.blizzard.mobilegarrison.ui.garrison.AvailableMissionsFragment}
     * to show which zone the mission is in
     * @param missionZone <b>(MissionZone)</b> - Zone for a particular mission, determined
     *                    from our Mission API
     * @return <b>(int)</b> - Resource ID for a particular Image Drawable
     */
    public static int getMissionZoneListViewBackgroundDrawable(MissionZone missionZone) {
        switch (missionZone) {
            case BLACKROCK_MOUNTAIN:
                return R.drawable.bg_listview_location_blackrock_mountain;
            case FROSTFIRE_RIDGE:
                return R.drawable.bg_listview_location_frostfire_ridge;
            case GORGROND:
                return R.drawable.bg_listview_location_gorgrond;
            case NAGRAND:
                return R.drawable.bg_listview_location_nagrand;
            case SHADOWMOON_VALLEY:
                return R.drawable.bg_listview_location_shadowmoon_valley;
            case SPIRES_OF_ARAK:
                return R.drawable.bg_listview_location_spires_of_arak;
            case TALADOR:
                return R.drawable.bg_listview_location_talador;
            default:
                return R.drawable.bg_listview_location_frostfire_ridge;
        }
    }

    /**
     * Returns a resource ID for a particular Mission Zone Background. <br />
     * This is used in {@link com.blizzard.mobilegarrison.ui.garrison.mission.MissionDetailFragment}
     * to show which zone the mission is in.
     * @param missionZone <b>(MissionZone)</b> - Zone for a particular mission, determined
     *                    from our Mission API
     * @return <b>(int)</b> - Resource ID for a particular Image Drawable.
     */
    public static int getMissionDetailBackgroundDrawable(MissionZone missionZone) {
        switch (missionZone) {
            case BLACKROCK_MOUNTAIN:
                return R.drawable.location_blackrock_mountain;
            case FROSTFIRE_RIDGE:
                return R.drawable.location_frostfire_ridge;
            case GORGROND:
                return R.drawable.location_gorgrond;
            case NAGRAND:
                return R.drawable.location_nagrand;
            case SHADOWMOON_VALLEY:
                return R.drawable.location_shadowmoon_valley;
            case SPIRES_OF_ARAK:
                return R.drawable.location_spires_of_arak;
            case TALADOR:
                return R.drawable.location_talador;
            default:
                return R.drawable.location_frostfire_ridge;
        }
    }

    /**
     * Returns a Resource ID for the Image Border that goes around a follower icon. This
     * signifies the quality of the follower.
     * @param quality <b>(Quality)</b> - Quality enum that is found within a follower
     * @return <b>(int)</b> - Resource ID
     */
    public static int getFollowerIconBorder(Quality quality) {

        switch (quality) {
            case UNCOMMON:
                return R.drawable.background_item_uncommon;
            case RARE:
                return R.drawable.background_item_rare;
            case EPIC:
                return R.drawable.background_item_epic;
            case LEGENDARY:
                return R.drawable.background_item_legendary;
            default:
                return R.drawable.background_item_default;
        }
    }

    /**
     * Returns a Resource ID for the Image Border that goes around the follower's levle.
     * This signifies the quality of the follower.
     * @param quality <b>(Quality)</b> - Quality enum that is found within a follower
     * @return <b>(int)</b> - Resource ID
     */
    public static int getFollowerLevelBorder(Quality quality) {

        switch (quality) {
            case UNCOMMON:
                return R.drawable.background_item_uncommon_no_border;
            case RARE:
                return R.drawable.background_item_rare_no_border;
            case EPIC:
                return R.drawable.background_item_epic_no_border;
            case LEGENDARY:
                return R.drawable.background_item_legendary_no_border;
            default:
                return R.drawable.background_item_default_no_border;
        }
    }



    /**
     * Returns a Resource ID for a particular Mission Type
     * @param missionType <b>(MissionType)</b> - MissionType associated with a Mission object
     * @return <b>(int)</b> - Resource ID for a particular Image Drawable
     */
    public static int getMissionTypeDrawable(MissionType missionType) {

        if (missionType == null) {
            return R.drawable.ic_mission_type_combat;
        }

        switch (missionType) {
            case COMBAT:
                return R.drawable.ic_mission_type_combat;
            case PATROL:
                return R.drawable.ic_mission_type_patrol;
            case TREASURE:
                return R.drawable.ic_mission_type_treasure;
            default:
                return R.drawable.ic_mission_type_combat;
        }
    }

    /**
     * Returns a Resource ID for a particular Mission Reward
     * @param missionReward <b>(int)</b> - Integer that is part of the Mission object that
     *                      correlates to a particular Image Drawable
     * @return <b>(int)</b> - Resource ID for a particular Image Drawable
     */
    public static int getMissionItemRewardDrawable(int missionReward) {

        switch (missionReward) {
            case REWARD_ABROGATOR_STONE:
                return R.drawable.ic_mission_reward_abrogator_stone;
            case REWARD_APEXIS_CRYSTAL:
                return R.drawable.ic_mission_reward_apexis_crystal;
            case REWARD_ARMOR_ENHANCEMENT_TOKEN:
                return R.drawable.ic_mission_reward_armor_enhancement_token;
            case REWARD_BLACKROCK_ARMOR_SET:
                return R.drawable.ic_blackrock_armor_set;
            case REWARD_BLACKROCK_WEAPONRY:
                return R.drawable.ic_blackrock_weaponry;
            case REWARD_CACHE_OF_HIGHMAUL_TREASURES_HEROIC:
            case REWARD_CACHE_OF_HIGHMAUL_TREASURES_MYTHIC:
            case REWARD_CACHE_OF_HIGHMAUL_TREASURES_NORMAL:
                return R.drawable.ic_mission_reward_cache_of_highmaul_treasures;
            case REWARD_CONQUEST_POINTS:
                return R.drawable.ic_mission_reward_conquest_points;
            case REWARD_ELEMENTAL_RUNE:
                return R.drawable.ic_mission_reward_elemental_rune;
            case REWARD_FOLLOWER_RETRAINING_CERTIFICATE:
                return R.drawable.ic_mission_reward_follower_retraining_certificatet;
            case REWARD_GRANDIOSE_SPAULDERS:
                return R.drawable.ic_mission_reward_grandiose_spaulders;
            case REWARD_HONOR_POINTS:
                return R.drawable.ic_mission_reward_honor_points;
            case REWARD_SEAL_OF_TEMPERED_FATE:
                return R.drawable.ic_mission_reward_seal_of_tempered_fate;
            case REWARD_WEAPON_ENHANCEMENT_TOKEN:
                return R.drawable.ic_mission_reward_weapon_enhancement_token;
            default:
                return R.drawable.ic_mission_reward_unknown;
        }
    }
}
