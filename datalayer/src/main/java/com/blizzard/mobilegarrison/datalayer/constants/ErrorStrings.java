package com.blizzard.mobilegarrison.datalayer.constants;

/**
 * Public class that houses the strings used for our different errors. These strings are passed
 * back through our EventBus to be displayed on the UI portion of the application.
 *
 * @author Joshua Hale (josh@joshuahale.com)
 */
public final class ErrorStrings {

    public static final String ERROR_ACCESS_TOKEN = "Could not retrieve account information. Please try again later.";
    public static final String ERROR_MISSION_NOT_STARTED = "Mission not started. Please try again later.";
    public static final String ERROR_NO_ABILITY_DETAILS= "Could not retrieve ability details for this follower. Please try again later.";
    public static final String ERROR_NO_CHARACTERS = "Could not retrieve characters for this account. Please try again later.";
    public static final String ERROR_NO_ENEMY_DETAILS = "Could not retrieve enemy details for this mission. Please try again later.";
    public static final String ERROR_NO_FOLLOWERS = "Could not retrieve followers for this character. Please try again later.";
    public static final String ERROR_NO_MISSIONS = "Could not retrieve missions for this character. Please try again later.";
    public static final String ERROR_NO_TRAIT_DETAILS = "Could not retrieve trait details for this follower. Please try again later.";
    public static final String ERROR_NOT_ENOUGH_FOLLOWERS = "There are not enough followers on the mission. Please add more followers.";
}
