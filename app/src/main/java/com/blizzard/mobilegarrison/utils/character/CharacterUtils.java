package com.blizzard.mobilegarrison.utils.character;

import android.content.Context;

import com.blizzard.mobilegarrison.R;
import com.blizzard.mobilegarrison.datalayer.objects.character.CharacterClass;
import com.blizzard.mobilegarrison.datalayer.objects.character.CharacterRace;
import com.blizzard.mobilegarrison.datalayer.objects.character.WoWCharacter;

/**
 * Utility class that gets detailed information on a
 * {@link com.blizzard.mobilegarrison.datalayer.objects.character.WoWCharacter} object
 * and returns the data that has been asked for back to the calling class.
 *
 * @author Joshua Hale (josh@joshuahale.com)
 */
public class CharacterUtils {

    private final Context context;

    /**
     * Default constructor
     * @param context <b>(Context)</b> - Activity context used to retrieve
     *                resource information from our applications resource folders
     */
    public CharacterUtils(final Context context) {
        super();
        this.context = context;
    }

    /**
     * Returns a value based on whether or not the character's specified race
     * is either in the Horde faction or the Alliance faction. <br /><br />
     * Returns true if character's race is in the alliance faction. <br />
     * Returns false if the character's race is in the horde faction. <br />
     * @param character <b>(WoWCharacter)</b> - WoWCharcter object that has required metadata
     * @return <b>(boolean)</b> - Flag returned depending on the CharacterRace criteria.
     */
    public boolean isCharacterAlliance(final WoWCharacter character) {

        final CharacterRace race = character.getCharacterRace();

        //-- This if statement checks all of the different alliance races and sees
        //-- if the CharacterRace object stored within the WoWCharacter model matches
        return (race.equals(CharacterRace.DRAENEI) || race.equals(CharacterRace.DWARF)
                || race.equals(CharacterRace.GNOME) || race.equals(CharacterRace.HUMAN)
                || race.equals(CharacterRace.NIGHT_ELF) || race.equals(CharacterRace.WORGEN)
                || race.equals(CharacterRace.PANDAREN_ALLIANCE));
    }

    /**
     * Returns the human-readable version of the character's class.
     * @param characterClass <b>(CharacterClass)</b> - CharacterClass enum
     * @return <b>(String)</b> - String value of the specified enum
     */
    public String getCharacterClassName(final CharacterClass characterClass) {
        switch(characterClass) {
            case DEATH_KNIGHT:
                return context.getString(R.string.class_death_knight);
            case DRUID:
                return context.getString(R.string.class_druid);
            case HUNTER:
                return context.getString(R.string.class_hunter);
            case MAGE:
                return context.getString(R.string.class_mage);
            case MONK:
                return context.getString(R.string.class_monk);
            case PALADIN:
                return context.getString(R.string.class_paladin);
            case PRIEST:
                return context.getString(R.string.class_priest);
            case ROGUE:
                return context.getString(R.string.class_rogue);
            case SHAMAN:
                return context.getString(R.string.class_shaman);
            case WARLOCK:
                return context.getString(R.string.class_warlock);
            case WARRIOR:
                return context.getString(R.string.class_warrior);
        }
        return null;
    }

    /**
     * Returns the Character's race depending on the enum qualifier
     * @param characterRace <b>(CharacterRace)</b> - Enum that contains all the different races
     * @return <b>(String)</b> - String value of the character's race
     */
    public String getCharacterRaceName(final CharacterRace characterRace) {

        switch(characterRace) {
            case BLOOD_ELF:
                return context.getString(R.string.race_blood_elf);
            case DRAENEI:
                return context.getString(R.string.race_draenei);
            case DWARF:
                return context.getString(R.string.race_dwarf);
            case GOBLIN:
                return context.getString(R.string.race_goblin);
            case GNOME:
                return context.getString(R.string.race_gnome);
            case HUMAN:
                return context.getString(R.string.race_human);
            case NIGHT_ELF:
                return context.getString(R.string.race_night_elf);
            case ORC:
                return context.getString(R.string.race_orc);
            case PANDAREN_ALLIANCE: case PANDAREN_HORDE: case PANDAREN_NEUTRAL:
                return context.getString(R.string.race_pandaren);
            case TAUREN:
                return context.getString(R.string.race_tauren);
            case TROLL:
                return context.getString(R.string.race_troll);
            case UNDEAD:
                return context.getString(R.string.race_undead);
            case WORGEN:
                return context.getString(R.string.race_worgen);
        }
        return null;
    }

    public int getCharacterRaceBackground(final CharacterRace characterRace) {
        switch(characterRace) {
            case BLOOD_ELF:
                return R.drawable.modelbg_bloodelf;
            case DRAENEI:
                return R.drawable.modelbg_draenei;
            case DWARF:
                return R.drawable.modelbg_dwarf;
            case GOBLIN:
                return R.drawable.modelbg_goblin;
            case GNOME:
                return R.drawable.modelbg_gnome;
            case HUMAN:
                return R.drawable.modelbg_human;
            case NIGHT_ELF:
                return R.drawable.modelbg_nightelf;
            case ORC:
                return R.drawable.modelbg_orc;
            case PANDAREN_ALLIANCE: case PANDAREN_HORDE: case PANDAREN_NEUTRAL:
                return R.drawable.modelbg_pandaren;
            case TAUREN:
                return R.drawable.modelbg_tauren;
            case TROLL:
                return R.drawable.modelbg_troll;
            case UNDEAD:
                return R.drawable.modelbg_undead;
            case WORGEN:
                return R.drawable.modelbg_worgen;
        }
        return 0;
    }

    /**
     * Returns the resource id for the drawable relating to the current character's class
     * @param characterClass <b>(CharacterClass)</b> - Enum for a specific class
     * @return <b>(int)</b> - Resource ID for Drawable
     */
    public int getClassIcon(final CharacterClass characterClass) {

        switch (characterClass) {
            case DEATH_KNIGHT:
                return R.drawable.class_deathknight;
            case DRUID:
                return R.drawable.class_druid;
            case HUNTER:
                return R.drawable.class_hunter;
            case MAGE:
                return R.drawable.class_mage;
            case PALADIN:
                return R.drawable.class_paladin;
            case PRIEST:
                return R.drawable.class_priest;
            case ROGUE:
                return R.drawable.class_rogue;
            case SHAMAN:
                return R.drawable.class_shaman;
            case WARLOCK:
                return R.drawable.class_warlock;
            case WARRIOR:
                return R.drawable.class_warrior;
            default:
                return 0;
        }
    }

    /**
     * Returns the pre-designated color for the specified CharacterClass enum
     * @param characterClass <b>(CharacterClass)</b> - CharacterClass enum
     * @return <b>(int)</b> - Color resource id linked to CharacterClass enum value
     */
    public int getCharacterColor(final CharacterClass characterClass) {

        int color = 0;

        switch(characterClass) {
            case DEATH_KNIGHT:
                color = R.color.class_death_knight;
                break;
            case DRUID:
                color = R.color.class_druid;
                break;
            case HUNTER:
                color = R.color.class_hunter;
                break;
            case MAGE:
                color = R.color.class_mage;
                break;
            case MONK:
                color = R.color.class_monk;
                break;
            case PALADIN:
                color = R.color.class_paladin;
                break;
            case PRIEST:
                color = R.color.class_priest;
                break;
            case ROGUE:
                color = R.color.class_rogue;
                break;
            case SHAMAN:
                color = R.color.class_shaman;
                break;
            case WARLOCK:
                color = R.color.class_warlock;
                break;
            case WARRIOR:
                color = R.color.class_warrior;
                break;
        }
        return color;
    }
}
