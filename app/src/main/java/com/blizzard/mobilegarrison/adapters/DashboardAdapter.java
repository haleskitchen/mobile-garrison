package com.blizzard.mobilegarrison.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.blizzard.mobilegarrison.R;
import com.blizzard.mobilegarrison.constants.Font;
import com.blizzard.mobilegarrison.datalayer.manager.user.UserManager;
import com.blizzard.mobilegarrison.utils.character.CharacterUtils;
import com.blizzard.mobilegarrison.utils.font.FontUtils;

/**
 * Adapter that is used in the GridView for the DashboardFragment <br />
 * The user selects a specific GridView item and we handle the Activity that they go to. This
 * Adapter shows the user which options are available.
 *
 * @author Joshua Hale (josh@joshuahale.com)
 * @version 0.1
 * @since 0.1
 */
public class DashboardAdapter extends BaseAdapter {

    private final Context context;
    private FontUtils fontUtils;
    private final String[] options;

    /**
     * Constructor takes an Activity context to pull Resources
     * @param context <b>(Context)</b> - Activity Context
     */
    public DashboardAdapter(final Context context) {
        this.context = context;
        options = context.getResources().getStringArray(R.array.dashboard);
    }

    @Override
    public int getCount() {
        return options.length;
    }

    @Override
    public Object getItem(final int position) {
        return null;
    }

    @Override
    public long getItemId(final int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {

        ViewHolder viewHolder;

        if (convertView == null) {
            final LayoutInflater inflater = (LayoutInflater) this.context.getSystemService(
                    Context.LAYOUT_INFLATER_SERVICE);

            fontUtils = new FontUtils(context);

            convertView = inflater.inflate(R.layout.item_dashboard, parent, false);

            viewHolder = new ViewHolder();
            viewHolder.dashboardImageView = (ImageView) convertView.findViewById(R.id.dashboardImageView);
            viewHolder.title = (TextView) convertView.findViewById(R.id.title);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.dashboardImageView.setImageResource(getDashboardImage(position));
        viewHolder.title.setText(options[position]);
        fontUtils.setTypeface(Font.MORPHEUS, viewHolder.title);

        return convertView;
    }

    /**
     * Returns the Dashboard item associated with the specific position.
     * @param position <b>(int)</b> - Position within the GridView
     * @return <b>(int)</b> - Resource ID for Drawable
     * //-- TODO: Re-factor this to LinkedHashMap for ordered properties
     * //-- TODO: and a 1 to 1 mapping of String to Resource ID
     */
    private int getDashboardImage(int position) {

        switch (position) {
            case 0:
                return getFactionGarrisonDrawable();
            case 1:
                return R.drawable.dashboard_auction_house;
            case 2:
                return R.drawable.dashboard_armory;
            case 3:
                return R.drawable.dashboard_guild_chat;
            case 4:
                return R.drawable.dashboard_guild_chat;
            case 5:
                return R.drawable.ic_launcher;
            case 6:
                return R.drawable.dashboard_realm_status;
            case 7:
                return R.drawable.dashboard_forums;
            default:
                return 0;
        }
    }

    /**
     * Returns the drawable for the Garrison portion of the dashboard based
     * on the characters faction
     * @return <b>(int)</b> - Resource ID for the correct faction garrison drawable
     */
    private int getFactionGarrisonDrawable() {
        CharacterUtils charUtils = new CharacterUtils(context);
        UserManager userManager = UserManager.getInstance(context);

        if (charUtils.isCharacterAlliance(userManager.getCurrentWoWCharacter())) {
            return R.drawable.dashboard_garrison_alliance;
        } else {
            return R.drawable.dashboard_garrison_horde;
        }
    }

    /**
     * ViewHolder pattern used to store the views without having to inflate
     * for each re-draw.
     */
    private static class ViewHolder {
        public ImageView dashboardImageView;
        public TextView title;
    }
}
