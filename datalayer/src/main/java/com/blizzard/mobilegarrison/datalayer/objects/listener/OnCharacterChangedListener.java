package com.blizzard.mobilegarrison.datalayer.objects.listener;

import com.blizzard.mobilegarrison.datalayer.objects.character.WoWCharacter;

/**
 * Listener that is fired when a user selects a different character.
 *
 * @author Joshua Hale
 */
public interface OnCharacterChangedListener {

    public void onCharacterChanged(final WoWCharacter wowCharacter);
}
