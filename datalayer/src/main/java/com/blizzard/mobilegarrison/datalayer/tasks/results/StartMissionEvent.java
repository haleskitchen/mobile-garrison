package com.blizzard.mobilegarrison.datalayer.tasks.results;

/**
 * Class that's used by the EventBus to signal that our
 * {@link com.blizzard.mobilegarrison.datalayer.tasks.async.StartMissionTask} has succesfully
 * completed and we can update the UI.
 *
 * @author Joshua Hale (josh@joshuahale.com)
 */
public class StartMissionEvent {

    private final long missionEndTime;

    public StartMissionEvent(long missionEndTime) {
        this.missionEndTime = missionEndTime;
    }

    public long getMissionEndTime() {
        return missionEndTime;
    }
}
