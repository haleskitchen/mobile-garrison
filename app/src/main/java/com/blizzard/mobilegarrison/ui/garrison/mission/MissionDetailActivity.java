package com.blizzard.mobilegarrison.ui.garrison.mission;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.blizzard.mobilegarrison.R;

/**
 * Activity that manages a {@link com.blizzard.mobilegarrison.ui.garrison.mission.MissionDetailFragment}
 * lifecycle
 *
 * @author Joshua Hale
 */
public class MissionDetailActivity extends ActionBarActivity {

    private Toolbar mActionBar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mission_detail);

        mActionBar = (Toolbar) findViewById(R.id.action_bar);

        mActionBar.setTitle(getTitle());
        mActionBar.setTitleTextColor(Color.WHITE);
        mActionBar.setBackgroundResource(R.drawable.actionbar_bg_garrison);

        mActionBar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        setSupportActionBar(mActionBar);

        FragmentTransaction ft = getFragmentManager().beginTransaction();
        Fragment fragment = new MissionDetailFragment();
        fragment.setArguments(getIntent().getExtras());
        ft.add(R.id.container, fragment);
        ft.commit();
    }

    Toolbar getToolbar() {
        return mActionBar;
    }
}
