package com.blizzard.mobilegarrison.datalayer.utils.logging;

import android.util.Log;

/**
 * CustomLog is used to turn off all log messages
 *
 * @author Joshua Hale
 */
public class CustomLog {

    private static final boolean LOG = true;

    /**
     * Logs an INFO object to Android Log
     * @param tag <b>(String)</b> - Tag to use for log
     * @param string <b>(String)</b> - String to log
     */
    public static void i(String tag, String string) {
        if (LOG) {
            Log.i(tag, string);
        }
    }

    /**
     * Logs an ERROR object to Android Log
     * @param tag <b>(String)</b> - Tag to use for log
     * @param string <b>(String)</b> - String to log
     */
    public static void e(String tag, String string) {
        if (LOG) {
            Log.e(tag, string);
        }
    }

    /**
     * Logs an DEBUG object to Android Log
     * @param tag <b>(String)</b> - Tag to use for log
     * @param string <b>(String)</b> - String to log
     */
    public static void d(String tag, String string) {
        if (LOG) {
            Log.d(tag, string);
        }
    }

    /**
     * Logs a VERBOSE object to Android Log
     * @param tag <b>(String)</b> - Tag to use for log
     * @param string <b>(String)</b> - String to log
     */
    public static void v(String tag, String string) {
        if (LOG) {
            android.util.Log.v(tag, string);
        }
    }

    /**
     * Logs a WARN object to Android Log
     * @param tag <b>(String)</b> - Tag to use for log
     * @param string <b>(String)</b> - String to log
     */
    public static void w(String tag, String string) {
        if (LOG) {
            android.util.Log.w(tag, string);
        }
    }
}
