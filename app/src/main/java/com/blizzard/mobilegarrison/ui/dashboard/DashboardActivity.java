package com.blizzard.mobilegarrison.ui.dashboard;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;

import com.blizzard.mobilegarrison.R;
import com.blizzard.mobilegarrison.datalayer.objects.character.WoWCharacter;
import com.blizzard.mobilegarrison.ui.base.BaseActivity;
import com.blizzard.mobilegarrison.ui.base.BaseFragment;

/**
 * Shows the Dashboard where the user can interact with different portions of the application
 *
 * @author Joshua Hale
 */
public class DashboardActivity extends BaseActivity {

    private Fragment currentFragment;

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_base);
        init();
    }

    @Override
    protected void init() {
        super.init();

        mToolbar.setBackgroundResource(R.drawable.actionbar_bg_garrison);

        setStatusBarColor(getResources().getColor(R.color.wow_brown_secondary));

        Fragment fragment = new DashboardFragment();
        currentFragment = fragment;

        final FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.container, fragment, "previous");
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public void onCharacterChanged(final WoWCharacter wowCharacter) {
        super.onCharacterChanged(wowCharacter);

        if (currentFragment instanceof BaseFragment) {
            BaseFragment fragment = (BaseFragment) currentFragment;
            fragment.onCharacterChanged(wowCharacter);
        }
    }

    @Override
    public void setTitle(CharSequence title) {
        super.setTitle(title);

        mToolbar.setTitle(title);
    }
}
