package com.blizzard.mobilegarrison.datalayer.tasks.results;

import com.blizzard.mobilegarrison.datalayer.objects.garrison.mission.Mission;

import java.util.List;

/**
 * Otto Event that is fired when {@link com.blizzard.mobilegarrison.datalayer.tasks.async.GetAvailableMissionsTask}
 * returns a list of Missions
 *
 * @author Joshua Hale
 */
public class GetAvailableMissionsEvent {

    private final List<Mission> missions;

    public GetAvailableMissionsEvent(List<Mission> missions) {
        this.missions = missions;
    }

    public List<Mission> getResult() {
        return this.missions;
    }
}
