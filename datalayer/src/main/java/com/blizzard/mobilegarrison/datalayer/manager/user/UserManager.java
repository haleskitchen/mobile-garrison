package com.blizzard.mobilegarrison.datalayer.manager.user;

import android.content.Context;
import android.content.SharedPreferences;

import com.blizzard.mobilegarrison.datalayer.objects.auth.AccessToken;
import com.blizzard.mobilegarrison.datalayer.objects.character.WoWCharacter;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.apache.commons.codec.binary.Base64;

import java.lang.reflect.Type;
import java.nio.charset.Charset;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Singleton instance that manages any transactions between the UI and the
 * SharedPreferences file for this application.
 *
 * @author Joshua Hale
 */
public class UserManager {

    private static UserManager sInstance;

    private static final String PREF_ACCESS_TOKEN = "PrefAccessToken";
    private static final String PREF_ACCOUNT_ID = "PrefAccountId";
    private static final String PREF_AUTH_CODE = "PrefAuthCode";
    private static final String PREF_CHARACTERS_LAST_UPDATED = "PrefCharactersLastUpdated";
    private static final String PREF_CURRENT_CHARACTERS = "PrefCurrentCharacters";
    private static final String PREF_WOW_CHARACTER = "PrefWoWCharacter";
    private static final String PREFS = "Prefs";
    private final SharedPreferences mPrefs;

    private static final Object mSyncObject = String.class;

    /**
     * Private constructor that gets called when we create a new instance of the UserManager
     * @param context <b>(Context)</b> - Activity context
     */
    private UserManager(Context context) {
        mPrefs = context.getSharedPreferences(PREFS, Context.MODE_PRIVATE);
    }

    /**
     * Creates a singleton instance of our UserManager
     * @param context <b>(Context)</b> - Activity context
     * @return <b>(UserManager)</b> - Singleton instance of our UserManager
     */
    public static UserManager getInstance(Context context) {

        synchronized(mSyncObject) {
            if (sInstance == null) {
                sInstance = new UserManager(context);
            }
        }
        return sInstance;
    }

    /**
     * Timestamp that gets added to SharedPreferences when a list of characters has been
     * added to UserManager.
     */
    private void setCharactersLastUpdated() {
        long timestamp = System.currentTimeMillis();
        getEditor().putLong(PREF_CHARACTERS_LAST_UPDATED, timestamp).commit();
    }

    /**
     * Checks to see if the timestamp we have for the last time our characters is valid.
     * We check to see if the characters have been updated in the last thirty minutes, so that
     * we are not constantly checking the server for new characters.
     * @return <b>(boolean)</b> True if within last thirty minutes, false if not.
     */
    private boolean isCharacterTimestampValid() {
        long timestamp = mPrefs.getLong(PREF_CHARACTERS_LAST_UPDATED, 0);

        long now = System.currentTimeMillis();

        return TimeUnit.MILLISECONDS.toMinutes(timestamp - now) > 30;
    }

    /**
     * Sets the current list of WoWCharacters in the account
     * @param characters <b>(List)</b>
     */
    public void setCharacters(List<WoWCharacter> characters) {
        Gson gson = new Gson();
        String json = gson.toJson(characters);
        getEditor().putString(PREF_CURRENT_CHARACTERS, json).commit();
        setCharactersLastUpdated();
    }

    /**
     * Returns the list of current WoWCharacters in the account.
     * @return <b>(List)</b>
     */
    public List<WoWCharacter> getCurrentWoWCharacters() {

        if (isCharacterTimestampValid()) {
            Gson gson = new Gson();
            String json = mPrefs.getString(PREF_CURRENT_CHARACTERS, "");
            Type type = new TypeToken<List<WoWCharacter>>() {
            }.getType();
            return gson.fromJson(json, type);
        }
        //-- If our timestamp isn't valid return null so we can get a new list.
        return null;
    }

    /**
     * Sets the currently selected WoWCharacter by the user to be the character we
     * use throughout the application
     * @param wowCharacter <b>(WoWCharacter)</b> - Current WoWCharacter
     */
    public void setCurrentWoWCharacter(WoWCharacter wowCharacter) {
        final Gson gson = new Gson();
        final String json = gson.toJson(wowCharacter);
        getEditor().putString(PREF_WOW_CHARACTER, json).commit();
    }

    /**
     * Returns the currently selected WoWCharacter by the user
     * @return <b>(WoWCharacter)</b>
     */
    public WoWCharacter getCurrentWoWCharacter() {
        final Gson gson = new Gson();
        final String json = mPrefs.getString(PREF_WOW_CHARACTER, "");
        return gson.fromJson(json, WoWCharacter.class);
    }

    /**
     * Returns the authorization code we use to get a valid access token
     * @return <b>(String)</b>
     */
    public String getAuthCode() {
        return mPrefs.getString(PREF_AUTH_CODE, null);
    }

    /**
     * Sets the authorization code we get returned from the server
     * @param authCode <b>(String)</b>
     */
    public void setAuthCode(String authCode) {
        getEditor().putString(PREF_AUTH_CODE, authCode).commit();
    }

    /**
     * Returns the AccessToken we use to access Battle.net API calls
     * @return <b>(AccessToken)</b>
     */
    public AccessToken getAccessToken() {
        final Gson gson = new Gson();
        final String json = mPrefs.getString(PREF_ACCESS_TOKEN, "");
        return gson.fromJson(json, AccessToken.class);
    }

    /**
     * Sets the AccessToken we use to access Battle.net APIs
     * @param accessToken <b>(AccessToken)</b>
     */
    public void setAccessToken(AccessToken accessToken) {
        final Gson gson = new Gson();
        final String json = gson.toJson(accessToken);
        getEditor().putString(PREF_ACCESS_TOKEN, json).commit();
    }

    /**
     * Returns the Unique Identifier for the Account ID
     * @return <b>(String)</b>
     */
    public String getAccountId() {
        return mPrefs.getString(PREF_ACCOUNT_ID, null);
    }

    /**
     * Sets the Unique identifier for the Account ID
     * @param accountId <b>(String)</b>
     */
    public void setAccountId(String accountId) {
        getEditor().putString(PREF_ACCOUNT_ID, accountId).commit();
    }

    /**
     * Our Database String (per character), is a Base64 encoded object that
     * encodes the account id and the name of the character. While this is not a perfect
     * solution, it allows different tables to be created in the backend to change
     * character mission data.
     * @return <b>(String)</b> - Returnes the Base64 encoded object for our table.
     */
    public String getDatabaseString() {
        String accountId = getAccountId();
        WoWCharacter wowCharacter = getCurrentWoWCharacter();

        String databaseString = accountId + ":" + wowCharacter.getName();

        byte[] encoded = Base64.encodeBase64(databaseString.getBytes());

        return new String(encoded, Charset.forName("UTF-8"));
    }

    /**
     * Returns a boolean value to determine if the user is logged in or not
     * @return <b>(boolean)</b>
     */
    public boolean isUserLoggedIn() {
        return getAccessToken() != null;
    }

    /**
     * Clears all the user data, called specifically when we log out of the application.
     */
    public void clearUserData() {
        getEditor().clear().commit();
    }

    /**
     * Returns the editor object to other things in SharedPreferences. Convenience method.
     * @return <b>(SharedPreferences.Editor)</b>
     */
    private SharedPreferences.Editor getEditor() {
        return mPrefs.edit();
    }
}
