package com.blizzard.mobilegarrison.datalayer.objects.garrison.follower;


import com.blizzard.mobilegarrison.datalayer.R;

/**
 * Enum that pre-determines which abilities to use for a specific follower. <br />
 * Each ID is attached to a spell within our Spell API to retrieve details. <br />
 * Each Counter is the counter for a specific follower. <br />
 * Each Drawable is the resource id for which image to use for the spell.
 *
 * @author Joshua Hale
 */
public enum FollowerAbility {

    ANTI_MAGIC_SHELL(48707, FollowerCounter.MAGIC_DEBUFF, R.drawable.spell_anti_magic_shell),
    BONE_SHIELD(49222, FollowerCounter.MASSIVE_STRIKE, R.drawable.spell_bone_shield),
    DARK_COMMAND(56222, FollowerCounter.WILD_AGGRESSION, R.drawable.trait_burst_of_power),
    DEATH_AND_DECAY(43265, FollowerCounter.MINION_SWARMS, R.drawable.spell_death_and_decay),
    EMPOWER_RUNE_WEAPON(47568, FollowerCounter.TIMED_BATTLE, R.drawable.spell_empower_rune_weapon),
    MIND_FREEZE(47528, FollowerCounter.POWERFUL_SPELL, R.drawable.spell_mind_freeze),
    BARKSKIN(22812, FollowerCounter.MASSIVE_STRIKE, R.drawable.spell_barkskin),
    BERSERK(50334, FollowerCounter.TIMED_BATTLE, R.drawable.spell_berserk),
    CELESTIAL_ALIGNMENT(160373, FollowerCounter.TIMED_BATTLE, R.drawable.spell_celestial_alignment),
    DASH(1850, FollowerCounter.DANGER_ZONES, R.drawable.spell_dash),
    ENTANGLING_ROOTS(339, FollowerCounter.DEADLY_MINIONS, R.drawable.spell_entangling_roots),
    GROWL(6795, FollowerCounter.WILD_AGGRESSION, R.drawable.spell_growl),
    HURRICANE(16914, FollowerCounter.MINION_SWARMS, R.drawable.spell_hurricane),
    INNERVATE(173565, FollowerCounter.TIMED_BATTLE, R.drawable.spell_innervate),
    NATURES_CURE(173406, FollowerCounter.MAGIC_DEBUFF, R.drawable.spell_natures_cure),
    WILD_GROWTH(48438, FollowerCounter.GROUP_DAMAGE, R.drawable.spell_wild_growth),
    COUNTER_SHOT(147362, FollowerCounter.POWERFUL_SPELL, R.drawable.spell_counter_shot),
    DETERRENCE(19263, FollowerCounter.MASSIVE_STRIKE, R.drawable.spell_deterrence),
    DISENGAGE(781, FollowerCounter.DANGER_ZONES, R.drawable.spell_disengage),
    FEIGN_DEATH(5384, FollowerCounter.WILD_AGGRESSION, R.drawable.spell_feign_death),
    FREEZING_TRAP(1499, FollowerCounter.DEADLY_MINIONS, R.drawable.spell_freezing_trap),
    MULTI_SHOT(2643, FollowerCounter.MINION_SWARMS, R.drawable.spell_multi_shot),
    RAPID_FIRE(3045, FollowerCounter.TIMED_BATTLE, R.drawable.spell_rapid_fire),
    BLINK(1953, FollowerCounter.DANGER_ZONES, R.drawable.spell_blink),
    BLIZZARD(42208, FollowerCounter.MINION_SWARMS, R.drawable.spell_blizzard),
    CONJURE_FOOD(173110, FollowerCounter.TIMED_BATTLE, R.drawable.spell_conjure_food),
    COUNTERSPELL(2139, FollowerCounter.POWERFUL_SPELL, R.drawable.spell_counterspell),
    ICE_BLOCK(45438, FollowerCounter.MASSIVE_STRIKE, R.drawable.spell_ice_block),
    POLYMORPH(118, FollowerCounter.DEADLY_MINIONS, R.drawable.spell_polymorph),
    TIME_WARP(80353, FollowerCounter.TIMED_BATTLE, R.drawable.spell_time_warp),
    CHI_WAVE(132463, FollowerCounter.GROUP_DAMAGE, R.drawable.spell_chi_wave),
    DETOX(115450, FollowerCounter.MAGIC_DEBUFF, R.drawable.spell_detox),
    ENERGIZING_BREW(173377, FollowerCounter.TIMED_BATTLE, R.drawable.spell_energizing_brew),
    GUARD(115295, FollowerCounter.MASSIVE_STRIKE, R.drawable.spell_guard),
    MANA_TEA(115294, FollowerCounter.TIMED_BATTLE, R.drawable.spell_mana_tea),
    PARALYSIS(115078, FollowerCounter.DEADLY_MINIONS, R.drawable.spell_paralysis),
    PROVOKE(115546, FollowerCounter.WILD_AGGRESSION, R.drawable.spell_provoke),
    ROLL(109132, FollowerCounter.DANGER_ZONES, R.drawable.spell_roll),
    SPEAR_HAND_STRIKE(116705, FollowerCounter.POWERFUL_SPELL, R.drawable.spell_spear_hand_strike),
    AVENGING_WRATH(31884, FollowerCounter.TIMED_BATTLE, R.drawable.spell_avenging_wrath),
    CLEANSE(4987, FollowerCounter.MAGIC_DEBUFF, R.drawable.spell_cleanse),
    DIVINE_PLEA(173521, FollowerCounter.TIMED_BATTLE, R.drawable.spell_divine_plea),
    DIVINE_SHIELD(642, FollowerCounter.MASSIVE_STRIKE, R.drawable.spell_divine_shield),
    DIVINE_STORM(53385, FollowerCounter.MINION_SWARMS, R.drawable.spell_divine_storm),
    HOLY_RADIANCE(82327, FollowerCounter.GROUP_DAMAGE, R.drawable.spell_holy_radiance),
    REBUKE(96231, FollowerCounter.POWERFUL_SPELL, R.drawable.spell_rebuke),
    RECKONING(62124, FollowerCounter.WILD_AGGRESSION, R.drawable.spell_reckoning),
    REPENTANCE(20066, FollowerCounter.DEADLY_MINIONS, R.drawable.spell_repentance),
    DISPEL_MAGIC(528, FollowerCounter.MAGIC_DEBUFF, R.drawable.spell_dispel_magic),
    DOMINATE_MIND(605, FollowerCounter.DEADLY_MINIONS, R.drawable.spell_dominate_mind),
    LEAP_OF_FAITH(73325, FollowerCounter.DANGER_ZONES, R.drawable.spell_leap_of_faith),
    MIND_SEAR(48045, FollowerCounter.MINION_SWARMS, R.drawable.spell_mind_sear),
    POWER_INFUSION(10060, FollowerCounter.TIMED_BATTLE, R.drawable.spell_power_infusion),
    PRAYER_OF_HEALING(596, FollowerCounter.GROUP_DAMAGE, R.drawable.spell_prayer_of_healing),
    SHADOWFIEND(34433, FollowerCounter.TIMED_BATTLE, R.drawable.spell_shadowfiend),
    EVASION(5277, FollowerCounter.MASSIVE_STRIKE, R.drawable.spell_evasion),
    FAN_OF_KNIVES(51723, FollowerCounter.MINION_SWARMS, R.drawable.spell_fan_of_knives),
    KICK(1766, FollowerCounter.POWERFUL_SPELL, R.drawable.spell_kick),
    MARKED_FOR_DEATH(137619, FollowerCounter.TIMED_BATTLE, R.drawable.spell_marked_for_death),
    SAP(6770, FollowerCounter.DEADLY_MINIONS, R.drawable.spell_sap),
    SPRINT(2983, FollowerCounter.DANGER_ZONES, R.drawable.spell_sprint),
    ASCENDANCE(114049, FollowerCounter.TIMED_BATTLE, R.drawable.spell_singe_magic),
    CHAIN_HEAL(1064, FollowerCounter.GROUP_DAMAGE, R.drawable.spell_chain_heal),
    CHAIN_LIGHTNING(421, FollowerCounter.MINION_SWARMS, R.drawable.spell_chain_lightning),
    GHOST_WOLF(2645, FollowerCounter.DANGER_ZONES, R.drawable.spell_ghost_wolf),
    HEX(51514, FollowerCounter.DEADLY_MINIONS, R.drawable.spell_hex),
    PURIFY_SPIRIT(77130, FollowerCounter.MAGIC_DEBUFF, R.drawable.spell_natures_cure),
    WATER_SHIELD(52127, FollowerCounter.TIMED_BATTLE, R.drawable.spell_water_shield),
    WIND_SHEAR(57994, FollowerCounter.POWERFUL_SPELL, R.drawable.spell_hurricane),
    DRAIN_LIFE(689, FollowerCounter.GROUP_DAMAGE, R.drawable.spell_drain_life),
    FEAR(5782, FollowerCounter.DEADLY_MINIONS, R.drawable.spell_fear),
    METAMORPHOSIS(103958, FollowerCounter.TIMED_BATTLE, R.drawable.spell_metamorphosis),
    RAIN_OF_FIRE(5740, FollowerCounter.MINION_SWARMS, R.drawable.spell_rain_of_fire),
    SINGE_MAGIC(132411, FollowerCounter.MAGIC_DEBUFF, R.drawable.spell_singe_magic),
    SPELL_LOCK(132409, FollowerCounter.POWERFUL_SPELL, R.drawable.spell_spell_lock),
    SUMMON_INFERNAL(1122, FollowerCounter.TIMED_BATTLE, R.drawable.spell_summon_infernal),
    UNENDING_RESOLVE(104773, FollowerCounter.MASSIVE_STRIKE, R.drawable.spell_unending_resolve),
    CLEAVE(119419, FollowerCounter.MINION_SWARMS, R.drawable.spell_cleave),
    HEROIC_LEAP(6544, FollowerCounter.DANGER_ZONES, R.drawable.spell_heroic_leap),
    PUMMEL(6552, FollowerCounter.POWERFUL_SPELL, R.drawable.spell_pummel),
    RECKLESSNESS(1719, FollowerCounter.TIMED_BATTLE, R.drawable.spell_recklessness),
    SHIELD_WALL(871, FollowerCounter.MASSIVE_STRIKE, R.drawable.spell_shield_wall),
    TAUNT(355, FollowerCounter.WILD_AGGRESSION, R.drawable.counter_wild_aggression);

    private final int followerAbility;
    private final FollowerCounter followerCounter;
    private final int iconResId;

    private FollowerAbility(final int followerAbility, final FollowerCounter followerCounter,
                            final int iconResId) {
        this.followerAbility = followerAbility;
        this.followerCounter = followerCounter;
        this.iconResId = iconResId;
    }

    public int getFollowerAbilityId() {
        return this.followerAbility;
    }

    public FollowerCounter getFollowerCounter() {
        return this.followerCounter;
    }

    public int getCounterResId() {
        return this.followerCounter.getIconResId();
    }

    public int getIconResId() {
        return this.iconResId;
    }

    public static FollowerAbility getEnum(final int value) {
        for (final FollowerAbility followerAbility : values()) {
            if(value == followerAbility.getFollowerAbilityId()) {
                return followerAbility;
            }
        }
        return null;
    }
}
