package com.blizzard.mobilegarrison.ui.garrison;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.support.v13.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerTitleStrip;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.blizzard.mobilegarrison.R;
import com.blizzard.mobilegarrison.constants.Font;
import com.blizzard.mobilegarrison.datalayer.objects.character.WoWCharacter;
import com.blizzard.mobilegarrison.ui.base.BaseFragment;
import com.blizzard.mobilegarrison.ui.garrison.follower.FollowersFragment;
import com.blizzard.mobilegarrison.utils.font.FontUtils;

import java.util.Locale;

/**
 * Fragment that shows the different Fragments pertaining to the Garrison
 * within a ViewPager.
 *
 * @author Joshua Hale
 */
public class GarrisonFragment extends BaseFragment {

    private View view;

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup parent,
                             final Bundle savedInstanceState) {

        getActivity().setTitle(getString(R.string.garrison));
        this.view = inflater.inflate(R.layout.fragment_garrison, parent, false);

        init();

        return this.view;
    }

    /**
     * Initializes the views for the GarrisonFragment
     */
    private void init() {

        final ViewPager viewPager = (ViewPager) this.view.findViewById(R.id.garrison_view_pager);
        final GarrisonPagerAdapter pagerAdapter = new GarrisonPagerAdapter(getChildFragmentManager());
        viewPager.setAdapter(pagerAdapter);
        viewPager.setOffscreenPageLimit(3);

        FontUtils fontUtils = new FontUtils(getActivity());

        PagerTitleStrip titleStrip = (PagerTitleStrip) view.findViewById(R.id.pager_title_strip);

        for (int counter = 0; counter < titleStrip.getChildCount(); counter++) {

            if (titleStrip.getChildAt(counter) instanceof TextView) {

                TextView textView = (TextView) titleStrip.getChildAt(counter);
                fontUtils.setTypeface(Font.MORPHEUS, textView);
                textView.setTextSize(18);
            }
        }
    }

    @Override
    public void onCharacterChanged(final WoWCharacter wowCharacter) {
        init();
    }

    private class GarrisonPagerAdapter extends FragmentStatePagerAdapter {

        private GarrisonPagerAdapter(final FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(final int position) {

            switch (position) {
                case 0:
                    return new AvailableMissionsFragment();
                case 1:
                    return new ActiveMissionsFragment();
                case 2:
                    return new FollowersFragment();
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public CharSequence getPageTitle(final int position) {

            switch (position) {
                case 0:
                    return "Available Missions".toUpperCase(Locale.getDefault());
                case 1:
                    return "Active Missions".toUpperCase(Locale.getDefault());
                case 2:
                    return "Followers".toUpperCase(Locale.getDefault());
            }
            return super.getPageTitle(position);
        }
    }
}
