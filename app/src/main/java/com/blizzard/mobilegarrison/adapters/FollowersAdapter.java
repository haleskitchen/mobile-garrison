package com.blizzard.mobilegarrison.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.blizzard.mobilegarrison.R;
import com.blizzard.mobilegarrison.constants.Font;
import com.blizzard.mobilegarrison.datalayer.objects.garrison.follower.Follower;
import com.blizzard.mobilegarrison.datalayer.objects.garrison.follower.FollowerAbility;
import com.blizzard.mobilegarrison.utils.character.CharacterUtils;
import com.blizzard.mobilegarrison.utils.font.FontUtils;
import com.blizzard.mobilegarrison.utils.garrison.GarrisonUtils;
import com.blizzard.mobilegarrison.utils.lazyload.ImageLoader;

import java.util.List;

/**
 * Adapter for the mFollowers. when we get a list of Followers we then
 * use this adapter to showcase all of the mFollowers to the end user.
 */
public class FollowersAdapter extends BaseAdapter {

    private final List<Follower> mFollowers;
    private CharacterUtils mCharacterUtils;
    private FontUtils mFontUtils;
    private ImageLoader mImageLoader;
    private final Context mContext;

    /**
     * Constructor that takes a list of Followers
     * @param context <b>(Context)</b> - Activity context
     * @param followers <b>(List)</b> - List of mFollowers returned from our DataManager
     */
    public FollowersAdapter(Context context, List<Follower> followers) {
        super();
        mContext = context;
        mFollowers = followers;

        init();
    }

    private void init() {
        mImageLoader = new ImageLoader();
        mCharacterUtils = new CharacterUtils(mContext);
    }

    @Override
    public int getCount() {
        return mFollowers.size();
    }

    @Override
    public Object getItem(int position) {
        return mFollowers.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        //-- New ViewHolder
        ViewHolder viewHolder;

        //-- Uses the ViewHolder pattern for our adapter
        if (convertView == null) {

            mFontUtils = new FontUtils(mContext);

            //-- Retrieve the follower layout.
            LayoutInflater inflater = (LayoutInflater) mContext.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(
                    R.layout.item_follower, parent, false);

            //-- Create a new ViewHolder object.
            viewHolder = new ViewHolder();

            //-- Set the ViewHolder view objects to views returned from our follower layout
            viewHolder.followerClassIcon = (ImageView) convertView.
                    findViewById(R.id.follower_class_icon);
            viewHolder.followerCounter1 = (ImageView) convertView.
                    findViewById(R.id.followerCounter1ImageView);
            viewHolder.followerCounter2 = (ImageView) convertView.
                    findViewById(R.id.followerCounter2ImageView);
            viewHolder.followerIcon = (ImageView) convertView.findViewById(R.id.follower_icon);
            viewHolder.followerIlvl = (TextView) convertView.findViewById(R.id.follower_ilvl);
            viewHolder.followerLevel = (TextView) convertView.findViewById(R.id.follower_level);
            viewHolder.followerName = (TextView) convertView.findViewById(R.id.follower_name);
            viewHolder.followerXpBar = (ProgressBar) convertView.
                    findViewById(R.id.follower_xp_bar);
            viewHolder.xpRemaining = (TextView) convertView.findViewById(R.id.xp_remaining);

            //-- Set the tag to our convertedview as the viewholder
            convertView.setTag(viewHolder);

        } else {
            //-- Return the ViewHolder as the tag from our view
            viewHolder = (ViewHolder) convertView.getTag();
        }

        //-- Retrieve the follower from the position.
        Follower follower = mFollowers.get(position);

        //-- Set the text for the different views from the current follower.
        viewHolder.followerName.setText(follower.getFollowerName());
        viewHolder.followerLevel.setText(String.valueOf(follower.getFollowerLevel()));
        viewHolder.followerIlvl.setText("Ilvl " + String.valueOf(follower.getAverageItemLevel()));

        //-- If our XP for our follower is zero, the follower is both EPIC and level 100
        if (follower.getFollowerTotalXp() == 0) {
            viewHolder.followerXpBar.setVisibility(View.GONE);
            viewHolder.xpRemaining.setVisibility(View.GONE);
        } else {
            int progress = GarrisonUtils.getFollowerXP(follower.getFollowerCurrentXp(), follower.getFollowerTotalXp());
            viewHolder.followerXpBar.setProgress(progress);
            viewHolder.xpRemaining.setText(String.valueOf(follower.getFollowerCurrentXp()) + " XP");
        }

        //-- Changes the background of the icon for the follower and the
        //-- background of the follower level based on the quality of the follower
        viewHolder.followerIcon.setBackgroundResource(GarrisonUtils.getFollowerIconBorder(follower.getQuality()));
        viewHolder.followerLevel.setBackgroundResource(GarrisonUtils.getFollowerLevelBorder(follower.getQuality()));


        //-- Retrieves the size of the follower abilities array
        int size = follower.getFollowerAbilities().size();

        //-- Every follower has at least one ability, so the zero element should always be there
        FollowerAbility followerAbility1 = follower.getFollowerAbilities().get(0);

        //-- Null check just in case
        if (followerAbility1 != null) {
            viewHolder.followerCounter1.setImageResource(followerAbility1.getCounterResId());
            viewHolder.followerCounter1.setVisibility(View.VISIBLE);
        }

        //-- If our size is greater than 1, we know we have two follower abilities
        //-- So either show the ability, or hide the view completely.
        if (size > 1) {
            FollowerAbility followerAbility2 = follower.getFollowerAbilities().get(1);
            if (followerAbility2 != null) {
                viewHolder.followerCounter2.setImageResource(followerAbility2.getCounterResId());
                viewHolder.followerCounter2.setVisibility(View.VISIBLE);
            }
        } else {
            viewHolder.followerCounter2.setVisibility(View.GONE);
        }

        //-- SEt the class icon the the class of the follower
        viewHolder.followerClassIcon.setImageResource(
                mCharacterUtils.getClassIcon(follower.getFollowerClass()));

        //-- LazyLoad the image from the follower object
        String imageUri = follower.getFollowerImageUri();

        //-- Pull in the image for the follower from our backend
        mImageLoader.displayImage(GarrisonUtils.getFollowerImageUrl(imageUri), viewHolder.followerIcon);

        //-- Set the typefaces for our Views.
        mFontUtils.setTypeface(Font.FRIZQT, viewHolder.followerName);
        mFontUtils.setTypeface(Font.ARIAL_NARROW, viewHolder.followerIlvl);

        return convertView;
    }

    /**
     * ViewHolder pattern that gets used for showing our Views
     */
    private static class ViewHolder {
        public ImageView followerIcon;
        public ImageView followerClassIcon;
        public ImageView followerCounter1;
        public ImageView followerCounter2;
        public TextView followerName;
        public TextView followerIlvl;
        public TextView followerLevel;
        public TextView xpRemaining;
        public ProgressBar followerXpBar;
    }
}
