package com.blizzard.mobilegarrison.datalayer.objects.garrison.follower;

/**
 * Model object that holds the details for a FollowerAbility
 *
 * @author Joshua Hale (josh@joshuahale.com)
 */
public class AbilityDetails {

    private String description;
    private String icon;
    private int id;
    private String name;
    private final int iconResId;
    private final int counterResId;

    public AbilityDetails(final FollowerAbility ability) {
        this.iconResId = ability.getIconResId();
        this.counterResId = ability.getCounterResId();
        this.id = ability.getFollowerAbilityId();
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSpellIconId() {
        return iconResId;
    }

    public int getCounterIconId() {
        return counterResId;
    }
}
