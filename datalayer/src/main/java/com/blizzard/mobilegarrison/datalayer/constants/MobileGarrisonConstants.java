package com.blizzard.mobilegarrison.datalayer.constants;

/**
 * Constants file used for different URLs throughout the application
 *
 * @author Joshua Hale (josh@joshuahale.com)
 * @version 0.1
 * @since 0.1
 */
public class MobileGarrisonConstants {

    //-- Personal Database URLs
    private static final String BASE_IMAGE_URL = "http://www.joshuahale.com/mobile_garrison/";
    private static final String BASE_DATABASE_URL = "http://www.joshuahale.com/mobile_garrison/api/";

    //-- URL for WoW character images
    public static final String BATTLE_NET_MEDIA_URL_US = "http://us.battle.net/static-render/us/";

    //-- API for Battle.net information
    public static final String BATTLE_NET_API_URL_US = "https://us.api.battle.net/";
    private static final String WOW_API_BASE_URL = "http://us.battle.net";
    public static final String API_ITEM = WOW_API_BASE_URL + "/api/wow/item/";

    //-- URL for retrieving the icon for an image (56 x 56)
    public static final String ITEM_ICON_URL = "http://us.media.blizzard.com/wow/icons/56/";

    //-- Retrieval of information from personal database
    public static final String AVAILABLE_MISSIONS_URL = BASE_DATABASE_URL + "available_missions.php?";
    public static final String ACTIVE_MISSIONS_URL = BASE_DATABASE_URL + "active_missions.php?";
    public static final String FOLLOWER_ABILITY_URL = BASE_DATABASE_URL + "ability.php?";
    public static final String FOLLOWERS_INACTIVE_URL = BASE_DATABASE_URL + "followers_inactive.php?";
    public static final String FOLLOWER_TRAIT_URL = BASE_DATABASE_URL + "traits.php?";
    public static final String FOLLOWERS_URL = BASE_DATABASE_URL + "followers_all.php?";
    public static final String MISSION_ENEMY_URL = BASE_DATABASE_URL + "mission_enemy.php?";
    public static final String START_MISSION_URL = BASE_DATABASE_URL + "startmission.php?";
    public static final String DELETE_MISSION_URL = BASE_DATABASE_URL + "delete_mission.php?";

    //-- Icon URLs for followers and enemies of garrison missions
    public static final String FOLLOWERS_IMAGE_URL = BASE_IMAGE_URL + "icons/followers/FollowerPortrait_";
    public static final String ENEMY_IMAGE_URL = BASE_IMAGE_URL + "icons/enemy/EnemyPortrait_";

    //-- Battle.net OAuth API Urls and information
    public static final String BATTLE_NET_OAUTH_KEY = "s4ext55be7hqepxzgcradmcvwe2nm77z";
    public static final String BATTLE_NET_OAUTH_SECRET = "mQvPJuhGKJB3MujWdq6e6B8TkMJWbPre";
    public static final String BATTLE_NET_TOKEN_URI = "https://us.battle.net/oauth/token";
    public static final String BATTLE_NET_API_USER_ACCOUNT_ID = "account/user/id?";
    public static final String BATTLE_NET_API_WOW_CHARACTERS = "wow/user/characters?";

    //-- Callback used for Battle net OAuth
    public static final String BATTLE_NET_CALLBACK_URI =
            "https://dev.battle.net/io-docs/oauth2callback";




}
