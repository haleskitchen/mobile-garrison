package com.blizzard.mobilegarrison.utils.color;

import com.blizzard.mobilegarrison.R;
import com.blizzard.mobilegarrison.datalayer.objects.character.Quality;

/**
 * ColorUtils file helps determine the different colors for things in the application
 * from class colors to the color that gets returned for a specific item quality or
 * in this case, follower quality. This will be a living class and will be refactored
 * and added to as necessary for the life of the project.
 *
 * @author Joshua Hale (josh@joshuahale.com)
 */
public class ColorUtils {

    private ColorUtils() {

    }

    public static int getQualityColor(Quality quality) {
        switch (quality) {
            case UNCOMMON:
                return R.color.item_uncommon;
            case RARE:
                return R.color.item_rare;
            case EPIC:
                return R.color.item_epic;
            case LEGENDARY:
                return R.color.item_legendary;
            default:
                return R.color.item_uncommon;
        }
    }
}
