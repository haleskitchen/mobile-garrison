package com.blizzard.mobilegarrison.datalayer.tasks.callable;

import android.net.http.AndroidHttpClient;

import com.blizzard.mobilegarrison.datalayer.manager.data.DataManager;
import com.blizzard.mobilegarrison.datalayer.objects.garrison.mission.MissionEnemy;
import com.blizzard.mobilegarrison.datalayer.utils.json.JSONUtils;

import org.apache.http.client.methods.HttpGet;

import java.io.InputStream;
import java.util.concurrent.Callable;

/**
 * Callable that is created when we are trying to retrieve the Enemy for a specific mission.
 * Once the callable has been created it gets put into our thread pool.
 *
 * @author Joshua Hale
 */
public class GetMissionEnemyCallable implements Callable<MissionEnemy> {

    private final String mUrl;

    public GetMissionEnemyCallable(String url) {
        mUrl = url;
    }

    @Override
    public MissionEnemy call() throws Exception {

        final HttpGet httpGet = new HttpGet(mUrl);

        AndroidHttpClient client = AndroidHttpClient.newInstance("Android");

        try {
            InputStream inputStream = DataManager.getInputStream(client, httpGet);

            String rawString = DataManager.getStringFromInputStream(inputStream);

            JSONUtils jsonUtils = new JSONUtils();

            return jsonUtils.getMissionEnemy(rawString);
        } finally {
            client.close();
        }
    }
}
