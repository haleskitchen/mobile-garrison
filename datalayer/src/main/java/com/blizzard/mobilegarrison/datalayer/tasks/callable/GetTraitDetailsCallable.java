package com.blizzard.mobilegarrison.datalayer.tasks.callable;

import android.net.http.AndroidHttpClient;

import com.blizzard.mobilegarrison.datalayer.manager.data.DataManager;
import com.blizzard.mobilegarrison.datalayer.objects.garrison.follower.FollowerTrait;
import com.blizzard.mobilegarrison.datalayer.objects.garrison.follower.TraitDetails;
import com.blizzard.mobilegarrison.datalayer.utils.json.JSONUtils;

import org.apache.http.client.methods.HttpGet;

import java.io.InputStream;
import java.util.concurrent.Callable;

/**
 * Callable that is created when we are attempting to retrieve the Details
 * for a specific Follower Trait. Once the callable has been created, it is
 * thrown into our ThreadPool.
 *
 * @author Joshua Hale
 */
public class GetTraitDetailsCallable implements Callable<TraitDetails> {

    private final FollowerTrait followerTrait;
    private final String url;

    public GetTraitDetailsCallable(final FollowerTrait followerTrait, final String url) {
        this.followerTrait = followerTrait;
        this.url = url;
    }

    @Override
    public TraitDetails call() throws Exception {

        final HttpGet httpGet = new HttpGet(url);

        final AndroidHttpClient client = AndroidHttpClient.newInstance("Android");

        try {
            final InputStream inputStream = DataManager.getInputStream(client, httpGet);

            final String rawString = DataManager.getStringFromInputStream(inputStream);

            final JSONUtils jsonUtils = new JSONUtils();
            return jsonUtils.getTraitDetails(followerTrait, rawString);
        } finally {
            client.close();
        }
    }
}