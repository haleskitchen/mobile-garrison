package com.blizzard.mobilegarrison.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.blizzard.mobilegarrison.R;
import com.blizzard.mobilegarrison.constants.Font;
import com.blizzard.mobilegarrison.datalayer.manager.user.UserManager;
import com.blizzard.mobilegarrison.utils.character.CharacterUtils;
import com.blizzard.mobilegarrison.utils.font.FontUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Adapter that manages what will show up in the List for our Toolbar. The
 * items are pre-populated and have designated classes to go to when clicked
 *
 * @author Joshua Hale
 */
public class DrawerAdapter extends BaseAdapter {

    private final FontUtils mFontUtils;
    private final List<DrawerItem> mDrawerItems;
    private final Context mContext;

    public DrawerAdapter(Context context) {
        mContext = context;
        mFontUtils = new FontUtils(context);
        mDrawerItems = new ArrayList<>();

        setupSliderData();
    }

    private void setupSliderData() {

        mDrawerItems.add(new DrawerItem(mContext.getString(R.string.garrison), getFactionGarrisonDrawable()));
        mDrawerItems.add(new DrawerItem(mContext.getString(R.string.auction_house), R.drawable.dashboard_auction_house));
        mDrawerItems.add(new DrawerItem(mContext.getString(R.string.armory), R.drawable.dashboard_armory));
        mDrawerItems.add(new DrawerItem(mContext.getString(R.string.guild_chat), R.drawable.dashboard_guild_chat));
        mDrawerItems.add(new DrawerItem(mContext.getString(R.string.guild_info), R.drawable.dashboard_guild_chat));
        mDrawerItems.add(new DrawerItem(mContext.getString(R.string.talent_calculator), R.drawable.ic_launcher));
        mDrawerItems.add(new DrawerItem(mContext.getString(R.string.realm_status), R.drawable.dashboard_realm_status));
        mDrawerItems.add(new DrawerItem(mContext.getString(R.string.forums), R.drawable.dashboard_forums));
        mDrawerItems.add(new DrawerItem(mContext.getString(R.string.settings), R.drawable.ic_launcher));
    }

    @Override
    public int getCount() {
        return mDrawerItems.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;

        if (convertView == null) {

            viewHolder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(
                    Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_drawer, null);

            viewHolder.drawerImageView = (ImageView) convertView.findViewById(R.id.drawerImageView);
            viewHolder.drawerTextView = (TextView) convertView.findViewById(R.id.drawerTextView);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        DrawerItem drawerItem = mDrawerItems.get(position);

        viewHolder.drawerImageView.setImageResource(drawerItem.getDrawerResId());
        viewHolder.drawerTextView.setText(drawerItem.getDrawerTitle());

        mFontUtils.setTypeface(Font.FRIZQT, viewHolder.drawerTextView);

        return convertView;
    }

    /**
     * Returns the drawable for the Garrison portion of the dashboard based
     * on the characters faction
     * @return <b>(int)</b> - Resource ID for the correct faction garrison drawable
     */
    private int getFactionGarrisonDrawable() {
        CharacterUtils charUtils = new CharacterUtils(mContext);
        UserManager userManager = UserManager.getInstance(mContext);

        if (charUtils.isCharacterAlliance(userManager.getCurrentWoWCharacter())) {
            return R.drawable.dashboard_garrison_alliance;
        } else {
            return R.drawable.dashboard_garrison_horde;
        }
    }

    static class ViewHolder {
        ImageView drawerImageView;
        TextView drawerTextView;
    }

    private class DrawerItem {

        final String drawerTitle;
        final Integer drawerResId;
        
        public DrawerItem(String title, int resId) {
            drawerTitle = title;
            drawerResId = resId;
        }

        public String getDrawerTitle() {
            return drawerTitle;
        }

        public Integer getDrawerResId() {
            return drawerResId;
        }
    }
}
