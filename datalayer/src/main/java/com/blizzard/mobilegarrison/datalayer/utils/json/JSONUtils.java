package com.blizzard.mobilegarrison.datalayer.utils.json;

import com.blizzard.mobilegarrison.datalayer.objects.auth.AccessToken;
import com.blizzard.mobilegarrison.datalayer.objects.character.Quality;
import com.blizzard.mobilegarrison.datalayer.objects.character.WoWCharacter;
import com.blizzard.mobilegarrison.datalayer.objects.garrison.follower.AbilityDetails;
import com.blizzard.mobilegarrison.datalayer.objects.garrison.follower.Follower;
import com.blizzard.mobilegarrison.datalayer.objects.garrison.follower.FollowerAbility;
import com.blizzard.mobilegarrison.datalayer.objects.garrison.follower.FollowerCounter;
import com.blizzard.mobilegarrison.datalayer.objects.garrison.follower.FollowerTrait;
import com.blizzard.mobilegarrison.datalayer.objects.garrison.follower.TraitDetails;
import com.blizzard.mobilegarrison.datalayer.objects.garrison.mission.Mission;
import com.blizzard.mobilegarrison.datalayer.objects.garrison.mission.MissionEnemy;
import com.blizzard.mobilegarrison.datalayer.objects.item.WoWItem;
import com.blizzard.mobilegarrison.datalayer.parser.character.WoWCharacterParser;
import com.blizzard.mobilegarrison.datalayer.parser.garrison.FollowersParser;
import com.blizzard.mobilegarrison.datalayer.parser.garrison.MissionsParser;
import com.blizzard.mobilegarrison.datalayer.utils.logging.CustomLog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Utility class that helps parse all the different JSON returns from different APIs
 *
 * @author Joshua Hale
 */
public class JSONUtils {

    //-- Different fields found within our JSON objects
    private static final String FIELD_ACCESS_TOKEN = "access_token";
    private static final String FIELD_ACCOUNT_ID = "accountId";
    private static final String FIELD_COUNTERS = "counters";
    private static final String FIELD_DESCRIPTION = "description";
    private static final String FIELD_EXPIRES_IN = "expires_in";
    private static final String FIELD_ICON = "icon";
    private static final String FIELD_ICON_URI = "iconUrl";
    private static final String FIELD_ID = "id";
    private static final String FIELD_NAME = "name";
    private static final String FIELD_QUALITY = "quality";
    private static final String FIELD_SCOPE = "scope";
    private static final String FIELD_TOKEN_TYPE = "token_type";

    private static final String JPG = ".jpg";

    private static final String TAG = "JSONUtils";

    public JSONUtils() {
        super();
    }

    /**
     * Parses a raw input string returned from the Battle.net API and gives a list
     * of the current WoW Characters the user has.
     * @param inputString <b>(String)</b> - JSON String that contains all of the character data
     * @return <b>(List)</b> - List of WoWCharacters for the current logged in user.
     */
    public List<WoWCharacter> getWoWCharacters(String inputString) {

        WoWCharacterParser characterParser = new WoWCharacterParser();
        return characterParser.getWoWCharacters(inputString);
    }

    /**
     * Returns a list of Followers from our Followers API
     * @param inputString <b>(String)</b> - Raw String input
     * @return <b>(List)</b> - List of Followers returned from our parser
     */
    public List<Follower> getFollowers(String inputString) {

        FollowersParser parser = new FollowersParser();

        return parser.getFollowers(inputString);
    }

    /**
     * Returns a list of Missions from our Missions API
     * @param inputString <b>(String)</b> - Raw string input
     * @return <b>(List)</b> - List of Missions returned fro mour parser
     */
    public List<Mission> getMissions(String inputString) {

        MissionsParser missionsParser = new MissionsParser();
        return missionsParser.getMissions(inputString);
    }

    /**
     * Gets our account id from a raw input String
     * @param inputString <b>(String)</b> - Raw string input
     * @return <b>(String)</b> - Account ID
     */
    public String getAccountId(final String inputString) {

        try {
            final JSONObject jsonObject = new JSONObject(inputString);

            return jsonObject.getString(FIELD_ID);
        } catch (final JSONException e) {
            CustomLog.w(TAG, "Error parsing JSON: " + e.getMessage());
        }

        return null;
    }

    public MissionEnemy getMissionEnemy(String inputString) {
        if (inputString.isEmpty()) {
            return null;
        }

        try {
            JSONArray jsonArray = new JSONArray(inputString);

            JSONObject jsonObject = jsonArray.getJSONObject(0);

            if (jsonObject != null) {
                MissionEnemy missionEnemy = new MissionEnemy();

                missionEnemy.setId(Integer.valueOf(jsonObject.getString(FIELD_ID)));
                missionEnemy.setName(jsonObject.getString(FIELD_NAME));
                missionEnemy.setIconUri(jsonObject.getString(FIELD_ICON_URI));

                String counter = jsonObject.getString(FIELD_COUNTERS);

                String[] counterArray = counter.split(",");

                List<FollowerCounter> counters = new ArrayList<>();

                for (String s : counterArray) {
                    int enemyId = Integer.valueOf(s);
                    FollowerCounter followerCounter = FollowerCounter.getFollowerCounter(enemyId);
                    counters.add(followerCounter);
                }

                missionEnemy.setCounters(counters);

                return missionEnemy;
            }
        } catch (JSONException e) {
            CustomLog.w(TAG, "Error parsing JSON: " + e.getMessage());
        }

        return null;
    }

    /**
     * Returns an access token from the Input
     * @param inputString <b>(String)</b> - Raw input string
     * @return <b>(AccessToken)</b> - AccessToken to access character information
     */
    public AccessToken getAccessToken(final String inputString) {

        try {
            final JSONObject jsonObject = new JSONObject(inputString);

            final AccessToken accessToken = new AccessToken();

            accessToken.setAccessToken(jsonObject.getString(FIELD_ACCESS_TOKEN));
            accessToken.setAccountId(jsonObject.getString(FIELD_ACCOUNT_ID));
            accessToken.setTokenType(jsonObject.getString(FIELD_TOKEN_TYPE));
            accessToken.setExpiresIn(jsonObject.getLong(FIELD_EXPIRES_IN));

            final String scope = jsonObject.getString(FIELD_SCOPE);
            final String[] scopeArray = scope.split("\\s+");
            accessToken.setScope(scopeArray);

            return accessToken;
        } catch (final JSONException e) {
            CustomLog.w(TAG, "Error parsing JSON: " + e.getMessage());
        }

        return null;
    }

    /**
     * Return an AbilityDetails Object from a Follower Ability object
     * @param followerAbility <b>(FollowerAbility)</b> - FollowerAbility that is tied to a Follower
     * @param inputString <b>(String)</b> - Raw string input from Follower Ability Detail API
     * @return <b>(AbilityDetails)</b> - Details for a specific Follower ability
     */
    public AbilityDetails getAbilityDetails(final FollowerAbility followerAbility, final String inputString) {

        final AbilityDetails abilityDetails = new AbilityDetails(followerAbility);

        try {
            JSONArray jsonArray = new JSONArray(inputString);

            if (jsonArray.length() > 0) {
                final JSONObject jsonObject = jsonArray.getJSONObject(0);
                abilityDetails.setName(jsonObject.getString(FIELD_NAME));
                abilityDetails.setDescription(jsonObject.getString(FIELD_DESCRIPTION));
            }
        } catch (JSONException e) {
            CustomLog.w(TAG, "Error parsing JSON: " + e.getMessage());
        }

        return abilityDetails;
    }

    /**
     * Returns a TraitDetails object from our Trait Details API
     * @param followerTrait <b>(FollowerTrait)</b> - FollowerTrait that is tied to a Follower
     * @param inputString <b>(String)</b> - Raw string input from Follower Trait API
     * @return <b>(TraitDetails)</b> - Details for a specific Follower Trait.
     */
    public TraitDetails getTraitDetails(final FollowerTrait followerTrait, final String inputString) {

        TraitDetails traitDetails = new TraitDetails(followerTrait);

        try {
            JSONArray jsonArray = new JSONArray(inputString);

            if (jsonArray.length() > 0) {
                final JSONObject jsonObject = jsonArray.getJSONObject(0);
                traitDetails.setName(jsonObject.getString(FIELD_NAME));
                traitDetails.setDescription(jsonObject.getString(FIELD_DESCRIPTION));
            }
        } catch (JSONException e) {
            CustomLog.w(TAG, "Error parsing JSON: " + e.getMessage());
        }

        return traitDetails;
    }

    /**
     * Returns a WoWItem object from our Item API
     * @param inputString <b>(String)</b> - Raw String input
     * @return <b>(WoWItem)</b> - Object that has all information for a WoWItem
     */
    public WoWItem getWowItem(final String inputString) {

        try {

            WoWItem wowItem = new WoWItem();
            JSONObject itemObject = new JSONObject(inputString);

            wowItem.setId(itemObject.getInt(FIELD_ID));
            wowItem.setIcon(itemObject.getString(FIELD_ICON) + JPG);
            wowItem.setName(itemObject.getString(FIELD_NAME));
            wowItem.setDescription(itemObject.getString(FIELD_DESCRIPTION));
            wowItem.setQuality(Quality.getEnum(itemObject.getInt(FIELD_QUALITY) - 1));


            return wowItem;
        } catch (JSONException e) {
            CustomLog.w(TAG, "Error parsing JSON: " + e.getMessage());
        }

        return null;
    }
}
