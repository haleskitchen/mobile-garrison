package com.blizzard.mobilegarrison.datalayer.tasks.callable;

import android.net.http.AndroidHttpClient;

import com.blizzard.mobilegarrison.datalayer.constants.MobileGarrisonConstants;
import com.blizzard.mobilegarrison.datalayer.manager.data.DataManager;

import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

/**
 * We use this to delete a single mission from the database. Since this is used in a Callable
 * we can actually multithread our delete calls from our database.
 *
 * @author Joshua Hale (josh@joshuahale.com)
 */
public class DeleteMissionCallable implements Callable<Void> {

    private final int mMissionId;
    private final String mUniqueId;

    private static final String MISSION_ID = "mission_id";
    private static final String UNIQUE_ID = "unique_id";

    public DeleteMissionCallable(String uniqueId, int missionId) {
        mMissionId = missionId;
        mUniqueId = uniqueId;
    }

    @Override
    public Void call() throws Exception {

        String url = MobileGarrisonConstants.DELETE_MISSION_URL;

        List<NameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair(MISSION_ID, String.valueOf(mMissionId)));
        params.add(new BasicNameValuePair(UNIQUE_ID, mUniqueId));
        String paramString = URLEncodedUtils.format(params, "UTF-8");
        url = url + paramString;

        AndroidHttpClient httpClient = AndroidHttpClient.newInstance("Android");
        HttpGet httpGet = new HttpGet(url);

        try {
            InputStream inputStream = DataManager.getInputStream(httpClient, httpGet);

            String rawString = DataManager.getStringFromInputStream(inputStream);
        } finally {
            httpClient.close();
        }

        return null;
    }
}
