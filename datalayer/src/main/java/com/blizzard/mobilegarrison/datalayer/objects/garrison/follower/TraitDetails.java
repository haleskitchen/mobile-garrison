package com.blizzard.mobilegarrison.datalayer.objects.garrison.follower;

/**
 * Object that holds the data for a specific Trait
 *
 * @author Joshua Hale
 */
public class TraitDetails {

    private String name;
    private String description;
    private int id;
    private int iconResId;

    public TraitDetails(FollowerTrait followerTrait) {
        this.iconResId = followerTrait.getIconResId();
        this.id = followerTrait.getFollowerTrait();
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(final int id) {
        this.id = id;
    }

    public int getIconResId() {
        return iconResId;
    }

    public void setIconResId(final int iconResId) {
        this.iconResId = iconResId;
    }
}
