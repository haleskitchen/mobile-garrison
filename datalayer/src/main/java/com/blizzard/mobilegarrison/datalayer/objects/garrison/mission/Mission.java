package com.blizzard.mobilegarrison.datalayer.objects.garrison.mission;

import com.blizzard.mobilegarrison.datalayer.objects.character.Faction;
import com.blizzard.mobilegarrison.datalayer.objects.character.Quality;

import java.io.Serializable;

/**
 * Object that contains all data pertaining to a specific Mission
 *
 * @author Joshua Hale
 */
public class Mission implements Serializable {


    private static final long serialVersionUID = -1292556548747570546L;
    private int Id;
    private int startTime;
    private int endTime;
    private Faction faction;
    private int missionIlvl;
    private int missionDuration;
    private Quality missionRarity;
    private MissionType missionType;
    private String missionEnvironment;
    private int missionLevel;
    private String missionName;
    private String missionDescription;
    private int missionEncounter1;
    private int missionEncounter2;
    private int missionEncounter3;
    private int garrisonResourceCost;
    private int xpGain;
    private MissionZone missionZone;
    private String missionLocation;
    private int successChance;
    private int followerLimit;
    private int availableHours;
    private int availableDays;

    private MissionReward missionReward;

    public int getMissionId() {
        return Id;
    }

    public void setMissionId(final int Id) {
        this.Id = Id;
    }

    public int getStartTime() {
        return startTime;
    }

    public void setStartTime(final int startTime) {
        this.startTime = startTime;
    }

    public int getId() {
        return Id;
    }

    public void setId(final int id) {
        Id = id;
    }

    public int getEndTime() {
        return endTime;
    }

    public void setEndTime(final int endTime) {
        this.endTime = endTime;
    }

    public Faction getFaction() {
        return faction;
    }

    public void setFaction(final Faction faction) {
        this.faction = faction;
    }

    public int getMissionIlvl() {
        return missionIlvl;
    }

    public void setMissionIlvl(final int missionIlvl) {
        this.missionIlvl = missionIlvl;
    }

    public int getMissionDuration() {
        return missionDuration;
    }

    public void setMissionDuration(final int missionDuration) {
        this.missionDuration = missionDuration;
    }

    public Quality getMissionRarity() {
        return missionRarity;
    }

    public void setMissionRarity(final Quality missionRarity) {
        this.missionRarity = missionRarity;
    }

    public MissionType getMissionType() {
        return missionType;
    }

    public void setMissionType(final MissionType missionType) {
        this.missionType = missionType;
    }

    public String getMissionEnvironment() {
        return missionEnvironment;
    }

    public void setMissionEnvironment(final String missionEnvironment) {
        this.missionEnvironment = missionEnvironment;
    }

    public int getMissionLevel() {
        return missionLevel;
    }

    public void setMissionLevel(final int missionLevel) {
        this.missionLevel = missionLevel;
    }

    public String getMissionName() {
        return missionName;
    }

    public void setMissionName(final String missionName) {
        this.missionName = missionName;
    }

    public String getMissionDescription() {
        return missionDescription;
    }

    public void setMissionDescription(final String missionDescription) {
        this.missionDescription = missionDescription;
    }

    public int getMissionEncounter1() {
        return missionEncounter1;
    }

    public void setMissionEncounter1(final int missionEncounter1) {
        this.missionEncounter1 = missionEncounter1;
    }

    public int getMissionEncounter2() {
        return missionEncounter2;
    }

    public void setMissionEncounter2(final int missionEncounter2) {
        this.missionEncounter2 = missionEncounter2;
    }

    public int getMissionEncounter3() {
        return missionEncounter3;
    }

    public void setMissionEncounter3(final int missionEncounter3) {
        this.missionEncounter3 = missionEncounter3;
    }

    public int getGarrisonResourceCost() {
        return garrisonResourceCost;
    }

    public void setGarrisonResourceCost(final int garrisonResourceCost) {
        this.garrisonResourceCost = garrisonResourceCost;
    }

    public int getXpGain() {
        return xpGain;
    }

    public void setXpGain(final int xpGain) {
        this.xpGain = xpGain;
    }

    public MissionZone getMissionZone() {
        return missionZone;
    }

    public void setMissionZone(MissionZone missionZone) {
        this.missionZone = missionZone;
    }

    public void setMissionLocation(String missionLocation) {
        this.missionLocation = missionLocation;
    }

    public String getMissionLocation() {
        return missionLocation;
    }

    public int getSuccessChance() {
        return successChance;
    }

    public void setSuccessChance(final int successChance) {
        this.successChance = successChance;
    }

    public int getFollowerLimit() {
        return followerLimit;
    }

    public void setFollowerLimit(final int followerLimit) {
        this.followerLimit = followerLimit;
    }

    public int getAvailableHours() {
        return availableHours;
    }

    public void setAvailableHours(final int availableHours) {
        this.availableHours = availableHours;
    }

    public int getAvailableDays() {
        return availableDays;
    }

    public void setAvailableDays(final int availableDays) {
        this.availableDays = availableDays;
    }

    public MissionReward getMissionReward() {
        return missionReward;
    }

    public void setMissionReward(final MissionReward missionReward) {
        this.missionReward = missionReward;
    }
}
