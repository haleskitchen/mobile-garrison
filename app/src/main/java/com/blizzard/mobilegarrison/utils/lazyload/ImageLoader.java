package com.blizzard.mobilegarrison.utils.lazyload;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.util.Log;
import android.widget.ImageView;

import com.blizzard.mobilegarrison.R;
import com.blizzard.mobilegarrison.datalayer.utils.logging.CustomLog;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collections;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * This class is an implementation of LazyLoading. We can send the URL and
 * specified ImageView to the ImageLoader and have it load the Bitmap
 * inside the ImageView once the URL has finished being processed. The ViewHolder
 * pattern must be used so that the reference of the ImageView in the ListView
 * or GridView can be reloaded (from the cache) if the view goes out of the user's
 * currently visible elements.
 *
 * @author qxg6106 (Joshua.Hale@partner.bmwgroup.com)
 */
public class ImageLoader {

    private static final String TAG = "ImageLoader";

    //-- Memory Cache used for short term purposes
    private final MemoryCache memoryCache = new MemoryCache();

    //-- Creates a WeakReference map of the ImageViews, so that we can
    //-- add Bitmaps to the ImageViews when we have them
    private final Map<ImageView, String> imageViews = Collections.synchronizedMap(
            new WeakHashMap<ImageView, String>());

    //-- Used to execute Runnables as we add them to our queue
    private final ExecutorService executorService;

    private final Handler handler = new Handler();

    //-- Color to show when the drawable hasn't been loaded yet.
    private static final int STUB_ID = R.drawable.follower_placeholder;

    /**
     * ImageLoader Constructor requires the context to create a new cache
     * on our File system
     */
    public ImageLoader() {

        executorService = Executors.newFixedThreadPool(5);
    }

    /**
     * Displayes a specific Bitmap (from a URL) to an ImageView
     * @param url <b>(String)</b> - URL that contains a Bitmap Image
     * @param imageView <b>(ImageView)</b> - ImageView that should show the Bitmap
     */
    public void displayImage(final String url, final ImageView imageView) {

        //-- Add this to our weak reference of image views for later
        imageViews.put(imageView, url);

        final Bitmap bitmap = memoryCache.get(url);

        //-- If our bitmap is in our memory cache, use it else download it.
        if(bitmap != null) {
            imageView.setImageBitmap(bitmap);
        } else {
            queuePhoto(url, imageView);

            imageView.setImageResource(STUB_ID);
        }
    }

    /**
     * Adds our Photo object that hasn't been downloaded to our Executor
     * @param url <b>(String)</b> - URL for image to be downloaded
     * @param imageView <b>(ImageView)</b> - ImageView to tie the Bitmap to the URL
     */
    private void queuePhoto(final String url, final ImageView imageView) {

        final UnloadedPhoto photo = new UnloadedPhoto(url, imageView);

        executorService.submit(new PhotosLoader(photo));
    }

    /**
     * Used as a holder object while we download objects from the Executor
     */
    private static class UnloadedPhoto {

        private final String url;
        private final ImageView imageView;

        public UnloadedPhoto(final String url, final ImageView imageView) {
            this.url = url;
            this.imageView = imageView;
        }

        public String getUrl() {
            return this.url;
        }

        public ImageView getImageView() {
            return this.imageView;
        }
    }

    /**
     * Loads our specific Photo from our Executor if we can find it
     */
    private class PhotosLoader implements Runnable {

        private final UnloadedPhoto photo;

        PhotosLoader(final UnloadedPhoto photo) {
            this.photo = photo;
        }

        @Override
        public void run() {

            //-- If our image view is already in use, don't add a new photo
            if(imageViewReused(photo)) {
                return;
            }

            //-- Get the bitmap from the specified url
            final Bitmap bitmap = getBitmap(photo.url);

            //-- Add our Bitmap to our MemoryCache
            memoryCache.put(photo.url, bitmap);

            //-- Check again for the ImageView in case we have already added the photo
            if(imageViewReused(photo)) {
                return;
            }

            //-- Display the Bitmap
            final BitmapDisplayer bd = new BitmapDisplayer(bitmap, photo);

            handler.post(bd);
        }
    }

    /**
     * Retrieves the Bitmap from the specified URL
     * @param url <b>(String)</b> - URL to retrieve the specified Bitmap
     * @return <b>(Bitmap)</b> - Bitmap that has been decoded and added to our cache
     */
    private Bitmap getBitmap(final String url) {

        try {
            Bitmap bitmap;

            final URL imageUrl = new URL(url);
            final HttpURLConnection connection = (HttpURLConnection)imageUrl.openConnection();
            connection.setConnectTimeout(30000);
            connection.setReadTimeout(30000);
            connection.setInstanceFollowRedirects(true);

            final InputStream is = connection.getInputStream();

            bitmap = BitmapFactory.decodeStream(is);

            return bitmap;
        } catch (final MalformedURLException e) {
            CustomLog.w(TAG, "MalformedURLException: " + e.getMessage());
        } catch (final IOException e) {
            Log.i(TAG, "IOException: No image found at URL: " + e.getMessage());
        }

        return null;
    }

    /**
     * Decodes our Bitmap from our File object
     * @param f <b>(File)</b> - File in which to download our Bitmap
     * @return <b>(Bitmap)</b> - Returned Bitmap after decoding
     */
    private Bitmap decodeFile(final File f){

        try {
            final FileInputStream stream = new FileInputStream(f);
            final Bitmap bitmap = BitmapFactory.decodeStream(stream);
            stream.close();
            return bitmap;

        } catch (final FileNotFoundException e) {
            Log.w(TAG, "File was not found: " + e.getMessage());
        }
        catch (final IOException e) {
            Log.w(TAG, "IOException: " + e.getMessage());
        }
        return null;
    }

    /**
     * Checks to see if the ImageView we are trying to add a Bitmap to
     * has already been used (since they should be Unique IDs)
     * @param photoToLoad <b>(UnloadedPhoto)</b> - Holder object for URL and ImageView
     * @return <b>(bollean)</b> - Returns true if reference is within our HashMap
     */
    private boolean imageViewReused(final UnloadedPhoto photoToLoad){

        final String tag = imageViews.get(photoToLoad.imageView);

        return tag == null || !tag.equals(photoToLoad.url);

    }

    /**
     * Displays the Bitmap on the UI Thread via this Runnable
     */
    private class BitmapDisplayer implements Runnable {
        private final Bitmap bitmap;
        private final UnloadedPhoto photoToLoad;

        /**
         * Constructor for Runnable, holds the data for the UI Thread
         * @param bitmap <b>(Bitmap)</b> - Bitmap to Display
         * @param photoToLoad <b>(UnloadedPhoto)</b> - Photo encapsulation object that
         *                    holds the ImageView to display specific Bitmap
         */
        public BitmapDisplayer(final Bitmap bitmap, final UnloadedPhoto photoToLoad){
            this.bitmap = bitmap;
            this.photoToLoad = photoToLoad;
        }

        @Override
        public void run() {

            if(imageViewReused(photoToLoad)) {
                return;
            }

            //-- Adds the Bitmap to the specific ImageView
            if(bitmap!=null) {
                photoToLoad.imageView.setImageBitmap(bitmap);
            } else {
                photoToLoad.imageView.setImageResource(STUB_ID);
            }
        }
    }

    /**
     * Clears the cache in case we get an OutOfMemoryException or
     * some other error. Can be triggered by the developer at any time
     */
    public void clearCache() {
        memoryCache.clear();
    }

    /**
     * Copies the input stream to the output stream, allows us to
     * write the output stream to our file cache
     * @param is <b>(InputStream)</b> - InputStream from our Http call
     * @param os <b>(OutputStream)</b> - OutputStream to save files to disk
     */
    private static void copyStream(final InputStream is, final OutputStream os) {

        final int bufferSize=1024;

        try {

            final byte[] bytes=new byte[bufferSize];

            //-- Adds each byte from our InputStream to our OutputStream
            for(;;) {

                //-- Read byte from input stream
                final int count=is.read(bytes, 0, bufferSize);
                if(count==-1) {
                    break;
                }

                //-- Write byte from output stream
                os.write(bytes, 0, count);
            }
        } catch (final IOException e) {
            CustomLog.w(TAG, "IOException: " + e.getMessage());
        }
    }
}
