package com.blizzard.mobilegarrison.datalayer.objects.character;

import java.io.Serializable;

/**
 * Object that holds all of the data for a particular WoW Character.
 *
 * @author Joshua Hale
 */
public class WoWCharacter implements Serializable {

    private static final long serialVersionUID = 6843604506022622368L;
    private int level;
    private int achievementPoints;
    private int honorableKills;
    private long lastModified;
    private String battlegroup;
    private String calcClass;
    private String name;
    private String server;
    private String thumbnailImageUrl;
    private String fullImageUrl;
    private CharacterClass characterClass;
    private CharacterGender characterGender;
    private CharacterRace characterRace;

    public WoWCharacter() {
        super();
    }

    public WoWCharacter(final String name, final String server) {
        this.name = name;
        this.server = server;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(final int level) {
        this.level = level;
    }

    public int getAchievementPoints() {
        return achievementPoints;
    }

    public void setAchievementPoints(final int achievementPoints) {
        this.achievementPoints = achievementPoints;
    }

    public int getHonorableKills() {
        return honorableKills;
    }

    public void setHonorableKills(final int honorableKills) {
        this.honorableKills = honorableKills;
    }

    public long getLastModified() {
        return lastModified;
    }

    public void setLastModified(final long lastModified) {
        this.lastModified = lastModified;
    }

    public String getBattlegroup() {
        return battlegroup;
    }

    public void setBattlegroup(final String battlegroup) {
        this.battlegroup = battlegroup;
    }

    public String getCalcClass() {
        return calcClass;
    }

    public void setCalcClass(final String calcClass) {
        this.calcClass = calcClass;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getServer() {
        return server;
    }

    public void setServer(final String server) {
        this.server = server;
    }

    public String getThumbnailImageUrl() {
        return thumbnailImageUrl;
    }

    public void setThumbnailImageUrl(final String thumbnailImageUrl) {
        this.thumbnailImageUrl = thumbnailImageUrl;
    }

    public String getFullImageUrl() {
        return fullImageUrl;
    }

    public void setFullImageUrl(final String fullImageUrl) {
        this.fullImageUrl = fullImageUrl;
    }

    public CharacterClass getCharacterClass() {
        return characterClass;
    }

    public void setCharacterClass(final CharacterClass characterClass) {
        this.characterClass = characterClass;
    }

    public CharacterGender getCharacterGender() {
        return characterGender;
    }

    public void setCharacterGender(final CharacterGender characterGender) {
        this.characterGender = characterGender;
    }

    public CharacterRace getCharacterRace() {
        return characterRace;
    }

    public void setCharacterRace(final CharacterRace characterRace) {
        this.characterRace = characterRace;
    }
}
