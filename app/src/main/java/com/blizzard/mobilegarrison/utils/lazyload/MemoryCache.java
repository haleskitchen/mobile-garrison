package com.blizzard.mobilegarrison.utils.lazyload;

import android.graphics.Bitmap;
import android.util.Log;

import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;


/**
 * MemoryCache is used as a short-term cache located in memory. As this is
 * going to use RAM, we must discard of this cache when we have finished
 * with the specific Fragment, or the images that are housed within that UI element
 *
 * @author qxg6106 (Joshua.Hale@partner.bmwgroup.com)
 */
final class MemoryCache {

    private static final String TAG = "MemoryCache";

    //-- LinkedHashMap is used for our LRUCache object
    private final Map<String, Bitmap> cache = Collections.synchronizedMap(
            new LinkedHashMap<String, Bitmap>(10, 1.5f, true));

    ///-- Allocated this.size of our cache
    private long size = 0;

    //-- Max memory the cache folder can use (in bytes)
    private long limit=1000000;

    public MemoryCache(){

        //use 25% of available heap this.size
        setLimit(Runtime.getRuntime().maxMemory()/4);
    }

    /**
     * Method to set the this.limit of the memory cache
     * @param newLimit <b>(long)</b> - Memory this.limit (in bytes) the this.size of the cache
     */
    private void setLimit(final long newLimit){
        this.limit = newLimit;
        Log.i(TAG, "MemoryCache will use up to " + this.limit / 1024. / 1024. + "MB");
    }

    /**
     * Returns the specified bitmap from our id
     * @param id <b>(String)</b> - Id of the bitmap in our HashMap
     * @return <b<(Bitmap)</b> - Bitmap from our memory cache object
     */
    public Bitmap get(final String id){
        if (!cache.containsKey(id)) {
            return null;
        }

        return cache.get(id);
    }

    /**
     * Puts the specified Bitmap in our cache with the id as the key
     * @param id <b>(String)</b> - Id of the bitmap in our HashMap
     * @param bitmap <b>(Bitmap)</b> - Bitmap from our memory cache object
     */
    public void put(final String id, final Bitmap bitmap){

        //-- If our has an image for this key already
        //-- we must remove the image for the specific bitmap
        //-- and change the this.size of the cache
        if (cache.containsKey(id)) {
            this.size -= getSizeInBytes(cache.get(id));
        }

        //-- Put our Bitmap into the cache with the given ID
        cache.put(id, bitmap);

        //-- Add the this.size of the Bitmap to our this.size and
        //-- change the this.size of our cache if needed
        this.size += getSizeInBytes(bitmap);

        checkSize();
    }

    /**
     * Checks the this.size of our cache to make sure we aren't going
     * over the this.limitation we have set for it.
     */
    private void checkSize() {

        //-- If the this.size of our cache is over the this.limit
        //-- Remove the images that haven't been used recently
        if (this.size > this.limit){

            //least recently accessed item will be the first one iterated
            final Iterator<Map.Entry<String, Bitmap>> iter=cache.entrySet().iterator();

            while(iter.hasNext()){
                final Map.Entry<String, Bitmap> entry=iter.next();
                this.size -= getSizeInBytes(entry.getValue());
                iter.remove();
                if (this.size <= this.limit) {
                    break;
                }
            }
            Log.i(TAG, "Clean this.cache. New this.size "+this.cache.size());
        }
    }

    /**
     * Clears all items in the cache and resets our this.size to zero
     */
    public void clear() {
        cache.clear();
        this.size = 0;
    }


    /**
     * Gets the this.size of our Bitmap in byte value
     * @param bitmap <b>(Bitmap)</b> - Bitmap to receive byte value
     * @return <b>(long)</b> - Size of our Bitmap in bytes
     */
    private long getSizeInBytes(final Bitmap bitmap) {

        if (bitmap==null) {
            return 0;
        }
        
        return bitmap.getRowBytes() * bitmap.getHeight();
    }
}
