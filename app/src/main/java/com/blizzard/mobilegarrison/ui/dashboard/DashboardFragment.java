package com.blizzard.mobilegarrison.ui.dashboard;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;

import com.blizzard.mobilegarrison.R;
import com.blizzard.mobilegarrison.adapters.DashboardAdapter;
import com.blizzard.mobilegarrison.datalayer.manager.user.UserManager;
import com.blizzard.mobilegarrison.datalayer.objects.character.WoWCharacter;
import com.blizzard.mobilegarrison.ui.base.BaseActivity;
import com.blizzard.mobilegarrison.ui.base.BaseFragment;
import com.blizzard.mobilegarrison.ui.garrison.GarrisonActivity;
import com.blizzard.mobilegarrison.utils.character.CharacterUtils;
import com.blizzard.mobilegarrison.widgets.general.WoWDialog;

/**
 * Fragment that allows the user to interact with the different portions of the Dashboard
 * and go to different portions of the application
 *
 * @author Joshua Hale
 */
public class DashboardFragment extends BaseFragment {

    private static final String TAG = "DashboardFragment";

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup parent,
                             final Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_dashboard, parent, false);
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle(getString(R.string.dashboard));

        init();
    }

    private void init() {

        final UserManager userManager = UserManager.getInstance(getActivity());
        final CharacterUtils charUtils = new CharacterUtils(getActivity());
        WoWCharacter wowCharacter = userManager.getCurrentWoWCharacter();

        if (getView() != null) {
            GridView dashboardGridView = (GridView) getView().findViewById(R.id.dashboardGridView);
            ImageView backgroundImageView = (ImageView) getView().findViewById(R.id.backgroundImageView);

            backgroundImageView.setBackgroundResource(charUtils.
                    getCharacterRaceBackground(wowCharacter.getCharacterRace()));

            final DashboardAdapter dashboardAdapter = new DashboardAdapter(getActivity());
            dashboardGridView.setAdapter(dashboardAdapter);

            dashboardGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(final AdapterView<?> parent, final View view,
                                        final int position, final long id) {
                    Class nextClass;

                    switch (position) {

                        case 0:
                            nextClass = GarrisonActivity.class;
                            break;
//                    case 1:
//                        nextClass = AuctionHouseActivity.class;
//                        break;
//                    case 2:
//                        nextClass = ArmoryActivity.class;
//                        break;
//                    case 3:
//                        nextClass = GuildChatActivity.class;
//                        break;
//                    case 4:
//                        nextClass = GuildInfoActivity.class;
//                        break;
//                    case 5:
//                        nextClass = TalentCalculatorActivity.class;
//                        break;
//                    case 6:
//                        nextClass = RealmStatusActivity.class;
//                        break;
//                    case 7:
//                        nextClass = ForumsActivity.class;
//                        break;
                        default:
                            nextClass = null;
                            break;
                    }

                    startNextActivity(nextClass);
                }
            });
        }
    }

    private void startNextActivity(Class nextClass) {

        if (nextClass == null) {
            WoWDialog wowDialog = new WoWDialog(getActivity(), "This content is not yet available");
            wowDialog.show();
        } else if (getActivity().getClass() == nextClass) {
            if (getActivity() instanceof BaseActivity) {
                ((BaseActivity) getActivity()).getDrawerLayout().closeDrawers();
            }
        } else {
            startActivity(new Intent(getActivity(), nextClass));
        }
    }

    @Override
    public void onCharacterChanged(final WoWCharacter wowCharacter) {
        init();
        super.onCharacterChanged(wowCharacter);
    }
}
