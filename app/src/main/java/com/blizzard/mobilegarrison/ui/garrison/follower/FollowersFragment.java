package com.blizzard.mobilegarrison.ui.garrison.follower;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.blizzard.mobilegarrison.R;
import com.blizzard.mobilegarrison.adapters.FollowersAdapter;
import com.blizzard.mobilegarrison.datalayer.manager.bus.EventBus;
import com.blizzard.mobilegarrison.datalayer.objects.character.WoWCharacter;
import com.blizzard.mobilegarrison.datalayer.objects.garrison.follower.Follower;
import com.blizzard.mobilegarrison.datalayer.objects.garrison.follower.FollowerAbility;
import com.blizzard.mobilegarrison.datalayer.objects.garrison.follower.FollowerCounter;
import com.blizzard.mobilegarrison.datalayer.objects.garrison.follower.FollowerTrait;
import com.blizzard.mobilegarrison.datalayer.tasks.async.GetAllFollowersTask;
import com.blizzard.mobilegarrison.datalayer.tasks.results.GetAllFollowersEvent;
import com.blizzard.mobilegarrison.datalayer.tasks.results.GetErrorEvent;
import com.blizzard.mobilegarrison.ui.base.BaseFragment;
import com.blizzard.mobilegarrison.utils.error.ErrorUtils;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Fragment that shows all of the different mFollowers in a list.
 *
 * @author Joshua Hale
 * @version 1.0
 * @since 1.0
 */
public class FollowersFragment extends BaseFragment {

    private List<Follower> mFollowers;
    private LinearLayout mFiltersLinearLayout;
    private ListView mFollowersListView;
    private TextView mFilteredByTextView;
    private View mView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent,
                             Bundle savedInstanceState) {

        //-- Inflates the view for the mFollowers fragment
        mView = inflater.inflate(R.layout.fragment_followers, parent, false);

        new GetAllFollowersTask(getActivity()).execute();

        return mView;
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getInstance().register(this);
    }

    /**
     * Updates the UI after the AsyncTask has completed
     */
    private void updateUI() {

        //-- If we have mFollowers then create a new adapter and add our filters
        if (mFollowers != null) {
            FollowersAdapter adapter = new FollowersAdapter(getActivity(), mFollowers);

            mFilteredByTextView = (TextView) mView.findViewById(R.id.filteredByTextView);
            mFiltersLinearLayout = (LinearLayout) mView.findViewById(R.id.filtersLinearLayout);
            mFollowersListView = (ListView) mView.findViewById(R.id.followersListView);

            mFollowersListView.setAdapter(adapter);

            addCounters();
            addFilters();

            mFollowersListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view,
                                        int position, long id) {
                    Follower follower = mFollowers.get(position);
                    Bundle extras = new Bundle();
                    extras.putSerializable(getString(R.string.argument_follower), follower);
                    Intent intent = new Intent(getActivity(), FollowerDetailActivity.class);
                    intent.putExtras(extras);
                    startActivity(intent);
                }
            });
        }
    }

    /**
     * Adds the counters to the HorizontalScrollView widget.
     */
    private void addCounters() {

        HashMap<FollowerCounter, Integer> followerCounterCount = new HashMap<>();

        for (Follower follower : mFollowers) {
            for (FollowerAbility ability : follower.getFollowerAbilities()) {

                FollowerCounter counter = ability.getFollowerCounter();

                Integer count = followerCounterCount.get(counter);

                if (count == null) {
                    count = 0;
                }
                count++;
                followerCounterCount.put(counter, count);
            }
        }

        //-- For each follower ability from our enum value
        //-- we add the image and the text to show that we have the ability
        for (final FollowerCounter counter : FollowerCounter.values()) {

            Bitmap bitmap = BitmapFactory.decodeResource(getResources(),
                    counter.getIconResId());

            Integer c = followerCounterCount.get(counter);

            if (c == null) {
                c = 0;
            }
            String text = String.valueOf(c);

            //-- Retrieve the filter view that contains both the icon view
            //-- and the textview for the count of filters or abilities
            View filterView = getActivity().getLayoutInflater().
                    inflate(R.layout.item_filter, null);

            ImageView icon = (ImageView) filterView.findViewById(R.id.icon_filter);
            TextView count = (TextView) filterView.findViewById(R.id.number_of_followers);

            //-- If our bitmap isn't null, set the image bitmap and the text
            if (bitmap != null) {
                icon.setImageBitmap(bitmap);
                count.setText(text);
            }

            //-- OnClickListener for when the follower trait gets clicked.
            //-- The mFollowers should be filtered down to the ones that only have
            //-- that specific trait.
            filterView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    filterFollowers(counter);
                }
            });

            //-- Add the view to the HorizontalScrollView
            mFiltersLinearLayout.addView(filterView);
        }
    }

    /**
     * Add the filters to the top of the ListView in the HorizontalScrollView. These
     * filters when clicked will only show the mFollowers that have those specific traits and/or
     * abilities.
     */
    private void addFilters() {

        Map<FollowerTrait, Integer> followerTraitCount = new HashMap<>();

        for (Follower follower : mFollowers) {

            for (FollowerTrait trait : follower.getFollowerTraits()) {

                Integer count = followerTraitCount.get(trait);

                if (count == null) {
                    count = 0;
                }

                count++;
                followerTraitCount.put(trait, count);
            }
        }

        //-- For each follower trait from our enum value, we add
        //-- the image and the text "1" to it to show that we have the traits.
        for (final FollowerTrait trait : FollowerTrait.values()) {

            Bitmap bitmap = BitmapFactory.decodeResource(getResources(), trait.getIconResId());

            Integer c = followerTraitCount.get(trait);

            if (c == null) {
                c = 0;
            }
            String text = String.valueOf(c);

            //-- Retrieve the filter view that contains both the icon view
            //-- and the textview for the count of filters or abilities
            View filterView = getActivity().getLayoutInflater().
                    inflate(R.layout.item_filter, null);

            ImageView icon = (ImageView) filterView.findViewById(R.id.icon_filter);
            TextView count = (TextView) filterView.findViewById(R.id.number_of_followers);

            //-- If our bitmap isn't null, set the image bitmap and the text
            if (bitmap != null) {
                icon.setImageBitmap(bitmap);
                count.setText(text);
            }

            //-- OnClickListener for when the follower trait gets clicked.
            //-- The mFollowers should be filtered down to the ones that only have
            //-- that specific trait.
            filterView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    filterFollowers(trait);
                }
            });

            //-- Add the view to the HorizontalScrollView
            mFiltersLinearLayout.addView(filterView);
        }
    }

    private void filterFollowers(FollowerCounter followerCounter) {

        List<Follower> filteredFollowers = new ArrayList<>();

        for (Follower follower : mFollowers) {

            List<FollowerAbility> abilities = follower.getFollowerAbilities();

            if (abilities != null) {
                for (FollowerAbility ability : abilities) {
                    if (ability.getFollowerCounter().getIconResId() == followerCounter.getIconResId()) {
                        filteredFollowers.add(follower);
                        break;
                    }
                }
            }
        }

        //-- Set a new adapter to the ListView once the
        //-- loop has been completed.
        if (filteredFollowers.size() > 0) {
            FollowersAdapter adapter = new FollowersAdapter(getActivity(), filteredFollowers);
            mFollowersListView.setAdapter(adapter);
        }

        setFilteredByText(followerCounter.getIconResId());
    }

    /**
     * Filters the mFollowers based on the specific trait that gets passed in
     * as the parameter.
     * @param followerTrait <b>(FollowerTrait)</b> - FollowerTrait to filter by.
     */
    private void filterFollowers(FollowerTrait followerTrait) {

        //-- Create a new list of filtered mFollowers
        List<Follower> filteredFollowers = new ArrayList<>();

        //-- Circle through each follower to see if the follower
        //-- has that specific trait.
        for (Follower follower : mFollowers) {

            //-- Return a list of FollowerTraits
            List<FollowerTrait> followerTraits = follower.getFollowerTraits();

            //-- If the follower has traits loop through to find that trait
            //-- if it exists.
            if (followerTraits != null) {
                for (FollowerTrait trait : followerTraits) {
                    if (trait.getFollowerTrait() == followerTrait.getFollowerTrait()) {
                        filteredFollowers.add(follower);
                        break;
                    }
                }

            }
        }

        //-- Set a new adapter to the ListView once the
        //-- loop has been completed.
        if (filteredFollowers.size() > 0) {
            FollowersAdapter adapter = new FollowersAdapter(getActivity(), filteredFollowers);
            mFollowersListView.setAdapter(adapter);
        }

        setFilteredByText(followerTrait.getIconResId());
    }

    private void setFilteredByText(int iconResId) {

        Drawable iconRight = getResources().getDrawable(iconResId);
        mFilteredByTextView.setVisibility(View.VISIBLE);
        mFilteredByTextView.setText("Filtered by: ");
        mFilteredByTextView.setCompoundDrawables(null, null, iconRight, null);
        mFilteredByTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mFilteredByTextView.setVisibility(View.GONE);

                FollowersAdapter adapter = new FollowersAdapter(getActivity(), mFollowers);
                mFollowersListView.setAdapter(adapter);
            }
        });
    }

    @Subscribe
    public void getFollowers(GetAllFollowersEvent event) {
        mFollowers = event.getResult();

        updateUI();
    }

    @Subscribe
    public void onError(GetErrorEvent event) {
        String message = event.getMessage();
        boolean showDialog = event.showDialog();

        ErrorUtils.showMessage(getActivity(), message, showDialog);
    }

    @Override
    public void onStop() {
        EventBus.getInstance().unregister(this);
        super.onStop();
    }

    @Override
    public void onCharacterChanged(final WoWCharacter wowCharacter) {
        super.onCharacterChanged(wowCharacter);

        new GetAllFollowersTask(getActivity()).execute();
    }
}
