package com.blizzard.mobilegarrison.widgets.general;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.blizzard.mobilegarrison.R;
import com.blizzard.mobilegarrison.constants.Font;
import com.blizzard.mobilegarrison.utils.font.FontUtils;

/**
 * Custom dialog made to look like the dialog found within World of Warcraft
 * when the user attempts to authenticate or retrieve server level information.
 *
 * @author Joshua Hale (josh@joshuahale.com)
 * @version 1.0
 * @since 1.0
 */
public class WoWDialog extends Dialog implements View.OnClickListener {

    private final String message;

    /**
     * Constructor that takes an Activity context and string to display to the user
     * @param context <b>(Context)</b> - Used to grab resources from our application
     * @param message <b>(String)</b> - Message to be used within the application
     */
    public WoWDialog(final Context context, final String message) {
        super(context);

        this.message = message;
    }

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //-- Turns off the title for our application and retrieves the content view
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.loading_dialog);

        //-- Makes the background of the window to transparent allowing our border to
        //-- look as the background of the dialog
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        //-- Retrieves the views within our dialogs content view
        final Button cancel = (Button) findViewById(R.id.button_cancel);
        final TextView dialogMessage = (TextView) findViewById(R.id.dialog_message);

        //-- Set the message passed into the constructor to the dialogMessage TextView
        dialogMessage.setText(this.message);

        //-- Click listener dismisses the dialog
        cancel.setOnClickListener(this);

        //-- Grabs the typefaces used for the dialog
        final FontUtils fontUtils = new FontUtils(getContext());

        //-- Sets the typefaces to our different TextViews
        fontUtils.setTypeface(Font.FRIZQT, dialogMessage);
        fontUtils.setTypeface(Font.FRIZQT, cancel);
    }

    @Override
    public void onClick(final View v) {
        switch(v.getId()) {
            case R.id.button_cancel:
                dismiss();
                break;
        }
    }
}
