package com.blizzard.mobilegarrison.datalayer.tasks.results;

import com.blizzard.mobilegarrison.datalayer.objects.garrison.follower.Follower;

import java.util.List;

/**
 * Otto Event that is fired when {@link com.blizzard.mobilegarrison.datalayer.tasks.async.GetAllFollowersTask}
 * returns a list of Followers
 *
 * @author Joshua Hale
 */
public class GetAllFollowersEvent {

    private final List<Follower> followers;

    public GetAllFollowersEvent(List<Follower> followers) {
        this.followers = followers;
    }

    public List<Follower> getResult() {
        return followers;
    }
}
