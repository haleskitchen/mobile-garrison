/**
 * Package that handles the UI for Logging into the application
 * when the user first starts up the application or logs out.
 *
 * @since 1.0
 * @version 1.0
 */
package com.blizzard.mobilegarrison.ui.login.fragments;