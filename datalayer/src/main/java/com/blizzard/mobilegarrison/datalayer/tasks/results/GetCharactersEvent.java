package com.blizzard.mobilegarrison.datalayer.tasks.results;

import com.blizzard.mobilegarrison.datalayer.objects.character.WoWCharacter;

import java.util.List;

/**
 * Otto event that is fired when {@link com.blizzard.mobilegarrison.datalayer.tasks.async.GetCharacterListTask}
 * returns a list of WoWCharacters
 *
 * @author Joshua Hale
 */
public class GetCharactersEvent {

    private final List<WoWCharacter> characters;

    public GetCharactersEvent(List<WoWCharacter> characters) {
        this.characters = characters;
    }

    public List<WoWCharacter> getResult() {
        return characters;
    }
}
