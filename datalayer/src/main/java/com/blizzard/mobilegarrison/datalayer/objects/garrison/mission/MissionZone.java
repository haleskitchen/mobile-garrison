package com.blizzard.mobilegarrison.datalayer.objects.garrison.mission;

/**
 * Enum that determines which zone a Mission is in
 *
 * @author Joshua Hale
 */
public enum MissionZone {

    BLACKROCK_MOUNTAIN,
    FROSTFIRE_RIDGE,
    GORGROND,
    NAGRAND,
    SHADOWMOON_VALLEY,
    SPIRES_OF_ARAK,
    TALADOR
}
