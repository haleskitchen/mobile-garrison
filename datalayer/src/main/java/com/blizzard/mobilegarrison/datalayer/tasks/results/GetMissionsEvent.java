package com.blizzard.mobilegarrison.datalayer.tasks.results;

import com.blizzard.mobilegarrison.datalayer.objects.garrison.mission.Mission;

import java.util.List;

/**
 * Otto Event that is fired when our GetCurrentMissionsTask returns a list of Missions
 *
 * @author Joshua Hale
 */
class GetMissionsEvent {

    private final List<Mission> missions;

    public GetMissionsEvent(List<Mission> missions) {
        this.missions = missions;
    }

    public List<Mission> getResult() {
        return this.missions;
    }
}
