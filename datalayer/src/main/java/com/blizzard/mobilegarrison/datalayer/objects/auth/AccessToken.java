package com.blizzard.mobilegarrison.datalayer.objects.auth;

import java.io.Serializable;

/**
 * Model object that contains all the data for the AccessToken
 * @author Joshua Hale (josh@joshuahale.com)
 */
public class AccessToken implements Serializable {

    private static final long serialVersionUID = 1254520281993202035L;
    private String accessToken;
    private String tokenType;
    private long expiresIn;
    private String[] scope;
    private String accountId;

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getTokenType() {
        return tokenType;
    }

    public void setTokenType(String tokenType) {
        this.tokenType = tokenType;
    }

    public long getExpiresIn() {
        return expiresIn;
    }

    public void setExpiresIn(long expiresIn) {
        this.expiresIn = expiresIn;
    }

    public String[] getScope() {
        return scope;
    }

    public void setScope(String[] scope) {
        this.scope = scope;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }
}
