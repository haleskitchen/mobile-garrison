package com.blizzard.mobilegarrison.utils.view;

import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;

/**
 * ViewUtils is a Utility class that helps with all different kinds of View related
 * methods in Android. Instead of consistently researching methods that are widely
 * used by in development practices, we've encapsulated most of those methods here
 * in order to be re-used by developers.
 *
 * @author Joshua Hale (josh@joshuahale.com)
 */
public class ViewUtils {

    private static final String TAG = "ViewUtils";

    private ViewUtils() {
        super();
    }

    /**
     * This method sets the height of the ListView dynamically based on the height of the
     * children that are within the ListView.<br />
     * This method is exceptionally helpful when determining if a list view is located
     * within another scrolling widget such as a ScrollView
     * @param listView <b>(ListView)</b> - ListView provided by the developer
     */
    public static void setListViewHeightBasedOnChildren(ListView listView) {

        ListAdapter listAdapter = listView.getAdapter();

        if(listAdapter == null) {
            Log.v(TAG, "No list adapter found attached to listview");
            return;
        }

        //-- Desired width by the developer
        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(),
                View.MeasureSpec.UNSPECIFIED);

        //-- Running total height of the ListView
        int totalHeight = 0;

        View view = null;

        //-- Iterates through each child view in the adapter
        //-- And adds the height of that specific child to
        //-- the total overall height of the ListView
        for(int i = 0; i < listAdapter.getCount(); i++) {

            view = listAdapter.getView(i, view, listView);

            if(i == 0) {
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth,
                        ViewGroup.LayoutParams.WRAP_CONTENT));
            }

            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);

            totalHeight += view.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();

        //-- Adds the height of the dividers to the total height
        params.height = totalHeight + (listView.getDividerHeight() *
                listAdapter.getCount() - 1);

        listView.setLayoutParams(params);
        listView.requestLayout();
    }
}
