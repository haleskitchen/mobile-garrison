package com.blizzard.mobilegarrison.datalayer.objects.garrison.follower;


import com.blizzard.mobilegarrison.datalayer.R;

/**
 * Enum that determines the counter. <br />
 * Id is the Id of the counter (irrelevant in most places) <br />
 * Resource ID determines which drawable to show for the counter
 *
 * @author Joshua Hale
 */
public enum FollowerCounter {

    DANGER_ZONES(1, R.drawable.counter_danger_zones),
    DEADLY_MINIONS(2, R.drawable.counter_deadly_minions),
    GROUP_DAMAGE(3, R.drawable.counter_group_damage),
    MAGIC_DEBUFF(4, R.drawable.counter_magic_debuff),
    MASSIVE_STRIKE(5, R.drawable.counter_massive_strike),
    MINION_SWARMS(6, R.drawable.counter_minion_swarms),
    POWERFUL_SPELL(7, R.drawable.counter_powerful_spell),
    TIMED_BATTLE(8, R.drawable.counter_timed_battle),
    WILD_AGGRESSION(9, R.drawable.counter_wild_aggression);

    private final int counterId;
    private final int resId;

    private FollowerCounter(int counterId, int resId) {
        this.counterId = counterId;
        this.resId = resId;
    }

    int getCounterId() {
        return counterId;
    }

    public int getIconResId() {
        return resId;
    }

    public static FollowerCounter getFollowerCounter(int id) {

        for (FollowerCounter counter : values()) {
            if (id == counter.getCounterId()) {
                return counter;
            }
        }
        return null;
    }
}
