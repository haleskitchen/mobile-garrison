package com.blizzard.mobilegarrison.ui.settings;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.blizzard.mobilegarrison.R;
import com.blizzard.mobilegarrison.datalayer.manager.user.UserManager;
import com.blizzard.mobilegarrison.datalayer.objects.character.WoWCharacter;
import com.blizzard.mobilegarrison.ui.login.activities.LoginActivity;
import com.blizzard.mobilegarrison.utils.character.CharacterUtils;

/**
 * PreferenceActivity that showcases the different Settings pertaining to Mobile Garrison.
 *
 * @author Joshua Hale
 */
public class SettingsActivity extends PreferenceActivity {

    private Toolbar mActionBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addPreferencesFromResource(R.xml.settings_general);

        mActionBar.setTitle(getTitle());
    }

    @Override
    public void setContentView(int layoutResID) {
        ViewGroup contentView = (ViewGroup) LayoutInflater.from(this).inflate(
                R.layout.activity_settings, new LinearLayout(this), false);

        mActionBar = (Toolbar) contentView.findViewById(R.id.action_bar);
        mActionBar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        CharacterUtils characterUtils = new CharacterUtils(this);
        UserManager userManager = UserManager.getInstance(this);

        int backgroundColor;

        WoWCharacter wowCharacter = userManager.getCurrentWoWCharacter();

        if (characterUtils.isCharacterAlliance(wowCharacter)) {
            backgroundColor = getResources().getColor(R.color.alliance_blue);
        } else {
            backgroundColor = getResources().getColor(R.color.horde_red);
        }

        mActionBar.setBackgroundColor(backgroundColor);
        mActionBar.setTitleTextColor(Color.WHITE);

        ViewGroup contentWrapper = (ViewGroup) contentView.findViewById(R.id.content_wrapper);
        LayoutInflater.from(this).inflate(layoutResID, contentWrapper, true);

        getWindow().setContentView(contentView);
    }

    @Override
    public void onResume() {
        super.onResume();

        Preference logout = findPreference("prefLogout");
        logout.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                AlertDialog.Builder builder = new AlertDialog.Builder(SettingsActivity.this);
                builder.setTitle(getString(R.string.dialog_title_logout));
                builder.setMessage(getString(R.string.dialog_message_logout));
                builder.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        logout();
                        dialog.dismiss();
                    }
                });

                builder.setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                builder.create().show();
                return false;
            }
        });
    }

    private void logout() {
        UserManager userManager = UserManager.getInstance(this);
        userManager.clearUserData();

        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }
}
