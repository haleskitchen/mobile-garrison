package com.blizzard.mobilegarrison.datalayer.utils.time;

import android.graphics.Color;
import android.widget.TextView;

import java.util.Date;

/**
 * TimeUtils helps us define the time a mission will start, when a mission will end and
 * what the current time is. The TimeUtils correctly calculates when a mission will end
 * based on the TimeZone of where the device currently is located. This class is also
 * charged with the task of determining the time remaining left in a mission
 * and setting up the UI for a TextView appropriately.
 *
 * @author Joshua Hale (josh@joshuahale.com)
 */
public class TimeUtils {

    private TimeUtils() {
        super();
    }

    /**
     * Returns the time (in seconds), when the mission will end
     * @param hours <b>(int)</b> - Number of hours til the mission ends
     * @param minutes <b>(int)</b> - Number of minutes til the mission ends
     * @param now <b>(long)</b> - The stored time for the mission, in seconds
     * @return <b>(long)</b> - The time the mission will complete, in seconds.
     */
    public static long getMissionCompletedTimestamp(int hours, int minutes, long now) {

        int hoursInSeconds = hours * 60 * 60;
        int minutesInSeconds = minutes * 60;

        return now + hoursInSeconds + minutesInSeconds;
    }

    /**
     * Returns the current time
     * @return <b>(long)</b> - The current time (in seconds)
     */
    public static long getTimeNow() {
        return new Date().getTime() / 1000;
    }

    /**
     * Checks to see if the mission is finished by checking to see if the result for the difference
     * between now and the end time is negative
     * @param endTime <b>(int)</b> - Unix time, in seconds, when the mission will end.
     * @return <b>(boolean)</b> - Returns true if the mission has completed, false if not.
     */
    public static boolean isMissionCompleted(int endTime) {

        long now = new Date().getTime();
        endTime *= 1000;

        long diff = endTime - now;

        return diff < 0;
    }

    /**
     * Shows the time remaining for a specific mission
     * @param textView <b>(TextView)</b> - TextView where the time will be shown
     * @param endTime <b>(int)</b> - Unix time, in seconds, of when the mission will end.
     */
    public static void showTimeRemaining(TextView textView, int endTime) {

        long now = new Date().getTime();
        endTime *= 1000;

        long diff = endTime - now;

        if (diff > 0) {
            long diffMinutes = diff / (60 * 1000) % 60;
            long diffHours = diff / (60 * 60 * 1000) % 24;

            textView.setText(diffMinutes + ": " + diffHours);
        } else {
            textView.setText("Completed");
            textView.setTextColor(Color.RED);
        }
    }
}
