package com.blizzard.mobilegarrison.datalayer.tasks.async;

import android.content.Context;
import android.os.AsyncTask;

import com.blizzard.mobilegarrison.datalayer.constants.ErrorStrings;
import com.blizzard.mobilegarrison.datalayer.manager.bus.EventBus;
import com.blizzard.mobilegarrison.datalayer.manager.data.DataManager;
import com.blizzard.mobilegarrison.datalayer.objects.garrison.mission.Mission;
import com.blizzard.mobilegarrison.datalayer.objects.garrison.mission.MissionEnemy;
import com.blizzard.mobilegarrison.datalayer.tasks.results.GetErrorEvent;
import com.blizzard.mobilegarrison.datalayer.tasks.results.GetMissionEnemyDetailsEvent;

import java.util.ArrayList;
import java.util.List;

/**
 * Gets the Mission Enemy Details for a specific Mission.
 * @author Joshua Hale (josh@joshuahale.com)
 */
public class GetMissionEnemyDetailsTask extends AsyncTask<Void, Void, List<MissionEnemy>> {

    private final Context mContext;
    private final Mission mMission;

    /**
     * Constructor that requires an activity context (for DataManager), and a Mission.
     * @param context <b>(Context)</b> - Activity context
     * @param mission <b>(Mission)</b> - Current mission to retrieve enemy information for.
     */
    public GetMissionEnemyDetailsTask(Context context, Mission mission) {
        mContext = context;
        mMission = mission;
    }

    @Override
    protected List<MissionEnemy> doInBackground(Void... params) {
        DataManager dataManager = DataManager.getInstance(mContext);

        //-- Create a new list of enemy ids
        List<Integer> missionEnemyIds = new ArrayList<>();

        //-- Since our missions can only have a maximum of three
        //-- enemies, we can check each one independently to see if one exists.

        if (mMission.getMissionEncounter1() != 0) {
            missionEnemyIds.add(mMission.getMissionEncounter1());
        }
        if (mMission.getMissionEncounter2() != 0) {
            missionEnemyIds.add(mMission.getMissionEncounter2());
        }
        if (mMission.getMissionEncounter3() != 0) {
            missionEnemyIds.add(mMission.getMissionEncounter3());
        }

        return dataManager.getMissionEnemies(missionEnemyIds);
    }

    @Override
    public void onPostExecute(List<MissionEnemy> enemies) {
        if (enemies != null) {
            EventBus.getInstance().post(new GetMissionEnemyDetailsEvent(enemies));
        } else {
            EventBus.getInstance().post(new GetErrorEvent(
                    ErrorStrings.ERROR_NO_ENEMY_DETAILS, true));
        }
    }
}
