package com.blizzard.mobilegarrison.datalayer.tasks.async;

import android.content.Context;
import android.net.http.AndroidHttpClient;
import android.os.AsyncTask;
import android.util.Base64;

import com.blizzard.mobilegarrison.datalayer.constants.ErrorStrings;
import com.blizzard.mobilegarrison.datalayer.constants.MobileGarrisonConstants;
import com.blizzard.mobilegarrison.datalayer.manager.bus.EventBus;
import com.blizzard.mobilegarrison.datalayer.manager.data.DataManager;
import com.blizzard.mobilegarrison.datalayer.manager.user.UserManager;
import com.blizzard.mobilegarrison.datalayer.objects.auth.AccessToken;
import com.blizzard.mobilegarrison.datalayer.tasks.results.GetErrorEvent;
import com.blizzard.mobilegarrison.datalayer.utils.json.JSONUtils;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Returns an Access Token that we receive from the server once our login was successful.
 * This allows for the retrieval of character data in order to get further information
 * for a user's account.
 *
 * @author Joshua Hale (josh@joshuahale.com)
 */
public class GetAccessTokenTask extends AsyncTask<String, Void, String> {

    private static final String HEADER_AUTHORIZATION = "Authorization";
    private static final String HEADER_BASIC = "Basic ";

    private static final String PARAM_REDIRECT_URI = "redirect_uri";
    private static final String PARAM_SCOPE = "scope";
    private static final String PARAM_GRANT_TYPE = "grant_type";
    private static final String PARAM_CODE = "code";

    private static final String VALUE_GRANT_TYPE = "authorization_code";
    private static final String VALUE_SCOPE = "wow.profile";

    private final Context context;

    /**
     * Constructor with Activity Context
     * @param context <b>(Context)</b> - Used to get singleton instances of classes
     */
    public GetAccessTokenTask(final Context context) {
        this.context = context;
    }

    @Override
    protected String doInBackground(final String... params) {
        final String authCode = params[0];

        final AndroidHttpClient client = AndroidHttpClient.newInstance("Android");

        try {

            //-- Create a new HttpPost with the required parameters needed
            //-- from the Battle.net API Documentation
            final HttpPost httpPost = new HttpPost(MobileGarrisonConstants.BATTLE_NET_TOKEN_URI);
            final List<NameValuePair> postParams = new ArrayList<>();
            postParams.add(new BasicNameValuePair(PARAM_REDIRECT_URI,
                    MobileGarrisonConstants.BATTLE_NET_CALLBACK_URI));
            postParams.add(new BasicNameValuePair(PARAM_SCOPE, VALUE_SCOPE));
            postParams.add(new BasicNameValuePair(PARAM_GRANT_TYPE, VALUE_GRANT_TYPE));
            postParams.add(new BasicNameValuePair(PARAM_CODE, authCode));
            httpPost.setEntity(new UrlEncodedFormEntity(postParams));

            final String credentials = MobileGarrisonConstants.BATTLE_NET_OAUTH_KEY
                    + ":" + MobileGarrisonConstants.BATTLE_NET_OAUTH_SECRET;

            final String base64EncodedCredentials = Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);

            //-- Adds the Basic Authentication header to retrieve an access token
            httpPost.addHeader(HEADER_AUTHORIZATION, HEADER_BASIC + base64EncodedCredentials);


            final HttpResponse response = client.execute(httpPost);

            final InputStream inputStream = response.getEntity().getContent();

            final String rawString = DataManager.getStringFromInputStream(inputStream);

            //-- Parse the token from the response and store it to SharedPreferences
            //-- using our UserManager instance.
            final JSONUtils jsonUtils = new JSONUtils();
            final AccessToken accessToken = jsonUtils.getAccessToken(rawString);
            final UserManager userManager = UserManager.getInstance(context);
            userManager.setAccessToken(accessToken);

            if (accessToken == null) {
                EventBus.getInstance().post(new GetErrorEvent(
                        ErrorStrings.ERROR_ACCESS_TOKEN, false));
                return null;
            }

            return accessToken.getAccessToken();

        } catch (final IOException e) {
            e.printStackTrace();
        } finally {
            client.close();
        }

        return null;
    }
}