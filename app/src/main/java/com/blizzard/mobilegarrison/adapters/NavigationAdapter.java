package com.blizzard.mobilegarrison.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blizzard.mobilegarrison.R;
import com.blizzard.mobilegarrison.datalayer.constants.MobileGarrisonConstants;
import com.blizzard.mobilegarrison.datalayer.objects.character.WoWCharacter;
import com.blizzard.mobilegarrison.utils.character.CharacterUtils;
import com.blizzard.mobilegarrison.utils.lazyload.ImageLoader;

import java.util.List;
import java.util.Locale;

/**
 * Adapter used for the list of WoW Characters returned from our DataManager
 */
public class NavigationAdapter extends BaseAdapter {

    private final boolean showBorder;
    private final Context context;
    private final ImageLoader imageLoader;
    private final List<WoWCharacter> wowCharacters;

    public NavigationAdapter(final Context context, final List<WoWCharacter> wowCharacters,
                             final boolean showBorder) {
        super();
        this.wowCharacters = wowCharacters;
        this.context = context;
        this.showBorder = showBorder;
        this.imageLoader = new ImageLoader();
    }

    @Override
    public int getCount() {
        return wowCharacters.size();
    }

    @Override
    public Object getItem(final int position) {
        return wowCharacters.get(position);
    }

    @Override
    public long getItemId(final int position) {
        return 0;
    }

    @SuppressWarnings("deprecated")
    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        
        ViewHolder viewHolder;

        //-- If our convertView is null we inflate our view and a new ViewHolder
        //-- then set the fields accordingly and store it via setTag
        if (convertView == null) {

            final LayoutInflater inflater = (LayoutInflater) this.context.getSystemService(
                    Context.LAYOUT_INFLATER_SERVICE);

            if (this.showBorder) {
                convertView = inflater.inflate(
                        R.layout.item_character_select, parent, false);
            } else {
                convertView = inflater.inflate(R.layout.item_navigation_spinner, parent, false);
            }

            viewHolder = new ViewHolder();
            viewHolder.name = (TextView) convertView.findViewById(R.id.character_name);
            viewHolder.metadata = (TextView) convertView.findViewById(R.id.character_metadata);
            viewHolder.realm = (TextView) convertView.findViewById(R.id.character_realm);
            viewHolder.thumbnail = (ImageView) convertView.findViewById(R.id.thumbnail);
            viewHolder.faction = (ImageView) convertView.findViewById(R.id.image_faction);
            viewHolder.parent = (LinearLayout) convertView.findViewById(R.id.parent);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        //-- Get the WoWCharacter at the specified position
        final WoWCharacter wowCharacter = this.wowCharacters.get(position);

        //-- Create an instance of our CharacterUtils class
        final CharacterUtils characterUtils = new CharacterUtils(this.context);

        //-- If the object returned isn't null, set the different parameters
        if (wowCharacter != null) {
            viewHolder.name.setText(wowCharacter.getName());
            viewHolder.metadata.setText(getMetadata(wowCharacter));
            viewHolder.realm.setText(wowCharacter.getServer());
            //-- Set the text color of our metadata based on the value returned from our
            //-- WoWCharacter's class type
            viewHolder.metadata.setTextColor(this.context.getResources().getColor(characterUtils.
                    getCharacterColor(wowCharacter.getCharacterClass())));

            //-- Retrieves the thumbnail URL from our WoWCharacter and our constants file
            final String thumbnailUrl = MobileGarrisonConstants.BATTLE_NET_MEDIA_URL_US
                    + wowCharacter.getThumbnailImageUrl();

            //-- Lazy loads the image into the specified ImageView with the URL
            imageLoader.displayImage(thumbnailUrl, viewHolder.thumbnail);
        }

        Drawable background;

        //-- Changes the drawable located within the ListView cell depending on the
        //-- current character's race.
        if (characterUtils.isCharacterAlliance(wowCharacter)) {
            if (this.showBorder) {
                background = this.context.getResources().
                        getDrawable(R.drawable.character_selection_alliance);
            } else {
                background = this.context.getResources().getDrawable(R.drawable.bg_faction_alliance);
            }
            viewHolder.faction.setImageResource(R.drawable.ic_alliance);
        } else {
            if (this.showBorder) {
                background = this.context.getResources().
                        getDrawable(R.drawable.character_selection_horde);
            } else {
                background = this.context.getResources().getDrawable(R.drawable.bg_faction_horde);
            }
            viewHolder.faction.setImageResource(R.drawable.ic_horde);
        }

        //-- Changes the way we set the background based on the API level of the current phone
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
            viewHolder.parent.setBackgroundDrawable(background);
        } else {
            viewHolder.parent.setBackground(background);
        }

        return convertView;
    }

    /**
     * Creates the metadata String from different parameters within the WoWCharacter object
     * @param wowCharacter <b>(WoWCharacter)</b> - Object that houses the data for a character
     * @return <b>(String)</b> - Metadata string crafted from WoWCharacter data.
     */
    private String getMetadata(final WoWCharacter wowCharacter) {

        //-- Retrieve an instance of our CharacterUtils
        final CharacterUtils characterUtils = new CharacterUtils(this.context);

        //-- Adds our LEVEL String
        String metadata = this.context.getString(R.string.level).toUpperCase(Locale.getDefault());

        //-- Appends the level from our WoWCharacter object to the end
        metadata += " " + wowCharacter.getLevel();

        //-- Appends the String value of our Character's Race to the end of our String
        metadata += " " + characterUtils.getCharacterRaceName(wowCharacter.getCharacterRace())
                .toUpperCase(Locale.getDefault());

        //-- Appends the String value of our Character's Class to the end of our String
        metadata += " " + characterUtils.getCharacterClassName(wowCharacter.getCharacterClass())
                .toUpperCase(Locale.getDefault());

        return metadata;
    }

    /**
     * ViewHolder pattern that allows for static references of our views to be
     * held in memory for our ListView
     */
    private static class ViewHolder {
        TextView name;
        TextView realm;
        TextView metadata;
        ImageView thumbnail;
        ImageView faction;
        LinearLayout parent;
    }
}
