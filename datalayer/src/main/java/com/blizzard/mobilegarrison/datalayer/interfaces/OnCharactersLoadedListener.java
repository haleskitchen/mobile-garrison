package com.blizzard.mobilegarrison.datalayer.interfaces;

import com.blizzard.mobilegarrison.datalayer.objects.character.WoWCharacter;

import java.util.List;

/**
 * Listener that is fired when the list of Characters is loaded in our Toolbar
 *
 * @author Joshua Hale
 */
public interface OnCharactersLoadedListener {

    public void onCharactersLoadedListener(List<WoWCharacter> characters);
}
