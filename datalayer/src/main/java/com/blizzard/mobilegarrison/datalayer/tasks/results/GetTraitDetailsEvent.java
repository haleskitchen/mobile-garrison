package com.blizzard.mobilegarrison.datalayer.tasks.results;

import com.blizzard.mobilegarrison.datalayer.objects.garrison.follower.TraitDetails;

import java.util.List;

/**
 * Event that is fired when {@link com.blizzard.mobilegarrison.datalayer.tasks.async.GetTraitsTask}
 * returns a list of Trait Details
 *
 * @author Joshua Hale
 */
public class GetTraitDetailsEvent {

    private final List<TraitDetails> traitDetails;

    public GetTraitDetailsEvent(List<TraitDetails> traitDetails) {
        this.traitDetails = traitDetails;
    }

    public List<TraitDetails> getResult() {
        return traitDetails;
    }
}
