package com.blizzard.mobilegarrison.ui.garrison.mission;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.blizzard.mobilegarrison.R;
import com.blizzard.mobilegarrison.adapters.FollowersAdapter;
import com.blizzard.mobilegarrison.constants.Font;
import com.blizzard.mobilegarrison.datalayer.constants.ErrorStrings;
import com.blizzard.mobilegarrison.datalayer.constants.MobileGarrisonConstants;
import com.blizzard.mobilegarrison.datalayer.interfaces.OnFollowerRemovedListener;
import com.blizzard.mobilegarrison.datalayer.manager.bus.EventBus;
import com.blizzard.mobilegarrison.datalayer.objects.garrison.follower.Follower;
import com.blizzard.mobilegarrison.datalayer.objects.garrison.mission.Mission;
import com.blizzard.mobilegarrison.datalayer.objects.garrison.mission.MissionEnemy;
import com.blizzard.mobilegarrison.datalayer.objects.item.WoWItem;
import com.blizzard.mobilegarrison.datalayer.tasks.async.GetInactiveFollowersTask;
import com.blizzard.mobilegarrison.datalayer.tasks.async.GetItemTask;
import com.blizzard.mobilegarrison.datalayer.tasks.async.GetMissionEnemyDetailsTask;
import com.blizzard.mobilegarrison.datalayer.tasks.async.StartMissionTask;
import com.blizzard.mobilegarrison.datalayer.tasks.results.GetErrorEvent;
import com.blizzard.mobilegarrison.datalayer.tasks.results.GetInactiveFollowersEvent;
import com.blizzard.mobilegarrison.datalayer.tasks.results.GetItemEvent;
import com.blizzard.mobilegarrison.datalayer.tasks.results.GetMissionEnemyDetailsEvent;
import com.blizzard.mobilegarrison.datalayer.tasks.results.StartMissionEvent;
import com.blizzard.mobilegarrison.utils.color.ColorUtils;
import com.blizzard.mobilegarrison.utils.error.ErrorUtils;
import com.blizzard.mobilegarrison.utils.font.FontUtils;
import com.blizzard.mobilegarrison.utils.garrison.GarrisonUtils;
import com.blizzard.mobilegarrison.utils.garrison.SuccessChance;
import com.blizzard.mobilegarrison.utils.lazyload.ImageLoader;
import com.blizzard.mobilegarrison.utils.notification.NotificationUtils;
import com.blizzard.mobilegarrison.utils.view.ViewUtils;
import com.blizzard.mobilegarrison.widgets.mission.MissionEnemyLayout;
import com.blizzard.mobilegarrison.widgets.mission.MissionFollowerLayout;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Fragment that shows the details for a specific Mission. This Fragment is pretty complex
 * with some other custom widgets that handle some of the UI. The Missions Enemy Layout
 * handles all of the enemies for a specific mission. The Followers Layout generates
 * a UI element for each follower that can be applied to a mission. We use the GarrisonUtils
 * to calculate the success chance of the missions as well.
 *
 * @author Joshua Hale (josh@joshuahale.com)
 * @version 0.1
 * @since 0.1
 */
public class MissionDetailFragment extends Fragment implements OnFollowerRemovedListener {

    private static final String TAG = "MissionDetailFragment";

    private Mission mMission;
    private List<MissionEnemy> mMissionEnemies;
    private Views mViews;
    private Map<MissionFollowerLayout, Follower> mFollowerLayouts;
    private int currentSuccessChance = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //-- Allows us to set an Options Menu for the Fragment
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_mission_detail, parent, false);

        mFollowerLayouts = new LinkedHashMap<>();

        mViews = new Views(view);
        init();

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getInstance().register(this);
    }

    /**
     * Initializes the views for the Fragment
     */
    private void init() {

        Bundle args = getArguments();

        if (args != null) {
            mMission = (Mission) args.getSerializable(getString(R.string.mission));
            setupMissionUI();

            int itemReward = mMission.getMissionReward().getItemReward();

            if (itemReward != 0) {
                new GetItemTask(getActivity(), itemReward).execute();
            }

            //-- Retrieves our followers
            new GetInactiveFollowersTask(getActivity()).execute();

            //-- Retrieves the Mission Enemy Details
            new GetMissionEnemyDetailsTask(getActivity(), mMission).execute();
        }

        if (getActivity() instanceof MissionDetailActivity) {
            ((MissionDetailActivity) getActivity()).getToolbar()
                    .setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {
                    getActivity().finish();
                }
            });
        }
    }

    /**
     * Sets up the Mission UI with most of the data from the Mission object.
     */
    private void setupMissionUI() {

        mViews.missionTypeImageView.setImageResource(
                GarrisonUtils.getMissionTypeDrawable(mMission.getMissionType()));
        mViews.missionDataLinearLayout.setBackgroundResource(
                GarrisonUtils.getMissionDetailBackgroundDrawable(mMission.getMissionZone()));
        mViews.missionCostTextView.setText(String.valueOf(mMission.getGarrisonResourceCost()));
        mViews.missionDescriptionTextView.setText(mMission.getMissionDescription());
        mViews.missionIlvlTextView.setText("(" + String.valueOf(mMission.getMissionIlvl() + ")"));
        mViews.missionLevelTextView.setText(String.valueOf(mMission.getMissionLevel()));
        mViews.missionNameTextView.setText(String.valueOf(mMission.getMissionName()));
        mViews.missionTimeTextView.setText(String.valueOf(mMission.getMissionDuration()) + " hr");
        mViews.missionTypeTextView.setText(mMission.getMissionEnvironment());
        mViews.missionZoneTextView.setText(mMission.getMissionLocation());

        FontUtils fontUtils = new FontUtils(getActivity());
        fontUtils.setTypeface(Font.MORPHEUS, mViews.missionNameTextView);
        fontUtils.setTypeface(Font.MORPHEUS, mViews.missionZoneTextView);

        if (mMission.getMissionIlvl() == 0) {
            mViews.missionIlvlTextView.setVisibility(View.GONE);
        }

        setupFollowerLayout();
    }

    /**
     * Sets up the Enemies Layout from the results of our AsyncTask
     * @param enemies <b>(List)</b> - List of Mission Enemies from the AsyncTask
     */
    private void setupEnemyLayout(List<MissionEnemy> enemies) {

        mMissionEnemies = enemies;

        int size = enemies.size();

        mViews.missionEnemyLinearLayout.setWeightSum((float) size);

        //-- So that we don't duplicate the views that are dynamically generated
        mViews.missionEnemyLinearLayout.removeAllViews();

        //-- For each MissionEnemy, create a new MissionEnemyLayout and add it to the
        //-- layout for the mission enemies.
        for (MissionEnemy enemy : enemies) {
            MissionEnemyLayout enemyLayout = new MissionEnemyLayout(getActivity(), enemy);

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    1.0f);

            mViews.missionEnemyLinearLayout.addView(enemyLayout, params);
        }
    }

    /**
     * Sets up the followers list after our async task has completed
     */
    private void setupFollowersList(final List<Follower> followers) {

        //-- If we have mFollowers then create a new adapter and add our filters
        if (followers != null) {
            FollowersAdapter adapter = new FollowersAdapter(getActivity(), followers);

            mViews.mFollowersListView.setAdapter(adapter);
            mViews.mFollowersListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    Follower follower = followers.get(position);

                    addFollowerToMap(follower);
                }
            });

            ViewUtils.setListViewHeightBasedOnChildren(mViews.mFollowersListView);
        }
    }

    /**
     * Sets up our interactive Follower view, based on how many followers we can have for the mission
     */
    private void setupFollowerLayout() {

        //-- Remove all the views in fear of duplication
        mViews.missionFollowerLinearLayout.removeAllViews();


        //-- Get the maximum amount of followers for the mission
        int followerSize = mMission.getFollowerLimit();

        //-- For each Follower, add a MissionFollower layout so the user can interact with
        //-- it when putting the follower in.
        for (int i = 0; i < followerSize; i++) {
            MissionFollowerLayout followerLayout = new MissionFollowerLayout(getActivity(), this);
            mFollowerLayouts.put(followerLayout, null);

            mViews.missionFollowerLinearLayout.addView(followerLayout);
        }
    }

    /**
     * Adds a follower to our HashMap of Followers. Each follower is linked in a 1:1 ratio with a View
     * @param follower <b>(Follower)</b> - Adds a follower to a view.
     */
    private void addFollowerToMap(Follower follower) {

        if (mFollowerLayouts.values().contains(follower)) {
            return;
        }
        MissionFollowerLayout currentLayout = null;

        for (Map.Entry<MissionFollowerLayout, Follower> entry : mFollowerLayouts.entrySet()) {
            currentLayout = entry.getKey();
            Follower value = entry.getValue();

            if (value == null) {
                break;
            }
        }

        if (currentLayout != null) {
            mFollowerLayouts.put(currentLayout, follower);
            currentLayout.setFollower(follower);
            calculateSuccessChance();
        }
    }

    /**
     * Removes a follower from our HashMap of Followers. Each follower is linked 1:1 with a View.
     * @param follower <b>(Follower)</b> - Follower to remove from a view.
     */
    private void removeFollowerFromMap(Follower follower) {

        MissionFollowerLayout currentLayout = null;

        for (Map.Entry<MissionFollowerLayout, Follower> entry : mFollowerLayouts.entrySet()) {
            currentLayout = entry.getKey();
            Follower value = entry.getValue();

            if (value.getId() == follower.getId()) {
                break;
            }
        }

        if (currentLayout != null) {
            mFollowerLayouts.put(currentLayout, null);
            calculateSuccessChance();
        }
    }

    /**
     * Calculates the success chance for the mission. <br />
     * The success chance is based on the current followers that are attached to the mission
     * and how well they would succeed.
     */
    private void calculateSuccessChance() {
        List<Follower> followers = new ArrayList<>(mFollowerLayouts.values());
        List<Follower> tempFollowers = new ArrayList<>();

        for (int i = 0; i < followers.size(); i++) {
            if (followers.get(i) != null) {
                tempFollowers.add(followers.get(i));
            }
        }

        SuccessChance successChance = new SuccessChance(mMission, tempFollowers, mMissionEnemies);

        int chance = successChance.getSuccessChance();

        //-- TODO: Animate the view from the start value to the end value
        //-- TODO: Rather than just set a static text view value
        mViews.missionSuccessChanceTextView.setText(chance + "%");

        currentSuccessChance = chance;
    }

    /**
     * Checks a mission to make sure all criteria is valid before submitting it to the server.
     */
    private void checkMission() {

        int missionFollowerSize = mMission.getFollowerLimit();

        int currentFollowerSize = 0;

        List<Follower> followers = new ArrayList<>(mFollowerLayouts.values());

        for (int i = 0; i < followers.size(); i++) {
            if (followers.get(i) != null) {
                currentFollowerSize++;
            }
        }

        if (missionFollowerSize != currentFollowerSize) {
            EventBus.getInstance().post(new GetErrorEvent(ErrorStrings.ERROR_NOT_ENOUGH_FOLLOWERS, true));
        } else {
            mMission.setSuccessChance(currentSuccessChance);
            new StartMissionTask(getActivity(), mMission, followers).execute();
        }
    }

    @Override
    public void onFollowerRemoved(Follower follower) {
        removeFollowerFromMap(follower);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.mission_details, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {

        switch (menuItem.getItemId()) {
            case R.id.actionStartMission:
                checkMission();
                break;
            default:
                break;
        }

        return super.onOptionsItemSelected(menuItem);
    }

    @Override
    public void onDestroy() {
        mViews = null;
        super.onDestroy();
    }

    @Override
    public void onStop() {
        EventBus.getInstance().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void getMissionEnemies(GetMissionEnemyDetailsEvent event) {
        setupEnemyLayout(event.getResult());
    }

    /**
     * Callback returned when our followers have been handed back to the UI
     * @param event <b>(GetFollowersEvent)</b> - Event that returns the resulting object to the UI
     */
    @Subscribe
    public void getFollowers(GetInactiveFollowersEvent event) {
        setupFollowersList(event.getResult());
    }

    /**
     * Callback returned for our MissionReward, once it has been handed back to the Main Thread
     * @param event <b>(GetItemEvent)</b> - Event object that contains the MissionReward object
     *              from the backend.
     */
    @Subscribe
    public void getMissionReward(GetItemEvent event) {

        WoWItem rewardItem = event.getResult();

        mViews.missionRewardTextView.setText(rewardItem.getName());
        mViews.missionRewardTextView.setTextColor(getResources()
                .getColor(ColorUtils.getQualityColor(rewardItem.getQuality())));

        String iconUrl = MobileGarrisonConstants.ITEM_ICON_URL + rewardItem.getIcon();

        ImageLoader imageLoader = new ImageLoader();
        imageLoader.displayImage(iconUrl, mViews.missionRewardImageView);
    }

    /**
     * Callback returned when the user has successfully started a mission
     * @param event <b>(StartMissionEvent)</b> - Event that tells the user the mission has been
     *              successfully started.
     */
    @Subscribe
    public void onMissionStarted(StartMissionEvent event) {
        NotificationUtils.scheduleNotification(getActivity(), event.getMissionEndTime());
        Toast.makeText(getActivity(), "Mission started!", Toast.LENGTH_SHORT).show();
        getActivity().finish();
    }

    /**
     * Callback returned when there was an error with something the user did.
     * @param event <b>(GetErrorEvent)</b> - Event that handles the message for the resulting error.
     */
    @Subscribe
    public void onError(GetErrorEvent event) {
        String message = event.getMessage();
        boolean showDialog = event.showDialog();

        ErrorUtils.showMessage(getActivity(), message, showDialog);
    }

    static class Views {

        final ImageView missionRewardImageView;
        final ImageView missionTypeImageView;
        final LinearLayout missionDataLinearLayout;
        final LinearLayout missionEnemyLinearLayout;
        final LinearLayout missionFollowerLinearLayout;
        final ListView mFollowersListView;
        final TextView missionCostTextView;
        final TextView missionDescriptionTextView;
        final TextView missionIlvlTextView;
        final TextView missionLevelTextView;
        final TextView missionNameTextView;
        final TextView missionRewardTextView;
        final TextView missionSuccessChanceTextView;
        final TextView missionRewardAmountTextView;
        final TextView missionTimeTextView;
        final TextView missionTypeTextView;
        final TextView missionZoneTextView;

        public Views(View view) {

            missionRewardImageView = (ImageView) view.findViewById(R.id.missionRewardImageView);
            missionTypeImageView = (ImageView) view.findViewById(R.id.missionTypeImageView);
            missionDataLinearLayout = (LinearLayout) view.findViewById(R.id.missionDataLinearLayout);
            missionEnemyLinearLayout = (LinearLayout) view.findViewById(R.id.missionEnemyLinearLayout);
            missionFollowerLinearLayout = (LinearLayout) view.findViewById(R.id.missionFollowersLinearLayout);
            missionCostTextView = (TextView) view.findViewById(R.id.missionCostTextView);
            missionDescriptionTextView = (TextView) view.findViewById(R.id.missionDescriptionTextView);
            missionIlvlTextView = (TextView) view.findViewById(R.id.missionIlvlTextView);
            missionLevelTextView = (TextView) view.findViewById(R.id.missionLevelTextView);
            missionNameTextView = (TextView) view.findViewById(R.id.missionNameTextView);
            missionRewardTextView = (TextView) view.findViewById(R.id.missionRewardTextView);
            missionRewardAmountTextView = (TextView) view.findViewById(R.id.missionRewardAmountTextView);
            missionSuccessChanceTextView = (TextView) view.findViewById(R.id.missionSuccessChanceTextView);
            missionTimeTextView = (TextView) view.findViewById(R.id.missionTimeTextView);
            missionTypeTextView = (TextView) view.findViewById(R.id.missionTypeTextView);
            missionZoneTextView = (TextView) view.findViewById(R.id.missionZoneTextView);

            mFollowersListView = (ListView) view.findViewById(R.id.followersListView);
        }
    }
}
